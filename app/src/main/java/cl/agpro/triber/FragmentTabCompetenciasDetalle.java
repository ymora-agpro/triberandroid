package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.regex.Pattern;

public class FragmentTabCompetenciasDetalle extends Fragment {

    Functions funciones;
    View view;
    private getData getData;
    private GetDataTodos getDataTodos = null;

    private Seguimientos seguimientos, seguimientosTodos = null, seguimientosFavoritos = null;
    private SeguimientosAdapterDynamic seguimientosAdapterDynamic;
    private String searchText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        funciones = new Functions(getActivity());

        getData = new getData();
        getData.execute(); //OBTENGO DATA DE WS

        View view = inflater.inflate(R.layout.fragment_tab_competencias_detalle, container, false);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        getData.cancel(true);

        if (getDataTodos != null)
            getDataTodos.cancel(true);
    }

    public void setData(String searchText) {

        this.searchText = searchText;

        if (getDataTodos == null)
            getDataTodos = new GetDataTodos();

        if (seguimientosTodos == null)
            getDataTodos.execute();
        else
            setData2();
    }

    public void setData2() {
        seguimientos.clear();

        for (Seguimiento seguimiento : seguimientosTodos)
            if (Pattern.compile(Pattern.quote(searchText), Pattern.CASE_INSENSITIVE).matcher(seguimiento.title).find())
                seguimientos.add(seguimiento);

        seguimientosAdapterDynamic.notifyDataSetChanged();
    }

    public void closeData() {
        seguimientos.clear();
        seguimientos.addAll(seguimientosFavoritos);
        seguimientosAdapterDynamic.notifyDataSetChanged();
    }

    private class getData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                url = new URL(Functions.URL_WS + "/cms/ws/detalle_competencia.php?torneo=" + "" + "&user=" + funciones.userId() +"&type=1");
                Log.d("URL ", Functions.URL_WS + "/cms/ws/detalle_competencia.php?torneo=" + "" + "&user=" + funciones.userId() +"&type=1");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    private class GetDataTodos extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                int paisId = 1;
                String sURL = Functions.URL_WS + "/cms/ws/list_categories.php?action=jugador&user=" + funciones.userId() + "&pais=" + paisId + "&busqueda=1";
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsTodos(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("players");
            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            seguimientos = new Seguimientos();
            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);
                String typeRow = row.getString("type");
                Log.d("TYPE", typeRow);
                if (typeRow == "2") {
                    int id = row.getInt("publicidad_id");
                    int impresion_id = row.getInt("impresion_id");
                    String nombre = row.getString("link");
                    seguimientos.add(new Seguimiento("jugador", id, nombre, typeRow, impresion_id));
                } else {
                    int id = row.getInt("id");
                    String nombre = row.getString("nombre");
                    seguimientos.add(new Seguimiento("jugador", id, nombre, typeRow, 0));
                }
            }

            seguimientosFavoritos = new Seguimientos();
            seguimientosFavoritos.addAll(seguimientos);

            ListView lv = (ListView) getActivity().findViewById(R.id.listViewJugadores);
            seguimientosAdapterDynamic = new SeguimientosAdapterDynamic(getActivity(), R.layout.item_mis_seguimientos, seguimientos);
            lv.setAdapter(seguimientosAdapterDynamic);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                    Seguimiento seguimiento = (Seguimiento) parent.getItemAtPosition(position);
                    String type = seguimiento.typeRow;
                    if (type == "2") {
                        int valor = seguimiento.id;
                        int impresion_id = seguimiento.impresion_id;
                        String url_ws = seguimiento.title;
                        Log.d("PUBLICIDAD", String.valueOf(valor));
                        Intent intent = new Intent(getActivity(), ActivityPublicidadVer.class);
                        intent.putExtra("publicidadId", valor);
                        intent.putExtra("publicidadUrl", url_ws);
                        intent.putExtra("publicidadImpresionId", impresion_id);
                        startActivity(intent);
                    } else {
                        int valor = seguimiento.id;
                        Log.d("JUGADOR ID", String.valueOf(valor));
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getResultsTodos(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("jugador");
            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            seguimientosTodos = new Seguimientos();
            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);

                int id = row.getInt("jugador_id");
                String nombre = row.getString("nombre");
                seguimientosTodos.add(new Seguimiento("jugador", id, nombre, "1", 0));
            }

            setData2();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
