package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class ActivityListArriendo extends AppCompatActivity {
    Functions funciones;
    String Stringvariable;
    String ARG_SECTION_NUMBER;

    public static final String STRINGVARIABLES = "STRINGVARIABLES";

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_arriendo);

        funciones = new Functions(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        Stringvariable = this.getIntent().getExtras().getString("variable");

        Log.d("Las variables son", Stringvariable);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_list_arriendo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Fragment para los precios de los arriendos
     */
    public static class FragmentPrecio extends Fragment {

        String variable;

        private Functions funciones;
        private View rootView;

        public FragmentPrecio() {
        }

        public static FragmentPrecio newInstance(String str) {
            FragmentPrecio fragment = new FragmentPrecio();
            Bundle args = new Bundle();
            args.putString(STRINGVARIABLES, str);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            funciones = new Functions(getActivity());

            variable = getArguments().getString(STRINGVARIABLES);

            GetData getData = new GetData();
            getData.execute(variable); //OBTENGO DATA DE WS

            rootView = inflater.inflate(R.layout.fragment_precio_list_arriendo, container, false);
            return rootView;
        }

        private class GetData extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                String variable = params[0];

                URL url = null;
                try {
                    Log.d("URL ", Functions.URL_WS + "/cms/ws/check_canchas.php?" + variable);
                    url = new URL(Functions.URL_WS + "/cms/ws/check_canchas.php?" + variable);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {

                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResults(result);
            }

        }

        public void getResults(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("results");

                //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
                Punteros punteros = new Punteros();

                JSONObject canchasArray = (JSONObject) dataArray.get(0);

                JSONArray canchasAr = canchasArray.getJSONArray("canchas");

                Puntero puntero;
                if (dataArray.length() > 0) {
                    for (int j = 0; j < canchasAr.length(); j++) {
                        JSONObject row = canchasAr.getJSONObject(j);

                        puntero = new Puntero();

                        puntero.id = row.getInt("cancha_id");
                        puntero.equipo_nombre = row.getString("cancha_nombre");
                        puntero.serie_nombre = row.getString("direccion");
                        puntero.puntos = row.getString("precio");

                        punteros.add(puntero);
                    }
                }

                ListView ListViewPunteros = (ListView) rootView.findViewById(R.id.listPrecioArriendo);
                ArrayAdapterPunteros arrayAdapterPunteros = new ArrayAdapterPunteros(getActivity(), punteros);
                ListViewPunteros.setAdapter(arrayAdapterPunteros);

                ListViewPunteros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Puntero puntero1 = (Puntero) parent.getItemAtPosition(position);


                        Log.d("CANCHA_ID", String.valueOf(puntero1.id));

                        Intent intent = new Intent(getContext(), ActivityCancha.class);
                        intent.putExtra(ActivityCancha.CANCHA_ID, puntero1.id);
                        startActivity(intent);
                    }
                });


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private class ArrayAdapterPunteros extends ArrayAdapter<Puntero> {

            private ImageView foto;

            public ArrayAdapterPunteros(Context context, List<Puntero> punteros) {
                super(context, R.layout.listview_list_arriendo, punteros);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                LayoutInflater inflater = getActivity().getLayoutInflater();
                view = inflater.inflate(R.layout.listview_list_arriendo, parent, false);


                Puntero puntero = getItem(position);

                foto = (ImageView) view.findViewById(R.id.img_field);

                if (puntero.con_foto == null)
                    new AsyncTaskFoto().execute(puntero);
                else if (puntero.con_foto == true)
                    foto.setImageBitmap(puntero.foto);

                TextView nombre = (TextView) view.findViewById(R.id.txt_name_field);
                nombre.setText(puntero.equipo_nombre);

                TextView precio = (TextView) view.findViewById(R.id.txt_price_field);
                precio.setText(puntero.puntos);

                TextView direccion = (TextView) view.findViewById(R.id.txt_address_field);
                direccion.setText(puntero.serie_nombre);

                return view;
            }

            private void setImage(Bitmap bitmap) {
                if (bitmap != null)
                    foto.setImageBitmap(bitmap);
            }

            private class AsyncTaskFoto extends AsyncTask<Puntero, Void, Bitmap> {

                protected Bitmap doInBackground(Puntero... params) {
                    Puntero puntero = params[0];
                    ///imagenes/canchas/lista/x3/%@.jpg
                    puntero.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/canchas/lista/res-720x1280/" + puntero.id + ".jpg");

                    if (puntero.foto != null) {
                        puntero.con_foto = true;
                        //puntero.foto = funciones.imagenRedonda(puntero.foto);
                    } else puntero.con_foto = false;

                    return puntero.foto;
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    setImage(bitmap);
                }
            }
        }

    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return FragmentPrecio.newInstance(Stringvariable);
                case 1:
                    return FragmentPrecio.newInstance(Stringvariable);
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 1 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Precio";
                case 1:
                    return "Calidad";
            }
            return null;
        }
    }
}
