package cl.agpro.triber;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by yerkomora on 09-03-16.
 */
public class Jugador {
    public int id;
    public String posicion, nombre;

    public Bitmap foto = null;
    public Boolean con_foto = null;
    public ImageView imageView;
}
