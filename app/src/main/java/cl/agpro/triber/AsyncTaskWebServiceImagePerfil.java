package cl.agpro.triber;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by yerkomora on 12-11-15.
 */
public class AsyncTaskWebServiceImagePerfil extends AsyncTask<Void, Void, Boolean> {
    private static final String TAG = "AsyncTaskWebService";
    private String REQUEST_METHOD = "POST", SUCCESS_RESPONSE = "200";

    private final Bitmap bitmap;
    private URL url = null;
    private int usuario_id;

    public AsyncTaskWebServiceImagePerfil(Bitmap bitmap, String url, int usuario_id) {
        this.bitmap = bitmap;

        try {
            this.url = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        this.usuario_id = usuario_id;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            sendPostImage();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean Resultado) {
        super.onPostExecute(Resultado);

        if (Resultado) {
        }
    }

    private String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);

        return temp;
    }

    private void sendPostImage() throws Exception {
        final String USER_AGENT = "Mozilla/5.0";

        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "usuario_id=" + usuario_id + "&imagen=" + BitMapToString(bitmap);

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        // Respuesta
        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        Log.d(TAG, "Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());
    }
}