package cl.agpro.triber;

import android.util.Log;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

/**
 * Created by yerkomora on 23-09-15.
 */
public class MySQL {
    //private static final String TAG = "MySQL", str = "jdbc:mysql://200.63.98.242:3306/triber_principal?user=triber_android&password=i1VZ35IgOOp24RxiJN";
    private static final String TAG = "MySQL", str = "jdbc:mysql://200.63.98.242:3306/triber_desarrollo?user=triber_cmsd&password=zLbPT@V6cTN.Iagaip";
    private Connection conn;
    private Boolean conectado;

    public MySQL() {
        conn = null;
        conectado = false;
    }

    private Boolean cargarLibreria() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Log.d(TAG, "Libreria encontrada.");
            return true;
        } catch (Exception ex) {
            Log.d(TAG, "Libreria no encontrada." + ex.getMessage());
            return false;
        }
    }

    public void conectar() {
        cargarLibreria();

        try {
            conn = DriverManager.getConnection(str);
            conectado = true;
            Log.d(TAG, "Conecto.");
        } catch (SQLException e) {
            Log.d(TAG, "No conecto.");
            e.printStackTrace();
        }
    }

    public void desconectar() {
        if (!conectado)
            return;

        try {
            conn.close();
            conectado = false;
            Log.d(TAG, "Desconecto.");
        } catch (SQLException e) {
            Log.d(TAG, "No Desconecto.");
        }
    }

    public Boolean setRegistro(Usuario usuario) {
        if (!conectado)
            return false;

        // Guardar Usuario
        try {
            String stsql = "INSERT users SET user_nick='" + usuario.nick + "',user_email='" + usuario.email + "',user_password=MD5('" + usuario.clave + "')";
            Log.d(TAG, stsql);
            Statement st = (Statement) conn.createStatement();
            st.executeUpdate(stsql);
            return true;
        } catch (SQLException e) {
            Log.d(TAG, "MySQL SQLException: " + e.getMessage());
            Log.d(TAG, "MySQL SQLState: " + e.getSQLState());
            Log.d(TAG, "MySQL VendorError: " + e.getErrorCode());
            return false;
        }
    }

    public Boolean existeUsuario(String nickOemail, String clave) {
        if (!conectado)
            return false;

        try {
            String stsql = "SELECT 1 FROM usuarios WHERE (user_nick='" + nickOemail + "' OR user_email='" + nickOemail + "') AND user_password=MD5('" + clave + "')";
            Statement st = (Statement) conn.createStatement();
            ResultSet resultSet;

            Log.d(TAG, "existUsuario");
            Log.d(TAG, stsql);

            resultSet = st.executeQuery(stsql);

            if (resultSet.next())
                return true;
            else
                return false;

        } catch (SQLException ex) {
            Log.d(TAG, "SQLException: " + ex.getMessage());
            Log.d(TAG, "SQLState: " + ex.getSQLState());
            Log.d(TAG, "VendorError: " + ex.getErrorCode());
            return false;
        }
    }

    public Boolean existeUsuarioNick(String nick) {
        if (!conectado)
            return false;

        try {
            String stsql = "SELECT 1 FROM users WHERE user_nick='" + nick + "'";
            Statement st = (Statement) conn.createStatement();
            ResultSet resultSet;

            Log.d(TAG, "existeUsuarioNick");
            Log.d(TAG, stsql);

            resultSet = st.executeQuery(stsql);

            if (resultSet.next())
                return true;
            else
                return false;

        } catch (SQLException ex) {
            Log.d(TAG, "SQLException: " + ex.getMessage());
            Log.d(TAG, "SQLState: " + ex.getSQLState());
            Log.d(TAG, "VendorError: " + ex.getErrorCode());
            return false;
        }
    }

    public Boolean existeUsuarioEmail(String email) {
        if (!conectado)
            return false;

        try {
            String stsql = "SELECT 1 FROM users WHERE user_email='" + email + "'";
            Statement st = (Statement) conn.createStatement();
            ResultSet resultSet;

            Log.d(TAG, "existeUsuarioEmail");
            Log.d(TAG, stsql);

            resultSet = st.executeQuery(stsql);

            if (resultSet.next())
                return true;
            else
                return false;

        } catch (SQLException ex) {
            Log.d(TAG, "SQLException: " + ex.getMessage());
            Log.d(TAG, "SQLState: " + ex.getSQLState());
            Log.d(TAG, "VendorError: " + ex.getErrorCode());
            return false;
        }
    }

    public Paises getPaises() {
        Paises paises= new Paises();

        try {
            String stsql;
            Statement st = (Statement) conn.createStatement();
            ResultSet resultSet;

            // Tipos
            stsql = "SELECT pais_id,pais_nombre FROM ubicaciones_paises WHERE pais_habilitado=1 AND pais_publicado=1 ORDER BY pais_nombre";

            Log.d(TAG, "getSucursales");
            Log.d(TAG, stsql);

            resultSet = st.executeQuery(stsql);

            Pais pais;
            while (resultSet.next()) {
                pais = new Pais();

                pais.id=resultSet.getInt("pais_id");
                pais.nombre = resultSet.getString("pais_nombre");

                paises.add(pais);
            }

        } catch (SQLException ex) {
            Log.d(TAG, "SQLException: " + ex.getMessage());
            Log.d(TAG, "SQLState: " + ex.getSQLState());
            Log.d(TAG, "VendorError: " + ex.getErrorCode());
        }

        return paises;
    }
}