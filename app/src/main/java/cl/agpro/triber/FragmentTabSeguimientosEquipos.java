package cl.agpro.triber;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.util.Log;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.widget.AdapterView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.regex.Pattern;

public class FragmentTabSeguimientosEquipos extends Fragment {
    Functions funciones;
    View view;
    private getData getData;
    private GetDataTodos getDataTodos = null;

    private Seguimientos seguimientos, seguimientosTodos = null, seguimientosFavoritos = null;
    private SeguimientosAdapterDynamic seguimientosAdapterDynamic;
    private String searchText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        funciones = new Functions(getActivity());

        View view = inflater.inflate(R.layout.fragment_tab_seguimientos_equipos, container, false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        seguimientos = new Seguimientos();

        ListView lv = (ListView) getActivity().findViewById(R.id.listViewEquipos);

        seguimientosAdapterDynamic = new SeguimientosAdapterDynamic(getActivity(), R.layout.item_mis_seguimientos_equipos, seguimientos);
        lv.setAdapter(seguimientosAdapterDynamic);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                Seguimiento seguimiento = (Seguimiento) parent.getItemAtPosition(position);
                String type = seguimiento.typeRow;
                if (type == "2") {
                    int valor = seguimiento.id;
                    int impresion_id = seguimiento.impresion_id;
                    String url_ws = seguimiento.title;
                    Log.d("PUBLICIDAD", String.valueOf(valor));
                    Intent intent = new Intent(getActivity(), ActivityPublicidadVer.class);
                    intent.putExtra("publicidadId", valor);
                    intent.putExtra("publicidadUrl", url_ws);
                    intent.putExtra("publicidadImpresionId", impresion_id);
                    startActivity(intent);
                } else {
                    Log.d(Functions.SERIE_ID, String.valueOf(seguimiento.id));

                    Intent intent = new Intent(getContext(), ActivityEquipo.class);
                    intent.putExtra(Functions.SERIE_ID, seguimiento.id);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        getData = new getData();
        getData.execute();
    }

    @Override
    public void onPause() {
        super.onPause();
        getData.cancel(true);

        if (getDataTodos != null)
            getDataTodos.cancel(true);
    }

    public void setData(String searchText) {

        this.searchText = searchText;

        if (getDataTodos == null)
            getDataTodos = new GetDataTodos();

        if (seguimientosTodos == null)
            getDataTodos.execute();
        else
            setData2();
    }

    public void setData2() {
        seguimientos.clear();

        for (Seguimiento seguimiento : seguimientosTodos)
            if (Pattern.compile(Pattern.quote(searchText), Pattern.CASE_INSENSITIVE).matcher(seguimiento.title).find())
                seguimientos.add(seguimiento);

        seguimientosAdapterDynamic.notifyDataSetChanged();
    }

    public void updateData() {
        getData = new getData();
        getData.execute(true);
    }

    public void closeData() {
        seguimientos.clear();

        if (seguimientosFavoritos != null) {
            seguimientos.addAll(seguimientosFavoritos);
            seguimientosAdapterDynamic.notifyDataSetChanged();
        }
    }

    private class getData extends AsyncTask<Boolean, Void, String> {
        private Boolean update = false;

        @Override
        protected String doInBackground(Boolean... params) {
            StringBuilder response = new StringBuilder();

            if (params != null && params.length > 0 && !params[0])
                update = params[0];

            URL url = null;
            try {
                url = new URL(Functions.URL_WS + "/cms/ws/get_list_muro.php?user=" + funciones.userId() + "&action=equipos&dispositivo=" + funciones.dispositivoId());
                Log.d("URL ", Functions.URL_WS + "/cms/ws/get_list_muro.php?user=" + funciones.userId() + "&action=equipos&dispositivo=" + funciones.dispositivoId());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            if (update)
                getResultsUpdate(result);
            else
                getResults(result);
        }
    }

    private class GetDataTodos extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                int paisId = 1;

                String sURL = Functions.URL_WS + "/cms/ws/list_categories.php?action=equipo&user=" + funciones.userId() + "&pais=" + paisId + "&busqueda=1";
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsTodos(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("equipos");

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);
                String typeRow = row.getString("type");
                Log.d("TYPE", typeRow);
                if (typeRow == "2") {
                    int id = row.getInt("publicidad_id");
                    int impresion_id = row.getInt("impresion_id");
                    String nombre = row.getString("link");
                    seguimientos.add(new Seguimiento("equipo", id, nombre, typeRow, impresion_id));
                } else {
                    int id = row.getInt("equipo_serie_id");
                    String nombre = row.getString("equipo_nombre");
                    int equipo_id = row.getInt("equipo_id");

                    Seguimiento seguimiento = new Seguimiento("equipo", equipo_id, id, nombre, typeRow, 0);
                    seguimiento.serie = row.getString("equipo_serie_nombre");

                    seguimientos.add(seguimiento);
                }
            }

            seguimientosFavoritos = new Seguimientos();
            seguimientosFavoritos.addAll(seguimientos);

            seguimientosAdapterDynamic.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getResultsTodos(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("equipo");
            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            seguimientosTodos = new Seguimientos();
            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);

                int id = row.getInt("serie_id");
                String nombre = row.getString("nombre");
                int equipo_id = row.getInt("id");

                Seguimiento seguimiento = new Seguimiento("equipo", equipo_id, id, nombre, "1", 0);
                seguimiento.serie = row.getString("serie");

                seguimientosTodos.add(seguimiento);
            }

            setData2();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getResultsUpdate(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("equipos");

            seguimientos.clear();

            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);
                String typeRow = row.getString("type");
                Log.d("TYPE", typeRow);
                if (typeRow == "2") {
                    int id = row.getInt("publicidad_id");
                    int impresion_id = row.getInt("impresion_id");
                    String nombre = row.getString("link");
                    seguimientos.add(new Seguimiento("equipo", id, nombre, typeRow, impresion_id));
                } else {
                    int equipo_id = row.getInt("equipo_id");
                    int id = row.getInt("equipo_serie_id");
                    String nombre = row.getString("equipo_nombre");
                    seguimientos.add(new Seguimiento("equipo", equipo_id, id, nombre, typeRow, 0));
                }
            }

            seguimientosAdapterDynamic.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
