package cl.agpro.triber;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

/**
 * Created by yerkomora on 16-03-16.
 */
public class ActivityNotaNueva extends AppCompatActivity {
    Functions funciones;
    private SaveData saveData;
    String titulo, texto;
    int partido_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nota_nueva);

        funciones = new Functions(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        partido_id = bundle.getInt(Functions.PARTIDO_ID);

        Button btn = (Button) findViewById(R.id.btn_save_new_note);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveNewNote();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nota_nueva, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void saveNewNote() {
        EditText title = (EditText)findViewById(R.id.txt_title_new_note);
        titulo = title.getText().toString();

        EditText text = (EditText) findViewById(R.id.txt_text_new_note);
        texto = text.getText().toString();

        if ( titulo.equals("") ) {
            DialogResultTitle();
        } else {
            if ( texto.equals("") ) {
                DialogResultText();
            } else {
                saveData = new SaveData();
                saveData.execute(); //OBTENGO DATA DE WS
            }
        }
    }

    private void DialogResultTitle() {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(getString(R.string.error_title_new_note))
                .setTitle("Error");

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private void DialogResultText() {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(getString(R.string.error_text_new_notee))
                .setTitle("Error");

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private void DialogResultError() {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(getString(R.string.error_new_notee))
                .setTitle("Error");

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private class SaveData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/notas.php?user_id=" + funciones.userId() + "&partido_id=" + partido_id + "&action=add&title=" + titulo.replace(" ", "*") + "&description=" + texto.replace(" ", "*"));
                url = new URL(Functions.URL_WS + "/cms/ws/notas.php?user_id=" + funciones.userId() + "&partido_id=" + partido_id + "&action=add&title=" + titulo.replace(" ", "*") + "&description=" + texto.replace(" ", "*"));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultSave(result);
        }

    }

    public void getResultSave(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");
            JSONObject rowComment = dataArray.getJSONObject(0);
            int val  = rowComment.getInt("ok");

            if ( val == 1 ) {
                Intent intent = new Intent(this, ActivityPartido.class);
                intent.putExtra(Functions.PARTIDO_ID, partido_id);
                startActivity(intent);
            } else {
                DialogResultError();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}