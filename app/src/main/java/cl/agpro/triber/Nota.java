package cl.agpro.triber;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by franciscoriveros on 15-03-16.
 */
public class Nota {

    public int nota_id, partido_id, usuario_id, cant;
    public String name_notero, titulo, descripcion;

    public Bitmap foto = null;
    public Boolean con_foto = null;

    public ImageView imageView;
}
