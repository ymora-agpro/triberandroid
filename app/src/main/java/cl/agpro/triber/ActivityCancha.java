package cl.agpro.triber;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class ActivityCancha extends AppCompatActivity {

    public static final String CANCHA_ID = "CANCHA_ID";

    Functions funciones;
    int cancha_id;

    private GetData getData;

    Boolean con_foto_cancha = null;
    ImageView foto_cancha;
    Bitmap cancha_bipmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancha);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        funciones = new Functions(this);

        Bundle bundle = getIntent().getExtras();
        cancha_id = bundle.getInt(CANCHA_ID);

        GetData getData = new GetData();
        getData.execute();

    }

    private class GetData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/get_cancha.php?cancha=" + cancha_id);
                url = new URL(Functions.URL_WS + "/cms/ws/get_cancha.php?cancha=" + cancha_id);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("cancha");

            JSONObject row = dataArray.getJSONObject(0);//


            String com_id = row.getString("cancha_id");

            foto_cancha = (ImageView) findViewById(R.id.img_field_detail);

            if (con_foto_cancha == null)
                new AsyncTaskFotoPartido().execute(com_id);
            else if (con_foto_cancha == true)
                foto_cancha.setImageBitmap(cancha_bipmap);


            TextView txt_name = (TextView) findViewById(R.id.txt_name_field_detail);
            txt_name.setText(row.getString("cancha_numero"));



            TextView precio = (TextView) findViewById(R.id.txt_price_field_detail);
            precio.setText(row.getString("cancha_precio"));


            TextView direccion = (TextView) findViewById(R.id.txt_address_field_detail);
            direccion.setText(row.getString("instalacion_direccion"));


            TextView contenido = (TextView) findViewById(R.id.txt_content_field_detail);
            contenido.setText(row.getString("complemento"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setImage(Bitmap bitmap) {
        if (bitmap != null)
            foto_cancha.setImageBitmap(bitmap);
    }

    private class AsyncTaskFotoPartido extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... params) {
            String id = params[0];
            cancha_bipmap = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/canchas/detalle/x2/" + id + ".jpg");


            if (cancha_bipmap != null) {
                con_foto_cancha = true;
                cancha_bipmap = funciones.imagenRedonda(cancha_bipmap);
            } else con_foto_cancha  = false;

            return cancha_bipmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            setImage(bitmap);
        }
    }
}
