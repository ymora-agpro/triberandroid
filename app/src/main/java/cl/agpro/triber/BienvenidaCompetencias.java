package cl.agpro.triber;

/**
 * Created by franciscoriveros on 17-02-16.
 */
public class BienvenidaCompetencias {

    public int icon;
    public String title;

    public BienvenidaCompetencias() {
        super();
    }

    public BienvenidaCompetencias(int icon, String title) {
        super();
        this.icon = icon;
        this.title = title;
    }
}