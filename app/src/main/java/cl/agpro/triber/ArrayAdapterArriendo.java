package cl.agpro.triber;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

/**
 * Created by franciscoriveros on 03-03-16.
 */
public class ArrayAdapterArriendo extends ArrayAdapter<ArriendoTipo> {
    Functions funciones;

    Context context;
    int layoutResourceId;
    ArriendoTipos data = null;

    public ArrayAdapterArriendo(Context context, ArriendoTipos data) {
        super(context, R.layout.spinner_arriendo, data);

        funciones = new Functions(context);
        this.context = context;
        layoutResourceId = R.layout.spinner_arriendo;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        FollowData follow = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            follow = new FollowData();
            follow.txtTitle = (TextView) row.findViewById(R.id.spint_text_arriendo);
            row.setTag(follow);
        } else {
            follow = (FollowData) row.getTag();
        }


        final ArriendoTipo followIndex = data.get(position);
        follow.txtTitle.setText(followIndex.title);
        final FollowData finalFollow = follow;


        return row;
    }

    static class FollowData {
        TextView txtTitle;
    }

    private class WsFollow extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String UrlFollow = params[0];
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                url = new URL(UrlFollow);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("post on follow", result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }
}
