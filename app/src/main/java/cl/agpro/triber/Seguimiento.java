package cl.agpro.triber;

import android.graphics.Bitmap;
import android.view.View;

/**
 * Created by agpro on 26-02-16.
 */
public class Seguimiento {
    public String type;
    public int id, equipo_id;
    public Bitmap icon = null;
    public String title, serie;
    public String typeRow;
    public int impresion_id;

    public View view = null;

    public Seguimiento() {
        super();
    }

    public Seguimiento(String type, int id, String title, String typeRow, int impresion_id) {
        super();
        this.type = type;
        this.id = id;
        this.title = title;
        this.typeRow = typeRow;
        this.impresion_id = impresion_id;
    }

    public Seguimiento(String type, int equipo_id, int id, String title, String typeRow, int impresion_id) {
        super();
        this.type = type;
        this.equipo_id = equipo_id;
        this.id = id;
        this.title = title;
        this.typeRow = typeRow;
        this.impresion_id = impresion_id;
    }
}
