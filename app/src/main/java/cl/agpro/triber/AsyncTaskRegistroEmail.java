package cl.agpro.triber;

import android.os.AsyncTask;

/**
 * Created by yerkomora on 23-09-15.
 */
public class AsyncTaskRegistroEmail extends AsyncTask<Void, Void, Boolean> {

    private ActivityRegistro activityRegistro;
    private final String email;

    public AsyncTaskRegistroEmail(ActivityRegistro activityRegistro, String email) {
        this.activityRegistro = activityRegistro;
        this.email = email;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        MySQL mySQL = new MySQL();
        mySQL.conectar();

        Boolean existe = mySQL.existeUsuarioEmail(email);

        mySQL.desconectar();
        return existe;
    }

    @Override
    protected void onPostExecute(Boolean existe) {
        super.onPostExecute(existe);

        if (existe)
            activityRegistro.emailExiste();
        else
            activityRegistro.emailValido();
    }
}
