package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class ActivitySeguimientos extends AppCompatActivity {

    public static final int seguimiento_id = 1;

    Functions funciones;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seguimientos);

        funciones = new Functions(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_seguimientos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Fragment para las competencias de los seguimientos
     */
    public static class FragmentCompetencia extends Fragment {

        private Functions funciones;
        private View rootView;

        private static final String ARG_SECTION_NUMBER = "section_number";

        public FragmentCompetencia() {
        }

        public static FragmentCompetencia newInstance(int equipo_id) {
            FragmentCompetencia fragment = new FragmentCompetencia();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, equipo_id);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            funciones = new Functions(getActivity());

            GetData getData = new GetData();
            getData.execute(); //OBTENGO DATA DE WS

            rootView = inflater.inflate(R.layout.fragment_seguimiento_competencias, container, false);
            return rootView;
        }

        private class GetData extends AsyncTask<Integer, Void, String> {
            @Override
            protected String doInBackground(Integer... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                URL url = null;
                try {
                    Log.d("URL ", Functions.URL_WS + "/cms/ws/seguimiento_user.php?action=torneo&user=" + funciones.userId());
                    url = new URL(Functions.URL_WS + "/cms/ws/seguimiento_user.php?action=torneo&user=" + funciones.userId());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResults(result);
            }

        }

        public void getResults(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("torneos");

                //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
                Punteros punteros = new Punteros();

                //RELLENO MI CLASE CON LA INFORMACION DEL WS
                Puntero puntero;
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject row = dataArray.getJSONObject(i);

                    puntero = new Puntero();

                    int suc = row.getInt("suc");

                    if ( suc == 1 ) {

                        puntero.id = row.getInt("id");
                        puntero.equipo_nombre = row.getString("nombre");
                        puntero.serie_nombre = row.getString("cant");
                        puntero.suc = row.getInt("seguido");

                        punteros.add(puntero);
                    }
                }

                ListView ListViewPunteros = (ListView) rootView.findViewById(R.id.listCompetenciasSeguimiento);
                ArrayAdapterPunteros arrayAdapterPunteros = new ArrayAdapterPunteros(getActivity(), punteros);
                ListViewPunteros.setAdapter(arrayAdapterPunteros);

                //lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                ListViewPunteros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Puntero puntero1 = (Puntero) parent.getItemAtPosition(position);
                        int id_com = puntero1.id;


                        GetDataFollow getDataFollow = new GetDataFollow();
                        getDataFollow.execute(id_com); //OBTENGO DATA DE WS
                    }
                });



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private class ArrayAdapterPunteros extends ArrayAdapter<Puntero> {

            private ImageView foto;

            public ArrayAdapterPunteros(Context context, List<Puntero> punteros) {
                super(context, R.layout.listview_seguimiento_competencia, punteros);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                LayoutInflater inflater = getActivity().getLayoutInflater();
                view = inflater.inflate(R.layout.listview_seguimiento_competencia, parent, false);

                if ((position + 1) % 2 == 1)
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_1));
                else
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_2));

                Puntero puntero = getItem(position);

                puntero.imageView = (ImageView) view.findViewById(R.id.img_seguimiento_competencia);

                if (puntero.con_foto == null)
                    new AsyncTaskFoto().execute(puntero);
                else if (puntero.con_foto == true)
                    puntero.imageView.setImageBitmap(puntero.foto);

                TextView equipo = (TextView) view.findViewById(R.id.name_seguimiento_competencia);
                equipo.setText(puntero.equipo_nombre);

                TextView serie = (TextView) view.findViewById(R.id.follow_seguimiento_competencia);
                serie.setText(puntero.serie_nombre);

                int seguido = puntero.suc;

                ImageView img = (ImageView) view.findViewById(R.id.star_seguimiento_competencia);

                if (seguido == 1) {
                    img.setImageResource(R.mipmap.ic_list_star_on);
                } else {
                    img.setImageResource(R.mipmap.ic_list_star_off);
                }

                return view;
            }

            private void setImage(Puntero puntero) {
                if (puntero.foto != null)
                    puntero.imageView.setImageBitmap(puntero.foto);
            }

            private class AsyncTaskFoto extends AsyncTask<Puntero, Void, Puntero> {

                protected Puntero doInBackground(Puntero... params) {
                    Puntero puntero = params[0];
                    puntero.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/torneos/" + puntero.id + ".jpg");

                    if (puntero.foto != null) {
                        puntero.con_foto = true;
                        puntero.foto = funciones.imagenRedonda(puntero.foto);
                    } else puntero.con_foto = false;

                    return puntero;
                }

                @Override
                protected void onPostExecute(Puntero puntero) {
                    setImage(puntero);
                }
            }
        }

        private class GetDataFollow extends AsyncTask<Integer, Void, String> {
            @Override
            protected String doInBackground(Integer... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                String id_com = params[0].toString();

                URL url = null;
                try {
                    Log.d("URL ", Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + id_com + "&follow_table=seguidores_torneos&follow_type=torneo_id");
                    url = new URL(Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + id_com + "&follow_table=seguidores_torneos&follow_type=torneo_id");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResultsFollow(result);
            }

        }

        public void getResultsFollow(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());

                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                //Punteros punteros = new Punteros();
                //punteros.clear();

                GetData getData = new GetData();
                getData.execute(); //OBTENGO DATA DE WS

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Fragment para los equipos de los seguimientos
     */
    public static class FragmentEquipo extends Fragment {
        private Functions funciones;
        private View rootView;

        private static final String ARG_SECTION_NUMBER = "section_number";

        public FragmentEquipo() {
        }

        public static FragmentEquipo newInstance(int equipo_id) {
            FragmentEquipo fragment = new FragmentEquipo();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, equipo_id);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            funciones = new Functions(getActivity());

            GetData getData = new GetData();
            getData.execute(); //OBTENGO DATA DE WS

            rootView = inflater.inflate(R.layout.fragment_seguimiento_equipos, container, false);
            return rootView;
        }

        private class GetData extends AsyncTask<Integer, Void, String> {
            @Override
            protected String doInBackground(Integer... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                URL url = null;
                try {
                    Log.d("URL ", Functions.URL_WS + "/cms/ws/seguimiento_user.php?action=equipo&user=" + funciones.userId());
                    url = new URL(Functions.URL_WS + "/cms/ws/seguimiento_user.php?action=equipo&user=" + funciones.userId());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResults(result);
            }

        }

        public void getResults(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("equipos");

                //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
                Punteros punteros = new Punteros();

                //RELLENO MI CLASE CON LA INFORMACION DEL WS
                Puntero puntero;
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject row = dataArray.getJSONObject(i);

                    puntero = new Puntero();

                    int suc = row.getInt("suc");

                    if ( suc == 1 ) {

                        puntero.id = row.getInt("equipo_id");
                        puntero.id_serie = row.getInt("serie_id");
                        puntero.equipo_nombre = row.getString("equipo_nombre") + " " + row.getString("serie_nombre");
                        puntero.serie_nombre = row.getString("cant");
                        puntero.suc = row.getInt("seguido");

                        punteros.add(puntero);
                    }
                }

                ListView ListViewPunteros = (ListView) rootView.findViewById(R.id.listEquiposSeguimiento);
                ArrayAdapterPunteros arrayAdapterPunteros = new ArrayAdapterPunteros(getActivity(), punteros);
                ListViewPunteros.setAdapter(arrayAdapterPunteros);

                ListViewPunteros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Puntero puntero1 = (Puntero) parent.getItemAtPosition(position);
                        int id_equ = puntero1.id_serie;


                        GetDataFollow getDataFollow = new GetDataFollow();
                        getDataFollow.execute(id_equ); //OBTENGO DATA DE WS
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private class ArrayAdapterPunteros extends ArrayAdapter<Puntero> {

            private ImageView foto;

            public ArrayAdapterPunteros(Context context, List<Puntero> punteros) {
                super(context, R.layout.listview_seguimiento_equipo, punteros);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                LayoutInflater inflater = getActivity().getLayoutInflater();
                view = inflater.inflate(R.layout.listview_seguimiento_equipo, parent, false);

                if ((position + 1) % 2 == 1)
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_1));
                else
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_2));

                Puntero puntero = getItem(position);

                puntero.imageView = (ImageView) view.findViewById(R.id.img_seguimiento_equipo);

                if (puntero.con_foto == null)
                    new AsyncTaskFoto().execute(puntero);
                else if (puntero.con_foto == true)
                    puntero.imageView.setImageBitmap(puntero.foto);

                TextView equipo = (TextView) view.findViewById(R.id.name_seguimiento_equipo);
                equipo.setText(puntero.equipo_nombre);

                TextView serie = (TextView) view.findViewById(R.id.follow_seguimiento_equipo);
                serie.setText(puntero.serie_nombre);

                int seguido = puntero.suc;

                ImageView img = (ImageView) view.findViewById(R.id.star_seguimiento_equipo);

                if (seguido == 1) {
                    img.setImageResource(R.mipmap.ic_list_star_on);
                } else {
                    img.setImageResource(R.mipmap.ic_list_star_off);
                }

                return view;
            }

            private void setImage(Puntero puntero) {
                if (puntero.foto != null)
                    puntero.imageView.setImageBitmap(puntero.foto);
            }

            private class AsyncTaskFoto extends AsyncTask<Puntero, Void, Puntero> {

                protected Puntero doInBackground(Puntero... params) {
                    Puntero puntero = params[0];
                    puntero.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/equipos/" + puntero.id + ".jpg");

                    if (puntero.foto != null) {
                        puntero.con_foto = true;
                        puntero.foto = funciones.imagenRedonda(puntero.foto);
                    } else puntero.con_foto = false;

                    return puntero;
                }

                @Override
                protected void onPostExecute(Puntero puntero) {
                    setImage(puntero);
                }
            }
        }

        private class GetDataFollow extends AsyncTask<Integer, Void, String> {
            @Override
            protected String doInBackground(Integer... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                String id_equ = params[0].toString();

                URL url = null;
                try {
                    Log.d("URL ", Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + id_equ + "&follow_table=seguidores_equipos_series&follow_type=equipo_serie_id");
                    url = new URL(Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + id_equ + "&follow_table=seguidores_equipos_series&follow_type=equipo_serie_id");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResultsFollow(result);
            }

        }

        public void getResultsFollow(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());

                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                //Punteros punteros = new Punteros();
                //punteros.clear();

                GetData getData = new GetData();
                getData.execute(); //OBTENGO DATA DE WS

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Fragment para los jugadores de los seguimientos
     */
    public static class FragmentJugador extends Fragment {
        private Functions funciones;
        private View rootView;

        private static final String ARG_SECTION_NUMBER = "section_number";

        public FragmentJugador() {
        }

        public static FragmentJugador newInstance(int equipo_id) {
            FragmentJugador fragment = new FragmentJugador();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, equipo_id);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            funciones = new Functions(getActivity());

            GetData getData = new GetData();
            getData.execute(); //OBTENGO DATA DE WS

            rootView = inflater.inflate(R.layout.fragment_seguimiento_jugadores, container, false);
            return rootView;
        }

        private class GetData extends AsyncTask<Integer, Void, String> {
            @Override
            protected String doInBackground(Integer... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                URL url = null;
                try {
                    Log.d("URL ", Functions.URL_WS + "/cms/ws/seguimiento_user.php?action=jugador&user=" + funciones.userId());
                    url = new URL(Functions.URL_WS + "/cms/ws/seguimiento_user.php?action=jugador&user=" + funciones.userId());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResults(result);
            }

        }

        public void getResults(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("players");

                //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
                Punteros punteros = new Punteros();

                //RELLENO MI CLASE CON LA INFORMACION DEL WS
                Puntero puntero;
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject row = dataArray.getJSONObject(i);

                    puntero = new Puntero();

                    int suc = row.getInt("suc");

                    if ( suc == 1 ) {

                        puntero.id = row.getInt("id");
                        puntero.equipo_nombre = row.getString("nombre");
                        puntero.serie_nombre = row.getString("cant");
                        puntero.suc = row.getInt("seguido");

                        punteros.add(puntero);
                    }
                }

                ListView ListViewPunteros = (ListView) rootView.findViewById(R.id.listJugadoresSeguimiento);
                ArrayAdapterPunteros arrayAdapterPunteros = new ArrayAdapterPunteros(getActivity(), punteros);
                ListViewPunteros.setAdapter(arrayAdapterPunteros);

                ListViewPunteros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Puntero puntero1 = (Puntero) parent.getItemAtPosition(position);
                        int id_jug = puntero1.id;


                        GetDataFollow getDataFollow = new GetDataFollow();
                        getDataFollow.execute(id_jug); //OBTENGO DATA DE WS
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private class ArrayAdapterPunteros extends ArrayAdapter<Puntero> {

            private ImageView foto;

            public ArrayAdapterPunteros(Context context, List<Puntero> punteros) {
                super(context, R.layout.listview_seguimiento_jugador, punteros);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                LayoutInflater inflater = getActivity().getLayoutInflater();
                view = inflater.inflate(R.layout.listview_seguimiento_jugador, parent, false);

                if ((position + 1) % 2 == 1)
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_1));
                else
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_2));

                Puntero puntero = getItem(position);

                puntero.imageView = (ImageView) view.findViewById(R.id.img_seguimiento_jugador);

                if (puntero.con_foto == null)
                    new AsyncTaskFoto().execute(puntero);
                else if (puntero.con_foto == true)
                    puntero.imageView.setImageBitmap(puntero.foto);

                TextView nombre = (TextView) view.findViewById(R.id.name_seguimiento_jugador);
                nombre.setText(puntero.equipo_nombre);

                TextView cant = (TextView) view.findViewById(R.id.follow_seguimiento_jugador);
                cant.setText(puntero.serie_nombre);

                int seguido = puntero.suc;

                ImageView img = (ImageView) view.findViewById(R.id.star_seguimiento_jugador);

                if (seguido == 1) {
                    img.setImageResource(R.mipmap.ic_list_star_on);
                } else {
                    img.setImageResource(R.mipmap.ic_list_star_off);
                }

                return view;
            }

            private void setImage(Puntero puntero) {
                if (puntero.foto != null)
                    puntero.imageView.setImageBitmap(puntero.foto);
            }

            private class AsyncTaskFoto extends AsyncTask<Puntero, Void, Puntero> {

                protected Puntero doInBackground(Puntero... params) {
                    Puntero puntero = params[0];
                    puntero.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/perfiles/" + puntero.id + ".jpg");

                    if (puntero.foto != null) {
                        puntero.con_foto = true;
                        puntero.foto = funciones.imagenRedonda(puntero.foto);
                    } else puntero.con_foto = false;

                    return puntero;
                }

                @Override
                protected void onPostExecute(Puntero puntero) {
                    setImage(puntero);
                }
            }
        }

        private class GetDataFollow extends AsyncTask<Integer, Void, String> {
            @Override
            protected String doInBackground(Integer... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                String id_jug = params[0].toString();

                URL url = null;
                try {
                    Log.d("URL ", Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + id_jug + "&follow_table=seguidores_usuarios&follow_type=user_seguidor_id");
                    url = new URL(Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + id_jug + "&follow_table=seguidores_usuarios&follow_type=user_seguidor_id");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResultsFollow(result);
            }

        }

        public void getResultsFollow(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());

                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                //Punteros punteros = new Punteros();
                //punteros.clear();

                GetData getData = new GetData();
                getData.execute(); //OBTENGO DATA DE WS

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Fragment para los seguidores de los seguimientos
     */
    public static class FragmentSeguidor extends Fragment {
        private Functions funciones;
        private View rootView;

        private static final String ARG_SECTION_NUMBER = "section_number";

        public FragmentSeguidor() {
        }

        public static FragmentSeguidor newInstance(int equipo_id) {
            FragmentSeguidor fragment = new FragmentSeguidor();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, equipo_id);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            funciones = new Functions(getActivity());

            GetData getData = new GetData();
            getData.execute(); //OBTENGO DATA DE WS

            rootView = inflater.inflate(R.layout.fragment_seguimiento_seguidores, container, false);
            return rootView;
        }

        private class GetData extends AsyncTask<Integer, Void, String> {
            @Override
            protected String doInBackground(Integer... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                URL url = null;
                try {
                    Log.d("URL ", Functions.URL_WS + "/cms/ws/seguimiento_user.php?action=seguidor&user=" + funciones.userId());
                    url = new URL(Functions.URL_WS + "/cms/ws/seguimiento_user.php?action=seguidor&user=" + funciones.userId());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResults(result);
            }

        }

        public void getResults(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("followers");

                //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
                Punteros punteros = new Punteros();

                //RELLENO MI CLASE CON LA INFORMACION DEL WS
                Puntero puntero;
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject row = dataArray.getJSONObject(i);

                    puntero = new Puntero();

                    int suc = row.getInt("suc");

                    if ( suc == 1 ) {

                        puntero.id = row.getInt("id");
                        puntero.equipo_nombre = row.getString("nombre");
                        puntero.serie_nombre = row.getString("cant");
                        puntero.suc = row.getInt("seguido");

                        punteros.add(puntero);
                    }
                }

                ListView ListViewPunteros = (ListView) rootView.findViewById(R.id.listSeguidoresSeguimiento);
                ArrayAdapterPunteros arrayAdapterPunteros = new ArrayAdapterPunteros(getActivity(), punteros);
                ListViewPunteros.setAdapter(arrayAdapterPunteros);

                ListViewPunteros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Puntero puntero1 = (Puntero) parent.getItemAtPosition(position);
                        int id_seg = puntero1.id;


                        GetDataFollow getDataFollow = new GetDataFollow();
                        getDataFollow.execute(id_seg); //OBTENGO DATA DE WS
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private class ArrayAdapterPunteros extends ArrayAdapter<Puntero> {

            private ImageView foto;

            public ArrayAdapterPunteros(Context context, List<Puntero> punteros) {
                super(context, R.layout.listview_seguimiento_seguidor, punteros);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                LayoutInflater inflater = getActivity().getLayoutInflater();
                view = inflater.inflate(R.layout.listview_seguimiento_seguidor, parent, false);

                if ((position + 1) % 2 == 1)
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_1));
                else
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_2));

                Puntero puntero = getItem(position);

                puntero.imageView = (ImageView) view.findViewById(R.id.img_seguimiento_seguidor);

                if (puntero.con_foto == null)
                    new AsyncTaskFoto().execute(puntero);
                else if (puntero.con_foto == true)
                    puntero.imageView.setImageBitmap(puntero.foto);

                TextView nombre = (TextView) view.findViewById(R.id.name_seguimiento_seguidor);
                nombre.setText(puntero.equipo_nombre);

                TextView cant = (TextView) view.findViewById(R.id.follow_seguimiento_seguidor);
                cant.setText(puntero.serie_nombre);

                int seguido = puntero.suc;

                ImageView img = (ImageView) view.findViewById(R.id.star_seguimiento_seguidor);

                if (seguido == 1) {
                    img.setImageResource(R.mipmap.ic_list_star_on);
                } else {
                    img.setImageResource(R.mipmap.ic_list_star_off);
                }

                return view;
            }

            private void setImage(Puntero puntero) {
                if (puntero.foto != null)
                    puntero.imageView.setImageBitmap(puntero.foto);
            }

            private class AsyncTaskFoto extends AsyncTask<Puntero, Void, Puntero> {

                protected Puntero doInBackground(Puntero... params) {
                    Puntero puntero = params[0];
                    puntero.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/perfiles/" + puntero.id + ".jpg");

                    if (puntero.foto != null) {
                        puntero.con_foto = true;
                        puntero.foto = funciones.imagenRedonda(puntero.foto);
                    } else puntero.con_foto = false;

                    return puntero;
                }

                @Override
                protected void onPostExecute(Puntero puntero) {
                    setImage(puntero
                    );
                }
            }
        }

        private class GetDataFollow extends AsyncTask<Integer, Void, String> {
            @Override
            protected String doInBackground(Integer... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                String id_seg = params[0].toString();

                URL url = null;
                try {
                    Log.d("URL ", Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + id_seg + "&follow_table=seguidores_usuarios&follow_type=user_seguidor_id");
                    url = new URL(Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + id_seg + "&follow_table=seguidores_usuarios&follow_type=user_seguidor_id");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResultsFollow(result);
            }

        }

        public void getResultsFollow(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());

                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                //Punteros punteros = new Punteros();
                //punteros.clear();

                GetData getData = new GetData();
                getData.execute(); //OBTENGO DATA DE WS

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return FragmentCompetencia.newInstance(seguimiento_id);
                case 1:
                    return FragmentEquipo.newInstance(seguimiento_id);
                case 2:
                    return FragmentJugador.newInstance(seguimiento_id);
                case 3:
                    return FragmentSeguidor.newInstance(seguimiento_id);
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Competencias";
                case 1:
                    return "Equipos";
                case 2:
                    return "Jugadores";
                case 3:
                    return "Seguidores";
            }
            return null;
        }
    }
}
