package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class ActivityIngreso extends AppCompatActivity {
    //URL FOR WEBSERVICE
    public String URL_WS = "http://www.triber.ag-pro.cl/desarrollo/cms/ws";

    private static final String TAG = "ActivityIngreso";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso);


        final EditText etIngresoIdentificador = (EditText) findViewById(R.id.etIngresoIdentificador);
        final EditText etIngresoClave = (EditText) findViewById(R.id.etIngresoClave);
        Button btIngresoListo = (Button) findViewById(R.id.btIngresoListo);


        btIngresoListo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String identificador, clave;

                identificador = etIngresoIdentificador.getText().toString();
                clave = etIngresoClave.getText().toString();

                Boolean valido = true;

                if (TextUtils.isEmpty(identificador)) {
                    etIngresoIdentificador.setError(getString(R.string.no_vacio));
                    valido = false;
                } else
                    etIngresoIdentificador.setError(null);

                if (TextUtils.isEmpty(clave)) {
                    etIngresoClave.setError(getString(R.string.no_vacio));
                    valido = false;
                } else
                    etIngresoClave.setError(null);

                Log.d(TAG, identificador);
                Log.d(TAG, clave);
                String deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                Log.d(TAG, "device ID = " + deviceId);
                if (valido) {
                    new login().execute(identificador, clave, deviceId);
                }
            }
        });
    }

    public void usuarioNoExiste() {
        Toast toast =
                Toast.makeText(getApplicationContext(),
                        R.string.activity_ingreso_usuario_no_existe, Toast.LENGTH_SHORT);

        toast.show();
    }

    public void irAlHome() {
        Intent intent = new Intent(getBaseContext(), ActivityPaises.class);

        /*
        Intent intent = new Intent(getBaseContext(), ActivityHome.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
*/
        startActivity(intent);
        finish();

        // Cerrar redes y tutorial
        ActivityRedes.activityRedes.finish();
        ActivityTutorial.activityTutorial.finish();
    }

    public void goNewHome() {
        Intent intent = new Intent(getBaseContext(), ActivityHomeFinal.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();
    }

    private class login extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {

            String username = params[0];
            String pass = params[1];
            String deviceUniqueId = params[2];
            String Lang = Locale.getDefault().getDisplayLanguage();
            Log.d("PASA POR ACA = ", "parametro 1 = " + params[0] + "parametro2 = " + params[1]);
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                url = new URL(URL_WS + "/login_ios.php?username=" + username + "&pass=" + pass + "&action=login&disp=" + deviceUniqueId + "&lang=" + Lang + "&token=(null)&sistema=android");
                Log.d(TAG, URL_WS + "/login_ios.php?username=" + username + "&pass=" + pass + "&action=login&disp=" + deviceUniqueId + "&lang=" + Lang + "&token=(null)&sistema=android");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            ParseJson(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void ParseJson(String response) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(response.toString());
            JSONArray dataWs = obj.getJSONArray("login");
            JSONObject c = dataWs.getJSONObject(0);
            int idUser = c.getInt("id");
            int val = c.getInt("val");
            Log.d(TAG, "usuario = " + idUser + " es seguidor = " + val);
            if (idUser > 0) {
                Log.d(TAG, "user id mayor a 0");
                SharedPreferences prefs = getSharedPreferences("usuario", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("userId", idUser);
                editor.commit();
                if (val > 0) {
                    Log.d(TAG, "ir a home");
                    goNewHome();
                } else {
                    Log.d(TAG, "ir a bienvenida");
                    irBienvenida();
                }
            } else {
                Log.d(TAG, "usuario no existe");
                usuarioNoExiste();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void goHome() {
        Intent intent = new Intent(getBaseContext(), ActivityHome.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    public void irBienvenida() {
        Intent intent = new Intent(getBaseContext(), ActivityPaises.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();
    }
}
