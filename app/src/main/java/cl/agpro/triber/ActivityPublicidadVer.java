package cl.agpro.triber;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ActivityPublicidadVer extends AppCompatActivity {
    Functions funciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        funciones = new Functions(this);

        setContentView(R.layout.activity_publicidad_ver);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.publicidad_title);
        setSupportActionBar(toolbar);

        //CARGO URL
        int paisId = this.getIntent().getExtras().getInt("publicidadId");
        int seguimientoId = this.getIntent().getExtras().getInt("publicidadImpresionId");
        String url_ws = this.getIntent().getExtras().getString("publicidadUrl");
        Log.d("DATOS RECIBIDOS", "paid = " + paisId + " url = " + url_ws + "seguimiento = " + seguimientoId);
        WebView myWebView = (WebView) this.findViewById(R.id.webView);
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.loadUrl(url_ws);

        String UrlSet = Functions.URL_WS + "/cms/ws/get_list_muro.php?action=click_ads&impresion_id=" + seguimientoId;
        new marcarPublicidadVista().execute(UrlSet);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class marcarPublicidadVista extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String UrlFollow = params[0];
            StringBuilder response = new StringBuilder();
            URL url = null;
            try {
                url = new URL(UrlFollow);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("post on follow", result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }
}
