package cl.agpro.triber;

import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by yerkomora on 10-03-16.
 */
public class ArrayAdapterPartidos extends ArrayAdapter<Partido> {
    private Functions funciones;

    public ArrayAdapterPartidos(Context context, List<Partido> Partidos) {
        super(context, R.layout.listview_partido, Partidos);

        funciones = new Functions(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.listview_partido, parent, false);

        Partido partido = getItem(position);
        Equipo equipo;
        int id;

        if(partido!=null)
        for (int i = 0; i < 2; i++) {
            equipo = partido.equipo[i];

            id = getContext().getResources().getIdentifier("equipo" + (i + 1) + "Imagen", "id", getContext().getPackageName());

            equipo.imageView = (ImageView) view.findViewById(id);

            if (equipo.con_foto == null)
                new AsyncTaskFoto().execute(equipo);
            else if (equipo.con_foto == true)
                equipo.imageView.setImageBitmap(equipo.foto);


            id = getContext().getResources().getIdentifier("equipo" + (i + 1) + "Nombre", "id", getContext().getPackageName());
            TextView nombre = (TextView) view.findViewById(id);
            nombre.setText(equipo.nombre);

            id = getContext().getResources().getIdentifier("equipo" + (i + 1) + "Serie", "id", getContext().getPackageName());
            TextView serie = (TextView) view.findViewById(id);
            serie.setText(equipo.serie.nombre);

            if (partido.jugado) {
                id = getContext().getResources().getIdentifier("equipo" + (i + 1) + "Goles", "id", getContext().getPackageName());
                TextView goles = (TextView) view.findViewById(id);
                goles.setText(String.valueOf(equipo.goles));
            }
        }

        TextView fecha = (TextView) view.findViewById(R.id.fecha);
        fecha.setText(partido.fecha);

        TextView separador = (TextView) view.findViewById(R.id.separador);

        if (!partido.jugado) {
            separador.setText("VS");
            separador.setTypeface(null, Typeface.BOLD);
        } else
            separador.setText("-");

        view.setTag(partido.id);

        return view;
    }

    private void setImage(Equipo equipo) {
        if (equipo.foto != null)
            equipo.imageView.setImageBitmap(equipo.foto);
    }

    private class AsyncTaskFoto extends AsyncTask<Equipo, Void, Equipo> {

        protected Equipo doInBackground(Equipo... params) {
            Equipo equipo = params[0];
            equipo.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/equipos/" + equipo.id + ".jpg");

            if (equipo.foto != null) {
                equipo.con_foto = true;
                equipo.foto = funciones.imagenRedonda(equipo.foto);
            } else equipo.con_foto = false;

            return equipo;
        }

        @Override
        protected void onPostExecute(Equipo equipo) {
            setImage(equipo);
        }
    }
}