package cl.agpro.triber;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ActivityTutorial extends AppCompatActivity {
    public static ActivityTutorial activityTutorial;

    private View rlTutorial;

    private final Handler HideBarHandler = new Handler();

    private final Runnable hideBarDelay = new Runnable() {
        @Override
        public void run() {
            hideBar();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityTutorial = this;
        setContentView(R.layout.activity_tutorial);

        // Ocultar Action y Status Bar
        rlTutorial = (RelativeLayout) findViewById(R.id.rlTutorial);
        hideBar();

        ViewPager vpTutorial = (ViewPager) findViewById(R.id.vpTutorial);

        vpTutorial.setAdapter(new FragmentPagerAdapterTutorial(getSupportFragmentManager()));

        final ImageView ivTutorialPunto1 = (ImageView)
                findViewById(R.id.ivTutorialPunto1);
        final ImageView ivTutorialPunto2 = (ImageView)
                findViewById(R.id.ivTutorialPunto2);
        final ImageView ivTutorialPunto3 = (ImageView)
                findViewById(R.id.ivTutorialPunto3);

        vpTutorial.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ivTutorialPunto1
                        .setImageResource(R.drawable.punto_desactivado);
                ivTutorialPunto2
                        .setImageResource(R.drawable.punto_desactivado);
                ivTutorialPunto3
                        .setImageResource(R.drawable.punto_desactivado);

                switch (position) {
                    case 0:
                        ivTutorialPunto1
                                .setImageResource(R.drawable.punto_activado);
                        break;
                    case 1:
                        ivTutorialPunto2
                                .setImageResource(R.drawable.punto_activado);
                        break;
                    case 2:
                        ivTutorialPunto3
                                .setImageResource(R.drawable.punto_activado);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Button bTutorialRegistrate = (Button) findViewById(R.id.bTutorialRegistrate);

        bTutorialRegistrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                irAlRedes(ActivityRedes.Actividad.REGISTRO);
            }
        });

        Button bTutorialIngresa = (Button) findViewById(R.id.bTutorialIngresa);

        bTutorialIngresa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                irAlRedes(ActivityRedes.Actividad.INGRESO);
            }
        });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        HideBarHandler.postDelayed(hideBarDelay, 500);
    }

    private void irAlRedes(ActivityRedes.Actividad actividad) {
        Intent intent = new Intent(getBaseContext(), ActivityRedes.class);
        intent.putExtra(ActivityRedes.ACTIVIDAD, actividad);
        startActivity(intent);
    }

    private void hideBar() {
        // Ocultar Action y Status Bar
        rlTutorial.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}