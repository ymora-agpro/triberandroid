package cl.agpro.triber;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ActivityPartido extends AppCompatActivity {

    private Functions funciones;
    private getData getData;

    /**
     * Foto
     */
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
    private static final int CAPTURE_IMAGE_FROM_GALLERY = 300;

    private static final int MEDIA_TYPE_IMAGE = 1;
    private static final int MEDIA_TYPE_VIDEO = 2;

    private Uri fileUri;

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private TabLayout tabLayout;

    private FragmentDetalles fragmentDetalles = null;
    private FragmentPartidoFotos fragmentPartidoFotos = null;
    private FragmentPartidoNotas fragmentPartidoNotas = null;

    private ViewPager mViewPager;
    private int partido_id;

    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partido);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                invalidateOptionsMenu();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        Bundle extras = getIntent().getExtras();
        partido_id = extras.getInt(Functions.PARTIDO_ID);
        Log.d(Functions.PARTIDO_ID, String.valueOf(partido_id));

        funciones = new Functions(this);
        usuario = new Usuario();
    }

    @Override
    public void onResume() {
        super.onResume();

        mSectionsPagerAdapter.updateData();

        getData = new getData();
        getData.execute(partido_id);

        new getDataRoles().execute(partido_id, funciones.userId());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_partido, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        switch (mViewPager.getCurrentItem()) {
            case 0:
                if (usuario.jugador || usuario.fotografo || usuario.notero)
                    menu.getItem(0).setIcon(R.mipmap.ic_megafono);
                else
                    menu.getItem(0).setVisible(false);
                break;
            case 1:
                if (usuario.fotografo)
                    menu.getItem(0).setIcon(R.mipmap.ic_photo);
                else
                    menu.getItem(0).setVisible(false);
                break;
            case 2:
                if (usuario.notero)
                    menu.getItem(0).setIcon(R.mipmap.ic_note);
                else
                    menu.getItem(0).setVisible(false);
                break;
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.home:
                funciones.irAlMuro();
                break;
            case R.id.accion:
                if (mViewPager.getCurrentItem() == 0) {
                    intent = new Intent(getBaseContext(), ActivityAccionSolicitud.class);
                    intent.putExtra(Functions.PARTIDO_ID, partido_id);
                    startActivity(intent);
                } else if (mViewPager.getCurrentItem() == 1)
                    clickpic();
                else if (mViewPager.getCurrentItem() == 2) {
                    intent = new Intent(getBaseContext(), ActivityNotaNueva.class);
                    intent.putExtra(Functions.PARTIDO_ID, partido_id);
                    startActivity(intent);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Foto
     */
    private void clickpic() {
        AlertDialog.Builder getImageFrom = new AlertDialog.Builder(this);
        getImageFrom.setTitle("Escoger Imagen desde: ")
                .setItems(R.array.array_items_foto, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a file to save the image
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
                            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE); //// start the image capture Intent
                        }
                        if (which == 1) {
                            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                            startActivityForResult(intent, CAPTURE_IMAGE_FROM_GALLERY);
                        }
                    }
                });
        AlertDialog dialog = getImageFrom.create();
        dialog.show();
    }

    private static Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private Bitmap BitmapCenterCropCuadrado(Bitmap srcBmp) {
        Bitmap dstBmp;

        if (srcBmp.getWidth() >= srcBmp.getHeight()) {
            dstBmp = Bitmap.createBitmap(
                    srcBmp,
                    srcBmp.getWidth() / 2 - srcBmp.getHeight() / 2,
                    0,
                    srcBmp.getHeight(),
                    srcBmp.getHeight()
            );

        } else {
            dstBmp = Bitmap.createBitmap(
                    srcBmp,
                    0,
                    srcBmp.getHeight() / 2 - srcBmp.getWidth() / 2,
                    srcBmp.getWidth(),
                    srcBmp.getWidth()
            );
        }

        return dstBmp;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                try {

                    Bitmap photo = MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);
                    photo = BitmapCenterCropCuadrado(photo);
                    photo = Bitmap.createScaledBitmap(photo, 400, 400, false);

                    new AsyncTaskWebServiceImage(photo, Functions.URL_WS + "/cms/ws/androi_subir_imagen_galeria_partido.php", partido_id, funciones.userId()).execute();
                    Toast.makeText(getBaseContext(), "Foto Actualizada", Toast.LENGTH_SHORT).show();
                } catch (IOException ex) {

                }

            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }

        if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Video captured and saved to fileUri specified in the Intent
                Toast.makeText(this, "Video saved to:\n" +
                        data.getData(), Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the video capture
            } else {
                // Video capture failed, advise user
            }
        }

        if (requestCode == CAPTURE_IMAGE_FROM_GALLERY) {
            if (resultCode == RESULT_OK) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();

                Bitmap imgseleccionado = BitmapFactory.decodeFile(imgDecodableString);
                imgseleccionado = BitmapCenterCropCuadrado(imgseleccionado);
                imgseleccionado = Bitmap.createScaledBitmap(imgseleccionado, 400, 400, false);
                new AsyncTaskWebServiceImage(imgseleccionado, Functions.URL_WS + "/cms/ws/androi_subir_imagen_galeria_partido.php", partido_id, funciones.userId()).execute();

                Toast.makeText(getBaseContext(), "Foto Actualizada", Toast.LENGTH_SHORT).show();
                //ImageView imgView = (ImageView) findViewById(R.id.imgView);
                // Set the Image in ImageView after decoding the String
                //imgView.setImageBitmap(BitmapFactory
                //        .decodeFile(imgDecodableString));

            }
        }
    }

    /**
     * Detalles del Partido
     */
    private class getData extends AsyncTask<Integer, Void, String> {
        @Override
        protected String doInBackground(Integer... params) {
            StringBuilder response = new StringBuilder();

            String partido_id = params[0].toString();

            URL url = null;
            try {
                String sURL = Functions.URL_WS + "/cms/ws/detalle_partidos.php?partido_id=" + partido_id;
                Log.d("URL ", sURL);
                url = new URL(sURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }
    }

    private void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("match");

            JSONObject row = dataArray.getJSONObject(0);

            int id;
            String pre;
            Equipo equipo;
            View.OnClickListener onClickListener;

            for (int i = 0; i < 2; i++) {
                //equipo = partido.equipo[i];
                equipo = new Equipo();

                if (i == 0)
                    pre = "local_";
                else
                    pre = "visita_";

                equipo.id = row.getInt(pre + "id");

                equipo.serie.id = row.getInt(pre + "serie_id");

                equipo.nombre = row.getString(pre + "nombre");
                equipo.serie.nombre = row.getString(pre + "serie_nombre");
                equipo.goles = row.getInt(pre + "goles");

                onClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int serie_id = (int) v.getTag();

                        Intent intent = new Intent(getBaseContext(), ActivityEquipo.class);
                        intent.putExtra(Functions.SERIE_ID, serie_id);
                        startActivity(intent);
                    }
                };

                id = getResources().getIdentifier("equipo" + (i + 1) + "Nombre", "id", getPackageName());
                TextView nombre = (TextView) findViewById(id);
                nombre.setText(equipo.nombre);
                nombre.setTag(equipo.serie.id);
                nombre.setOnClickListener(onClickListener);

                id = getResources().getIdentifier("equipo" + (i + 1) + "Serie", "id", getPackageName());
                TextView serie = (TextView) findViewById(id);
                serie.setText(equipo.serie.nombre);
                serie.setTag(equipo.serie.id);
                serie.setOnClickListener(onClickListener);

                if (row.getInt("jugado") == 1) {
                    id = getResources().getIdentifier("equipo" + (i + 1) + "Goles", "id", getPackageName());
                    TextView goles = (TextView) findViewById(id);
                    goles.setText(String.valueOf(equipo.goles));
                }

                id = getResources().getIdentifier("equipo" + (i + 1) + "Imagen", "id", getPackageName());
                equipo.imageView = (ImageView) findViewById(id);
                equipo.imageView.setTag(equipo.serie.id);
                equipo.imageView.setOnClickListener(onClickListener);

                if (equipo.con_foto == null)
                    new AsyncTaskFoto().execute(equipo);
                else if (equipo.con_foto == true)
                    equipo.imageView.setImageBitmap(equipo.foto);
            }

            TextView separador = (TextView) findViewById(R.id.separador);

            if (row.getInt("jugado") == 0) {
                separador.setText("VS");
                separador.setTypeface(null, Typeface.BOLD);
            } else
                separador.setText("-");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Roles del Usuario
     */
    private class getDataRoles extends AsyncTask<Integer, Void, String> {
        @Override
        protected String doInBackground(Integer... params) {
            StringBuilder response = new StringBuilder();

            String partido_id = params[0].toString();
            String usuario_id = params[1].toString();

            URL url = null;
            try {
                String sURL = Functions.URL_WS + "/cms/ws/roles.php?user_id=" + usuario_id + "&partido_id=" + partido_id + "&rol=all_partidosinfo";
                Log.d("URL ", sURL);
                url = new URL(sURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsRoles(result);
        }
    }

    private void getResultsRoles(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");

            JSONObject row = dataArray.getJSONObject(0);

            usuario.jugador = row.getInt("jugador") == 1;
            usuario.notero = row.getInt("notero") == 1;
            usuario.fotografo = row.getInt("fotografo") == 1;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        invalidateOptionsMenu();
    }

    /**
     * Imagen de los Equipos
     */
    private void setImage(Equipo equipo) {
        if (equipo.foto != null)
            equipo.imageView.setImageBitmap(equipo.foto);
    }

    private class AsyncTaskFoto extends AsyncTask<Equipo, Void, Equipo> {

        protected Equipo doInBackground(Equipo... params) {
            Equipo equipo = params[0];
            equipo.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/equipos/" + equipo.id + ".jpg");

            if (equipo.foto != null) {
                equipo.con_foto = true;
                equipo.foto = funciones.imagenRedonda(equipo.foto);
            } else equipo.con_foto = false;

            return equipo;
        }

        @Override
        protected void onPostExecute(Equipo equipo) {
            setImage(equipo);
        }
    }

    /**
     * Fragment para los detalles del partido
     */
    public static class FragmentDetalles extends Fragment {
        private getData getData;
        private View rootView;
        private Functions funciones;

        public FragmentDetalles() {
        }

        public static FragmentDetalles newInstance(int partido_id) {
            FragmentDetalles fragment = new FragmentDetalles();
            Bundle args = new Bundle();
            args.putInt(Functions.PARTIDO_ID, partido_id);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            funciones = new Functions(getActivity());

            getData = new getData();
            getData.execute(getArguments().getInt(Functions.PARTIDO_ID)); //OBTENGO DATA DE WS

            rootView = inflater.inflate(R.layout.fragment_partido_detalles, container, false);

            return rootView;
        }

        private class getData extends AsyncTask<Integer, Void, String> {
            @Override
            protected String doInBackground(Integer... params) {
                StringBuilder response = new StringBuilder();

                String partido_id = params[0].toString();

                URL url = null;
                try {
                    String sURL = Functions.URL_WS + "/cms/ws/detalle_partidos.php?partido_id=" + partido_id;
                    Log.d("URL ", sURL);
                    url = new URL(sURL);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResults(result);
            }
        }

        private void getResults(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("match");

                JSONObject row = dataArray.getJSONObject(0);

                final Partido partido = new Partido();

                partido.id = row.getInt("partido_id");

                partido.torneo.id = row.getInt("torneo_id");
                partido.torneo.nombre = row.getString("torneo_nombre");

                partido.fecha = row.getString("fecha");
                partido.cancha.direccion = row.getString("dir_cancha");


                TextView fecha = (TextView) rootView.findViewById(R.id.fecha);
                fecha.setText(partido.fecha);

                TextView direccion = (TextView) rootView.findViewById(R.id.direccion);
                direccion.setText(partido.cancha.direccion);

                TextView torneoNombre = (TextView) rootView.findViewById(R.id.torneoNombre);
                torneoNombre.setText(partido.torneo.nombre);

                partido.torneo.imageView = (ImageView) rootView.findViewById(R.id.torneoImagen);

                if (partido.torneo.con_foto == null)
                    new AsyncTaskFotoTorneo().execute(partido.torneo);
                else if (partido.torneo.con_foto == true)
                    partido.torneo.imageView.setImageBitmap(partido.torneo.foto);


                LinearLayout torneo = (LinearLayout) rootView.findViewById(R.id.torneo);

                torneo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), ActivityCompetencia.class);
                        intent.putExtra(Functions.COMPETENCIA_ID, partido.torneo.id);
                        startActivity(intent);
                    }
                });

                new getDataAcciones().execute(partido.id, 1);
                new getDataAcciones().execute(partido.id, 2);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        /**
         * Imagen del Torneo
         */
        private void setImage(Torneo torneo) {
            if (torneo.foto != null)
                torneo.imageView.setImageBitmap(torneo.foto);
        }

        private class AsyncTaskFotoTorneo extends AsyncTask<Torneo, Void, Torneo> {

            protected Torneo doInBackground(Torneo... params) {
                Torneo torneo = params[0];
                torneo.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/torneos/" + torneo.id + ".jpg");

                if (torneo.foto != null) {
                    torneo.con_foto = true;
                    torneo.foto = funciones.imagenRedonda(torneo.foto);
                } else torneo.con_foto = false;

                return torneo;
            }

            @Override
            protected void onPostExecute(Torneo torneo) {
                setImage(torneo);
            }
        }

        /**
         * Resultados del Partido
         */
        private class getDataAcciones extends AsyncTask<Integer, Void, String> {
            private String tiempo;

            @Override
            protected String doInBackground(Integer... params) {
                StringBuilder response = new StringBuilder();

                String partido_id;

                partido_id = params[0].toString();

                if (params[1] == 1)
                    tiempo = "primero";
                else
                    tiempo = "segundo";

                URL url = null;
                try {
                    String sURL = Functions.URL_WS + "/cms/ws/detalle_tiempo_partido.php?partido_id=" + partido_id + "&action=" + tiempo;
                    Log.d("URL ", sURL);
                    url = new URL(sURL);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResultsAcciones(result, tiempo);
            }
        }

        public void getResultsAcciones(String response, String tiempo) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("tiempo");

                //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
                Acciones acciones = new Acciones();

                //RELLENO MI CLASE CON LA INFORMACION DEL WS
                Accion accion;
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject row = dataArray.getJSONObject(i);

                    accion = new Accion();

                    accion.nombre = row.getString("nombre");
                    accion.equipo_posicion = row.getString("equipo_posicion");
                    accion.notificacion = row.getString("notificacion");
                    accion.minutos = row.getString("minutos");

                    acciones.add(accion);
                }

                LinearLayout lTiempo, ll;
                TextView tv;
                ImageView iv;

                int notificacion = 0;

                if (tiempo.equals("primero"))
                    lTiempo = (LinearLayout) rootView.findViewById(R.id.primerTiempo);
                else
                    lTiempo = (LinearLayout) rootView.findViewById(R.id.segundoTiempo);

                for (Accion a : acciones) {

                    ll = new LinearLayout(getActivity());

                    ll.setPadding(20, 20, 20, 20);

                    switch (a.notificacion) {
                        case "gol":
                            notificacion = R.mipmap.ic_goal;
                            break;
                        case "cambio_asale":
                            notificacion = R.mipmap.ic_leave;
                            break;
                        case "cambio_entra":
                            notificacion = R.mipmap.ic_enter;
                            break;
                        case "amarilla":
                            notificacion = R.mipmap.ic_yellow;
                            break;
                        case "roja":
                            notificacion = R.mipmap.ic_red;
                            break;
                        case "bloqueo":
                            notificacion = R.mipmap.ic_block;
                            break;
                        case "atiro":
                            notificacion = R.mipmap.ic_shot;
                            break;
                    }

                    if (a.equipo_posicion.equals("local")) {
                        ll.setGravity(Gravity.LEFT);

                        tv = new TextView(getActivity());
                        tv.setText(a.minutos);
                        ll.addView(tv);

                        iv = new ImageView(getActivity());
                        iv.setImageResource(notificacion);
                        iv.setPadding(20, 0, 20, 0);
                        ll.addView(iv);

                        tv = new TextView(getActivity());
                        tv.setText(a.nombre);
                        ll.addView(tv);
                    } else {
                        ll.setGravity(Gravity.RIGHT);

                        tv = new TextView(getActivity());
                        tv.setText(a.nombre);
                        ll.addView(tv);

                        iv = new ImageView(getActivity());
                        iv.setImageResource(notificacion);
                        iv.setPadding(20, 0, 20, 0);
                        ll.addView(iv);

                        tv = new TextView(getActivity());
                        tv.setText(a.minutos);
                        ll.addView(tv);
                    }

                    lTiempo.addView(ll);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    if (fragmentDetalles == null)
                        fragmentDetalles = FragmentDetalles.newInstance(partido_id);
                    return fragmentDetalles;
                case 1:
                    if (fragmentPartidoFotos == null)
                        fragmentPartidoFotos = FragmentPartidoFotos.newInstance(partido_id);
                    return fragmentPartidoFotos;
                default:
                    if (fragmentPartidoNotas == null)
                        fragmentPartidoNotas = FragmentPartidoNotas.newInstance(partido_id);
                    return fragmentPartidoNotas;
            }
        }


        public void updateData() {
            switch (tabLayout.getSelectedTabPosition()) {
                case 0:
                    //if (fragmentDetalles != null)
                    //fragmentDetalles.updateData();
                    break;
                case 1:
                    if (fragmentPartidoFotos != null)
                        fragmentPartidoFotos.updateData();
                    break;
                default:
                    if (fragmentPartidoNotas != null)
                        fragmentPartidoNotas.updateData();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Detalles";
                case 1:
                    return "Fotos";
                case 2:
                    return "Notas";
            }
            return null;
        }
    }
}
