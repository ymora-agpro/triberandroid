package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by jaimemedina on 19-11-15.
 */
public class ActivityPaises extends AppCompatActivity {
    //URL FOR WEBSERVICE
    public String URL_WS = Functions.URL_WS + "/cms/ws";
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paises);

        AsyncTaskPaises asyncTaskPaises = new AsyncTaskPaises(this);
        asyncTaskPaises.execute();
    }

    public void cargarPaises(Paises paises) {
        ListView lvPaises = (ListView) findViewById(R.id.lvPaises);
        lvPaises.setAdapter(new ArrayAdapterPaises(this, paises));

        lvPaises.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                Pais pais = (Pais) parent.getItemAtPosition(position);
                int valor = pais.id;
                new connectWs().execute(String.valueOf(valor));
                Intent intent = new Intent(getBaseContext(), ActivityTabBienvenida.class);
                intent.putExtra("pais", valor);
                startActivity(intent);
                Log.d("TAG", String.valueOf(valor));
            }
        });
    }

    private class connectWs extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String paisId = params[0];
            StringBuilder response = new StringBuilder();
            URL url = null;
            try {
                prefs = getSharedPreferences("usuario", Context.MODE_PRIVATE);
                url = new URL(URL_WS + "/select_country.php?action=add&user_id=" + prefs.getInt("userId", 0) + "&country_id=" + paisId);
                Log.d("ActivityPaises", URL_WS + "/select_country.php?action=add&user_id=" + prefs.getInt("userId", 0) + "&country_id=" + paisId);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            ParseJson(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public String ParseJson(String response) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(response.toString());
            Log.d("GUARDAR PAIS", String.valueOf(obj));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }
}
