package cl.agpro.triber;

import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.CallbackManager;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import java.security.MessageDigest;

import android.util.Log;

import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import android.util.Base64;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class ActivityFacebook extends AppCompatActivity {
    //URL FOR WEBSERVICE
    public String URL_WS = "http://www.triber.ag-pro.cl/desarrollo/cms/ws";

    private static final String TAG = "ActivityFacebook";
    public static String TYPE = "";
    SharedPreferences prefs;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        //setContentView(R.layout.activity_facebook);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        if (loginButton != null) {
            loginButton.setReadPermissions("public_profile", "email", "user_friends");

            callbackManager = CallbackManager.Factory.create();
            getInstance();
        }
    }

    private void getInstance() {
        //LoginManager instance = LoginManager.getInstance();

        //instance.logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d(TAG, "onSuccess");

                        Profile profile = Profile.getCurrentProfile();
                        if (profile != null) {
                            Log.d(TAG, profile.getId());
                            Log.d(TAG, profile.getFirstName() + " " + profile.getMiddleName() + " " + profile.getLastName());
                        }


                        // App code
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        String email = "";
                                        String name  = "";
                                        // Application code
                                        try {
                                            email = object.get("email").toString();
                                            name = object.get("name").toString();
                                            String deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                                            String Lang = Locale.getDefault().getDisplayLanguage();
                                            Log.v("LoginActivity", email);
                                            switch (TYPE) {
                                                case "REGISTRO":
                                                    ActivityRedes.activityRedes.new registrarValidarFacebook().execute(email, email, name);
                                                    break;
                                                case "LOGIN":
                                                    ActivityRedes.activityRedes.new loginFb().execute(email, deviceId, Lang);
                                                    break;
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Log.v("LoginActivity", TYPE);
                                        Log.v("LoginActivity", response.toString());
                                        //irAlHome();
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "onSuccess");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d(TAG, "onSuccess");
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    protected void calcularKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            /*PackageInfo info = getPackageManager().getPackageInfo(
                    "cl.agpro.triber",
                    PackageManager.GET_SIGNATURES);*/
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
    protected void irAlHome() {
        Intent intent = new Intent(getBaseContext(), ActivityHome.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();
    }
    public static void typeLogin(String type){

        TYPE = type;
    }
}