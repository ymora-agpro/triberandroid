package cl.agpro.triber;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created by yerkomora on 18-01-16.
 */
public class ArrayAdapterPaises extends ArrayAdapter<Pais> {
    private Context context;
    private Paises paises;

    public ArrayAdapterPaises(Context context, Paises paises) {
        super(context, R.layout.listview_pais, paises);
        this.context = context;
        this.paises = paises;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View lvPais = inflater.inflate(R.layout.listview_pais, parent, false);

        Pais pais = paises.get(position);

        TextView tvPais = (TextView) lvPais.findViewById(R.id.tvPais);
        tvPais.setText(pais.nombre);

        return lvPais;
    }

}
