package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class ActivityNota extends AppCompatActivity {

    public static final String NOTA_ID = "NOTA_ID", PARTIDO_ID = "PARTIDO_ID";
    private GetData getData;
    private SaveData saveData;
    Functions funciones;
    int nota_id, partido_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nota);

        funciones = new Functions(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        nota_id = bundle.getInt(NOTA_ID);
        partido_id = bundle.getInt(PARTIDO_ID);

        getData = new GetData();
        getData.execute(partido_id); //OBTENGO DATA DE WS

        Log.d("NOTA_ID", String.valueOf(nota_id));
        Log.d("PARTIDO_ID", String.valueOf(partido_id));

        RelativeLayout rel = (RelativeLayout) findViewById(R.id.rel_add_coment);
        rel.setVisibility(View.GONE);

        Button btn_add = (Button) findViewById(R.id.btn_add_comment);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addComment();
            }
        });

        Button btn_cancel = (Button) findViewById(R.id.btn_cancel_comment);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_comment();
            }
        });

        Button btn_save = (Button) findViewById(R.id.btn_save_comment);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save_comment();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nota, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class GetData extends AsyncTask<Integer, Void, String> {
        @Override
        protected String doInBackground(Integer... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            String partido_id = params[0].toString();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/notas.php?action=list&partido_id=" + partido_id);
                url = new URL(Functions.URL_WS + "/cms/ws/notas.php?action=list&partido_id=" + partido_id);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }

    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");

            Notas notas = new Notas();
            Nota nota;

            if (dataArray.length() > 0) {

                JSONObject fotosArray = (JSONObject) dataArray.get(0);

                JSONArray fotosAr = fotosArray.getJSONArray("notas");

                for (int j = 0; j < fotosAr.length(); j++) {
                    JSONObject row = fotosAr.getJSONObject(j);

                    int nota_id_partido = row.getInt("nota_id");

                    if (nota_id == nota_id_partido) {

                        TextView nombre = (TextView) findViewById(R.id.txt_nombre_notero_partido);
                        nombre.setText(row.getString("name_notero"));

                        TextView titulo = (TextView) findViewById(R.id.txt_titulo_nota);
                        titulo.setText(row.getString("titulo"));

                        TextView texto = (TextView) findViewById(R.id.txt_descripcion_nota);
                        texto.setText(row.getString("texto"));

                        JSONObject comentariosArray = (JSONObject) fotosAr.get(j);
                        JSONArray CommentAr = comentariosArray.getJSONArray("comentarios");

                        if (CommentAr.length() == 0) {

                            ListView list = (ListView) findViewById(R.id.listComentNotas);
                            list.setVisibility(View.GONE);
                        }

                        for (int i = 0; i < CommentAr.length(); i++) {
                            JSONObject rowComment = CommentAr.getJSONObject(i);
                            nota = new Nota();

                            nota.usuario_id = rowComment.getInt("usuario_id");
                            nota.name_notero = rowComment.getString("name_coment");
                            nota.descripcion = rowComment.getString("texto");
                            nota.titulo = rowComment.getString("agregado");

                            notas.add(nota);
                        }
                        ListView ListViewNotas = (ListView) findViewById(R.id.listComentNotas);
                        ArrayAdapterNotas arrayAdapterNotas = new ArrayAdapterNotas(this, notas);
                        ListViewNotas.setAdapter(arrayAdapterNotas);

                        ListViewNotas.getLayoutParams().height = funciones.getPixeles(85 * CommentAr.length());
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class ArrayAdapterNotas extends ArrayAdapter<Nota> {

        public ArrayAdapterNotas(Context context, List<Nota> notas) {
            super(context, R.layout.listview_partidos_comentario_notas, notas);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.listview_partidos_comentario_notas, parent, false);

                Nota nota = getItem(position);

                ImageView imageView = (ImageView) convertView.findViewById(R.id.img_cometario_nota_partido);

                if (nota.con_foto == null)
                    new AsyncTaskFoto(imageView).execute(nota);
                else if (nota.con_foto == true)
                    imageView.setImageBitmap(nota.foto);


                TextView fecha = (TextView) convertView.findViewById(R.id.txt_fecha_comentario_nota_partido);
                fecha.setText(nota.titulo);

                TextView nombre = (TextView) convertView.findViewById(R.id.txt_nombre_comentario_nota_partido);
                nombre.setText(nota.name_notero);

                TextView descripcion = (TextView) convertView.findViewById(R.id.txt_descripcion_comentario_nota);
                descripcion.setText(nota.descripcion);
            }

            return convertView;
        }

        private class AsyncTaskFoto extends AsyncTask<Nota, Void, Bitmap> {
            public ImageView imageView;

            public AsyncTaskFoto(ImageView imageView) {
                this.imageView = imageView;
            }

            protected Bitmap doInBackground(Nota... params) {
                Nota nota = params[0];
                nota.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/perfiles/" + nota.usuario_id + ".jpg");

                if (nota.foto != null) {
                    nota.con_foto = true;
                    nota.foto = funciones.imagenRedonda(nota.foto);
                } else nota.con_foto = false;

                return nota.foto;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if (bitmap != null)
                    imageView.setImageBitmap(bitmap);
            }
        }
    }

    public void addComment() {
        RelativeLayout rel = (RelativeLayout) findViewById(R.id.rel_add_coment);
        rel.setVisibility(View.VISIBLE);
    }

    public void cancel_comment() {
        EditText text = (EditText) findViewById(R.id.txt_add_comentario);
        text.setText("");

        RelativeLayout rel = (RelativeLayout) findViewById(R.id.rel_add_coment);
        rel.setVisibility(View.GONE);
    }

    public void save_comment() {
        EditText text = (EditText) findViewById(R.id.txt_add_comentario);
        String texto = text.getText().toString();


        if (texto.equals("")) {
            DialogResultError();
        } else {
            texto = texto.replace(" ", "*");

            saveData = new SaveData();
            saveData.execute(texto); //OBTENGO DATA DE WS
        }

    }

    private void DialogResultError() {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage("La comentario no puede estar vacío.")
                .setTitle("Error");

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private class SaveData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            String text = params[0];

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/notas.php?action=comentario&nota_id=" + nota_id + "&user_id=" + funciones.userId() + "&description=" + text);
                url = new URL(Functions.URL_WS + "/cms/ws/notas.php?action=comentario&nota_id=" + nota_id + "&user_id=" + funciones.userId() + "&description=" + text);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultSave(result);
        }

    }

    public void getResultSave(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");
            JSONObject rowComment = dataArray.getJSONObject(0);
            int val = rowComment.getInt("ok");

            if (val == 1) {
                EditText text = (EditText) findViewById(R.id.txt_add_comentario);
                text.setText("");

                RelativeLayout rel = (RelativeLayout) findViewById(R.id.rel_add_coment);
                rel.setVisibility(View.GONE);

                getData = new GetData();
                getData.execute(partido_id); //OBTENGO DATA DE WS

            } else {
                EditText text = (EditText) findViewById(R.id.txt_add_comentario);
                text.setText("");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
