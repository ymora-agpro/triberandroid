package cl.agpro.triber;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class ActivityMensajes extends AppCompatActivity {

    Functions funciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensajes);

        funciones = new Functions(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        GetData getData = new GetData();
        getData.execute(); //OBTENGO DATA DE WS

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mensajes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class GetData extends AsyncTask<Integer, Void, String> {
        @Override
        protected String doInBackground(Integer... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/list_mensajes.php?user_id=" + funciones.userId());
                url = new URL(Functions.URL_WS + "/cms/ws/list_mensajes.php?user_id=" + funciones.userId());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }

    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("mensajes");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            Mensajes mensajes = new Mensajes();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            Mensaje mensaje;
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);

                mensaje = new Mensaje();

                int suc = row.getInt("suc");

                if ( suc == 1 ) {

                    mensaje.id = row.getInt("invita_id");
                    mensaje.invita = row.getString("invita");
                    mensaje.nombre = row.getString("nombre");
                    mensaje.descripcion = row.getString("descripcion");
                    mensaje.fecha = row.getString("fecha");

                    mensajes.add(mensaje);
                }
            }

            ListView ListViewMensaje = (ListView) findViewById(R.id.listViewMensajes);
            ArrayAdapterMensajes arrayAdapterMensajes = new ArrayAdapterMensajes(this, mensajes);
            ListViewMensaje.setAdapter(arrayAdapterMensajes);

            //lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class ArrayAdapterMensajes extends ArrayAdapter<Mensaje> {

        private ImageView foto;

        public ArrayAdapterMensajes(Context context, List<Mensaje> mensajes) {
            super(context, R.layout.listview_mensajes, mensajes);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            LayoutInflater inflater = getLayoutInflater();
            view = inflater.inflate(R.layout.listview_mensajes, parent, false);

            Mensaje mensaje = getItem(position);

            mensaje.imageView = (ImageView) view.findViewById(R.id.img_invita_mensaje);

            if (mensaje.con_foto == null)
                new AsyncTaskFoto().execute(mensaje);
            else if (mensaje.con_foto == true)
                mensaje.imageView.setImageBitmap(mensaje.foto);

            TextView nombre = (TextView) view.findViewById(R.id.name_invita_mensaje);
            nombre.setText(mensaje.invita);

            TextView descripcion = (TextView) view.findViewById(R.id.descripcion_mensaje);
            descripcion.setText(mensaje.descripcion);

            TextView fecha = (TextView) view.findViewById(R.id.fecha_mensaje);
            fecha.setText(mensaje.fecha);

            return view;
        }

        private void setImage(Mensaje mensaje) {
            if (mensaje.foto != null)
                mensaje.imageView.setImageBitmap(mensaje.foto);
        }

        private class AsyncTaskFoto extends AsyncTask<Mensaje, Void, Mensaje> {

            protected Mensaje doInBackground(Mensaje... params) {
                Mensaje mensaje = params[0];
                mensaje.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/perfiles/" + mensaje.id + ".jpg");

                if (mensaje.foto != null) {
                    mensaje.con_foto = true;
                    mensaje.foto = funciones.imagenRedonda(mensaje.foto);
                } else mensaje.con_foto = false;

                return mensaje;
            }

            @Override
            protected void onPostExecute(Mensaje mensaje) {
                setImage(mensaje);
            }
        }
    }
}
