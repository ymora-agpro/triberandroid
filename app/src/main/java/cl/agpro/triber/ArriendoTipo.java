package cl.agpro.triber;

/**
 * Created by franciscoriveros on 03-03-16.
 */
public class ArriendoTipo {

    public String title;
    public int id;

    public ArriendoTipo(String title, int id) {
        this.title = title;
        this.id = id;
    }

    @Override
    public String toString() {
        return title;
    }
}
