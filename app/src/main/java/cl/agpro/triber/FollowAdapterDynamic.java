package cl.agpro.triber;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

/**
 * Created by agpro on 23-02-16.
 */
public class FollowAdapterDynamic extends ArrayAdapter<Follow> {
    private Functions funciones;

    private Context context;
    private int mResource;

    private final LayoutInflater mInflater;

    public FollowAdapterDynamic(Context context, int mResource, Follows data) {
        super(context, mResource, data);

        funciones = new Functions(context);
        this.mResource = mResource;
        this.context = context;

        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        Follow followIndex = getItem(position);

        Button imgFollow;

        LayoutInflater inflater = mInflater;
        view = inflater.inflate(mResource, parent, false);

        // Imagen
        ImageView imgIcon = (ImageView) view.findViewById(R.id.imagen);

        String tipoFoto = null;
        if (followIndex.type == "torneo") {
            tipoFoto = "torneos";
            imgIcon.setImageResource(R.mipmap.ic_list_cup);
        } else if (followIndex.type == "equipo") {
            tipoFoto = "equipos";
            imgIcon.setImageResource(R.mipmap.ic_list_team);
        } else if (followIndex.type == "jugador") {
            tipoFoto = "perfiles";
            imgIcon.setImageResource(R.mipmap.ic_list_player);
        }

        int id;
        if (followIndex.type == "equipo")
            id = followIndex.equipo_id;
        else
            id = followIndex.id;

        if (followIndex.icon == null)
            new AsyncTaskImagen(imgIcon, followIndex).execute(Functions.URL_WS + "/imagenes/" + tipoFoto + "/" + id + ".jpg");
        else
            imgIcon.setImageBitmap(followIndex.icon);

        // Titulo
        TextView txtTitle = (TextView) view.findViewById(R.id.nombre);
        txtTitle.setText(String.valueOf(followIndex.title));

        // Estrella
        imgFollow = (Button) view.findViewById(R.id.imagenStar);

        if (followIndex.follower == "1")
            imgFollow.setBackgroundResource(R.drawable.estrella_activada);

        imgFollow.setTag(followIndex);

        imgFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View cellSelected) {

                Log.d("Estrella", "Click");

                String tablaOnClick = "";
                String campoOnClick = "";

                Follow followIndex = (Follow) cellSelected.getTag();

                //Log.d("click follow", "torneo id = " + followIndex.id + " is follower = " + cellSelected.getTag());

                if (followIndex.type == "torneo") {
                    tablaOnClick = "seguidores_torneos";
                    campoOnClick = "torneo_id";
                } else if (followIndex.type == "equipo") {
                    tablaOnClick = "seguidores_equipos_series";
                    campoOnClick = "equipo_serie_id";
                } else if (followIndex.type == "jugador") {
                    tablaOnClick = "seguidores_usuarios";
                    campoOnClick = "user_seguidor_id";
                }

                String UrlAddFollow = Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + followIndex.id + "&follow_table=" + tablaOnClick + "&follow_type=" + campoOnClick;
                new WsFollow().execute(UrlAddFollow);

                //Log.d("URL CLICK", UrlAddFollow);

                if (followIndex.follower == "1") {
                    followIndex.follower = "0";
                    cellSelected.setBackgroundResource(R.drawable.estrella_desactivada);
                } else {
                    followIndex.follower = "1";
                    cellSelected.setBackgroundResource(R.drawable.estrella_activada);
                }
            }
        });

        return view;
    }


    private class AsyncTaskImagen extends AsyncTask<String, Void, Bitmap> {
        private Follow follow;
        private ImageView imgIcon;

        public AsyncTaskImagen(ImageView followData, Follow follow) {
            this.imgIcon = followData;
            this.follow = follow;
        }

        protected Bitmap doInBackground(String... params) {
            Log.d("AsyncTaskImage", params[0]);
            return funciones.downloadBitmap(params[0]);
        }

        @Override
        protected void onPostExecute(Bitmap mBitmap) {
            if (mBitmap != null) {
                follow.icon = funciones.imagenRedonda(mBitmap);
                imgIcon.setImageBitmap(follow.icon);
            } else {
                Drawable myDrawable = null;
                if (follow.type == "torneo") {
                    myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_tournament, null);
                } else if (follow.type == "equipo") {
                    myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_team, null);
                } else if (follow.type == "jugador") {
                    myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_profile, null);
                }

                follow.icon = ((BitmapDrawable) myDrawable).getBitmap();
                imgIcon.setImageBitmap(follow.icon);
            }
        }
    }


    private class WsFollow extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String UrlFollow = params[0];
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                url = new URL(UrlFollow);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("post on follow", result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }
}