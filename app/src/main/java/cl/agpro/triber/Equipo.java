package cl.agpro.triber;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by yerkomora on 10-03-16.
 */
public class Equipo {
    public int id;
    public String nombre;

    public Serie serie;

    public Bitmap foto = null;
    public Boolean con_foto = null;
    public ImageView imageView;

    public int goles = 0;

    public Equipo() {
        serie = new Serie();
    }
}
