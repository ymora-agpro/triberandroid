package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class ActivityRegistro extends AppCompatActivity {
    //URL FOR WEBSERVICE
    public String URL_WS = "http://www.triber.ag-pro.cl/desarrollo/cms/ws";
    private static final String TAG = "ActivityRegistro";
    private EditText etRegistroNick, etRegistroEmail, etRegistroClave, etRegistroClaveRepetida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etRegistroNick = (EditText) findViewById(R.id.etRegistroNick);
        etRegistroEmail = (EditText) findViewById(R.id.etRegistroEmail);
        etRegistroClave = (EditText) findViewById(R.id.etRegistroClave);
        etRegistroClaveRepetida = (EditText) findViewById(R.id.etRegistroClaveRepetida);

        Button btRegistroListo = (Button) findViewById(R.id.btRegistroListo);

        btRegistroListo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Validar
                Boolean valido = true;

                String nick, email, clave, clave_repetida;

                nick = etRegistroNick.getText().toString();
                email = etRegistroEmail.getText().toString();
                clave = etRegistroClave.getText().toString();
                clave_repetida = etRegistroClaveRepetida.getText().toString();

                if (TextUtils.isEmpty(nick)) {
                    etRegistroNick.setError(getString(R.string.no_vacio));
                    valido = false;
                }

                if (TextUtils.isEmpty(email)) {
                    etRegistroEmail.setError(getString(R.string.no_vacio));
                    valido = false;
                }
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    etRegistroEmail.setError(getString(R.string.activity_registro_email_invalido));
                    valido = false;
                }
                if (TextUtils.isEmpty(clave)) {
                    etRegistroClave.setError(getString(R.string.no_vacio));
                    valido = false;
                }
                if (TextUtils.isEmpty(clave_repetida)) {
                    etRegistroClaveRepetida.setError(getString(R.string.no_vacio));
                    valido = false;
                } else {
                    if (!clave_repetida.equals(clave)) {
                        etRegistroClaveRepetida.setError(getString(R.string.activity_registro_clave_diferente));
                        valido = false;
                    }
                }

                // Guardar Usuario
                if (valido) {
                    new registrarValidar().execute(nick, email);
                }
            }
        });
    }

    private class registrarValidar extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {

            String username = params[0];
            String email = params[1];
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                url = new URL(URL_WS + "/login_ios.php?username=" + username + "&correo=" + email + "&action=validate");
                Log.d("URL VALIDATE", URL_WS + "/login_ios.php?username=" + username + "&correo=" + email + "&action=validate");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            responseValidate(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    private class registrar extends AsyncTask<String, Void, String> {
        String nick2 = etRegistroNick.getText().toString();
        String email2 = etRegistroEmail.getText().toString();
        String clave2 = etRegistroClave.getText().toString();
        String deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        String Lang = Locale.getDefault().getDisplayLanguage();

        @Override
        protected String doInBackground(String... params) {
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                url = new URL(URL_WS + "/login_ios.php?username=" + nick2 + "&correo=" + email2 + "&pass=" + clave2 + "&disp=" + deviceId + "&token=&lang=" + Lang + "&action=new&sistema=android");
                Log.d("url qla", URL_WS + "/login_ios.php?username=" + nick2 + "&correo=" + email2 + "&pass=" + clave2 + "&disp=" + deviceId + "&token=&lang=" + Lang + "&action=new&sistema=android");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            responseCreateNew(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void responseValidate(String response) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(response.toString());
            int success = obj.getInt("success");
            if (success == 2) {
                Log.d("xD", "sad 2");
                Toast toast = Toast.makeText(getApplicationContext(), R.string.activity_registro_nick_existe_final, Toast.LENGTH_SHORT);
                toast.show();
            } else if (success == 3) {
                Log.d("xD", "sad 3");
                Toast toast = Toast.makeText(getApplicationContext(), R.string.activity_registro_email_existe_final, Toast.LENGTH_SHORT);
                toast.show();
            } else {
                Log.d("xD", "sad 1");
                new registrar().execute();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void responseCreateNew(String response) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(response.toString());
            int userId = obj.getInt("success");
            if (userId > 0) {
                Log.d(TAG, "user id mayor a 0");
                SharedPreferences prefs = getSharedPreferences("usuario", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("userId", userId);
                editor.commit();

                irBienvenida();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void irAlHome() {
        Intent intent = new Intent(getBaseContext(), ActivityHome.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();

        // Cerrar redes y tutorial
        ActivityRedes.activityRedes.finish();
        ActivityTutorial.activityTutorial.finish();
    }

    public void irBienvenida() {
        Intent intent = new Intent(getBaseContext(), ActivityPaises.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();
    }

    public void nickExiste() {
        etRegistroNick.setError(getString(R.string.activity_registro_nick_existe));
    }

    public void nickValido() {
        etRegistroNick.setError(null);
    }

    public void emailExiste() {
        etRegistroEmail.setError(getString(R.string.activity_registro_email_existe));
    }

    public void emailValido() {
        etRegistroEmail.setError(null);
    }
}