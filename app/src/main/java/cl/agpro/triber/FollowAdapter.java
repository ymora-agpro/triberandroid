package cl.agpro.triber;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

/**
 * Created by agpro on 23-02-16.
 */
public class FollowAdapter extends ArrayAdapter<Follow> {
    Functions funciones;

    Context context;
    int layoutResourceId;
    Follow data[] = null;

    public FollowAdapter(Context context, int layoutResourceId, Follow[] data) {
        super(context, layoutResourceId, data);

        funciones = new Functions(context);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        FollowData follow = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            follow = new FollowData();
            follow.imgIcon = (ImageView) row.findViewById(R.id.imagen);
            follow.imgFollow = (Button) row.findViewById(R.id.imagenStar);
            follow.txtTitle = (TextView) row.findViewById(R.id.nombre);
            row.setTag(follow);
        } else {
            follow = (FollowData) row.getTag();
        }

        Button btn = (Button) row.findViewById(R.id.imagenStar);

        final Follow followIndex = data[position];
        follow.txtTitle.setText(followIndex.title);
        btn.setTag(followIndex.follower);
        if (followIndex.follower == "1") {
            follow.imgFollow.setBackgroundResource(R.drawable.estrella_activada);
        } else {
            follow.imgFollow.setBackgroundResource(R.drawable.estrella_desactivada);
        }
        //follow.imgIcon.setImageResource(followIndex.icon);
        //follow.imgIcon.setImageBitmap(followIndex.icon);
        final FollowData finalFollow = follow;
        //URL PARA IMAGEN
        String tipoFoto = null;
        if (followIndex.type == "torneo") {
            tipoFoto = "torneos";
        } else if (followIndex.type == "equipo") {
            tipoFoto = "equipos";
        } else if (followIndex.type == "jugador") {
            tipoFoto = "perfiles";
        }
        new AsyncTask<String, Void, Bitmap>() {

            protected Bitmap doInBackground(String... params) {
                return funciones.downloadBitmap(params[0]);
            }

            @Override
            protected void onPostExecute(Bitmap mBitmap) {
                if (mBitmap != null) {
                    finalFollow.imgIcon.setImageBitmap(funciones.imagenRedonda(mBitmap));
                } else {
                    Drawable myDrawable = null;
                    if (followIndex.type == "torneo") {
                        myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_tournament, null);
                    } else if (followIndex.type == "equipo") {
                        myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_team, null);
                    } else if (followIndex.type == "jugador") {
                        myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_profile, null);
                    }
                    finalFollow.imgIcon.setImageBitmap(((BitmapDrawable) myDrawable).getBitmap());
                }
            }
        }.execute(Functions.URL_WS + "/imagenes/" + tipoFoto + "/" + followIndex.id + ".jpg");

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View cellSelected) {
                String tablaOnClick = null;
                String campoOnClick = null;
                Log.d("click follow", "torneo id = " + followIndex.id + " is follower = " + cellSelected.getTag());
                if (followIndex.type == "torneo") {
                    tablaOnClick = "seguidores_torneos";
                    campoOnClick = "torneo_id";
                } else if (followIndex.type == "equipo") {
                    tablaOnClick = "seguidores_equipos_series";
                    campoOnClick = "equipo_serie_id";
                } else if (followIndex.type == "jugador") {
                    tablaOnClick = "seguidores_usuarios";
                    campoOnClick = "user_seguidor_id";
                }
                String UrlAddFollow = Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + followIndex.id + "&follow_table=" + tablaOnClick + "&follow_type=" + campoOnClick;
                new WsFollow().execute(UrlAddFollow);
                Log.d("URL CLICK", UrlAddFollow);
                if (cellSelected.getTag() == "1") {
                    cellSelected.setTag("0");
                    cellSelected.setBackgroundResource(R.drawable.estrella_desactivada);
                } else {
                    cellSelected.setTag("1");
                    cellSelected.setBackgroundResource(R.drawable.estrella_activada);
                }
            }
        });

        return row;
    }

    static class FollowData {
        ImageView imgIcon;
        Button imgFollow;
        TextView txtTitle;
    }

    private class WsFollow extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String UrlFollow = params[0];
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                url = new URL(UrlFollow);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("post on follow", result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }
}