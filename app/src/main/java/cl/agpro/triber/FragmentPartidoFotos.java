package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

/**
 * Created by yerkomora on 15-03-16.
 */
public class FragmentPartidoFotos extends Fragment {
    private Functions funciones;

    GetData getData;

    int partido_id;

    private View rootView;

    private Fotos fotos;
    private ArrayAdapterFotos arrayAdapterFotos;

    public FragmentPartidoFotos() {
    }

    public static FragmentPartidoFotos newInstance(int partido_id) {
        FragmentPartidoFotos fragment = new FragmentPartidoFotos();
        Bundle args = new Bundle();
        args.putInt(Functions.PARTIDO_ID, partido_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        funciones = new Functions(getActivity());

        partido_id = getArguments().getInt(Functions.PARTIDO_ID);

        getData = new GetData();
        getData.execute(partido_id);

        rootView = inflater.inflate(R.layout.fragment_partido_fotos, container, false);

        return rootView;
    }

    public void updateData() {
        getData = new GetData();
        getData.execute(partido_id, 1);
    }

    private class GetData extends AsyncTask<Integer, Void, String> {
        private Boolean update = false;

        @Override
        protected String doInBackground(Integer... params) {
            StringBuilder response = new StringBuilder();

            String partido_id = params[0].toString();

            if (params.length > 1 && params[1] == 1)
                update = true;

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/gallery.php?action=partidos&partido_id=" + partido_id);
                url = new URL(Functions.URL_WS + "/cms/ws/gallery.php?action=partidos&partido_id=" + partido_id);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            if (update)
                getResultsUpdate(result);
            else
                getResults(result);
        }

    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");

            fotos = new Fotos();
            Foto foto;

            if (dataArray.length() > 0) {

                JSONObject fotosArray = (JSONObject) dataArray.get(0);

                int galery_id = fotosArray.getInt("galeria_id");


                JSONArray fotosAr = fotosArray.getJSONArray("fotos");

                for (int i = 0; i < fotosAr.length(); i++) {
                    JSONObject row = fotosAr.getJSONObject(i);

                    foto = new Foto();

                    foto.id_galery = galery_id;
                    foto.id_foto = row.getInt("foto_id");
                    foto.id_fotografo = row.getInt("fotografo_id");
                    foto.nombre_fotografo = row.getString("fotografo_nombre");
                    foto.cant = row.getInt("total_coment");

                    fotos.add(foto);
                }
            }

            ListView ListViewFotos = (ListView) rootView.findViewById(R.id.listFotosPartidos);
            arrayAdapterFotos = new ArrayAdapterFotos(getActivity(), fotos);
            ListViewFotos.setAdapter(arrayAdapterFotos);

            ListViewFotos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Foto foto1 = (Foto) parent.getItemAtPosition(position);
                    int id_foto = foto1.id_foto;
                    int id_galery = foto1.id_galery;

                    Intent intent = new Intent(getContext(), ActivityFoto.class);
                    intent.putExtra(ActivityFoto.FOTO_ID, id_foto);
                    intent.putExtra(ActivityFoto.PARTIDO_ID, partido_id);
                    intent.putExtra(ActivityFoto.GALERY_ID, id_galery);
                    startActivity(intent);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getResultsUpdate(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");

            fotos.clear();

            Foto foto;
            if (dataArray.length() > 0) {

                JSONObject fotosArray = (JSONObject) dataArray.get(0);

                int galery_id = fotosArray.getInt("galeria_id");

                JSONArray fotosAr = fotosArray.getJSONArray("fotos");

                for (int i = 0; i < fotosAr.length(); i++) {
                    JSONObject row = fotosAr.getJSONObject(i);

                    foto = new Foto();

                    foto.id_galery = galery_id;
                    foto.id_foto = row.getInt("foto_id");
                    foto.id_fotografo = row.getInt("fotografo_id");
                    foto.nombre_fotografo = row.getString("fotografo_nombre");
                    foto.cant = row.getInt("total_coment");

                    fotos.add(foto);
                }

            }

            arrayAdapterFotos.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class ArrayAdapterFotos extends ArrayAdapter<Foto> {

        public ArrayAdapterFotos(Context context, List<Foto> fotos) {
            super(context, R.layout.listview_partido_fotos, fotos);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Foto foto = getItem(position);
            View view;

            if (foto.view == null) {
                LayoutInflater inflater = getActivity().getLayoutInflater();
                view = inflater.inflate(R.layout.listview_partido_fotos, parent, false);

                TextView nombre = (TextView) view.findViewById(R.id.txt_nombre_fotografo_partido);
                nombre.setText(foto.nombre_fotografo);

                TextView total = (TextView) view.findViewById(R.id.txt_cant_comentarios_foto);
                total.setText(String.valueOf(foto.cant));

                foto.imageView_fotografo = (ImageView) view.findViewById(R.id.img_fotografo_partido);
                foto.imageView_f = (ImageView) view.findViewById(R.id.img_foto_partido);

                new AsyncTaskFoto().execute(foto);
                new AsyncTaskFotoFotografo().execute(foto);

                foto.view = view;
            } else
                view = foto.view;

            return view;
        }

        private void setImageFoto(Foto foto) {
            if (foto.foto_f != null) {
                foto.imageView_f.setImageBitmap(foto.foto_f);
                arrayAdapterFotos.notifyDataSetChanged();
            }
        }

        private void setImageFotografo(Foto foto) {
            if (foto.foto_fotografo != null)
                foto.imageView_fotografo.setImageBitmap(foto.foto_fotografo);
        }

        private class AsyncTaskFoto extends AsyncTask<Foto, Void, Foto> {

            protected Foto doInBackground(Foto... params) {
                Foto foto = params[0];
                Log.d("FOTO ID ES: ", String.valueOf(foto.id_foto));
                foto.foto_f = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/galerias/" + foto.id_galery + "/" + foto.id_foto + ".jpg");
                Log.d("URL DE LA FOTO", Functions.URL_WS + "/imagenes/galerias/" + foto.id_galery + "/" + foto.id_foto + ".jpg");

                return foto;
            }

            @Override
            protected void onPostExecute(Foto foto) {
                setImageFoto(foto);
            }
        }

        private class AsyncTaskFotoFotografo extends AsyncTask<Foto, Void, Foto> {

            protected Foto doInBackground(Foto... params) {
                Foto foto = params[0];
                foto.foto_fotografo = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/perfiles/" + foto.id_fotografo + ".jpg");

                if (foto.foto_fotografo != null) {
                    foto.foto_fotografo = funciones.imagenRedonda(foto.foto_fotografo);
                }

                return foto;
            }

            @Override
            protected void onPostExecute(Foto foto) {
                setImageFotografo(foto);
            }
        }
    }
}
