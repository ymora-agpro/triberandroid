package cl.agpro.triber;

/**
 * Created by agpro on 26-02-16.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by agpro on 23-02-16.
 */
public class SeguimientosAdapter extends ArrayAdapter<Seguimiento> {
    Functions funciones;

    Context context;
    int layoutResourceId;
    Seguimiento data[] = null;

    public SeguimientosAdapter(Context context, int layoutResourceId, Seguimiento[] data) {
        super(context, layoutResourceId, data);

        funciones = new Functions(context);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        FollowData follow = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            follow = new FollowData();
            follow.imgIcon = (ImageView) row.findViewById(R.id.imagen);
            follow.txtTitle = (TextView) row.findViewById(R.id.nombre);
            follow.arrow = (ImageView) row.findViewById(R.id.arrowRight);
            follow.banner = (ImageView) row.findViewById(R.id.SeguimientosBanner);
            row.setTag(follow);
        } else {
            follow = (FollowData) row.getTag();
        }


        final Seguimiento followIndex = data[position];
        follow.txtTitle.setText(followIndex.title);

        //TIPO DE FILA
        final FollowData finalFollow = follow;
        if (followIndex.typeRow == "3") {
            follow.banner.setVisibility(View.INVISIBLE);
            follow.imgIcon.setVisibility(View.VISIBLE);
            follow.txtTitle.setVisibility(View.VISIBLE);
            follow.arrow.setVisibility(View.VISIBLE);
        } else if (followIndex.typeRow == "2") {
            follow.banner.setVisibility(View.VISIBLE);
            follow.imgIcon.setVisibility(View.INVISIBLE);
            follow.txtTitle.setVisibility(View.INVISIBLE);
            follow.arrow.setVisibility(View.INVISIBLE);
        }

        //URL PARA IMAGEN
        String tipoFoto = null;
        if (followIndex.type == "torneo") {
            if (followIndex.typeRow == "2") {
                tipoFoto = "publicidades/banner";
            } else {
                tipoFoto = "torneos";
            }
        } else if (followIndex.type == "equipo") {
            tipoFoto = "equipos";
        } else if (followIndex.type == "jugador") {
            tipoFoto = "perfiles";
        }

        new AsyncTask<String, Void, Bitmap>() {

            protected Bitmap doInBackground(String... params) {
                Log.d("URL DD IMAGE", params[0]);
                return funciones.downloadBitmap(params[0]);
            }

            @Override
            protected void onPostExecute(Bitmap mBitmap) {
                if (mBitmap != null) {
                    if (followIndex.typeRow == "2") {
                        finalFollow.banner.setImageBitmap(mBitmap);
                    } else {
                        finalFollow.imgIcon.setImageBitmap(funciones.imagenRedonda(mBitmap));
                    }
                } else {
                    Drawable myDrawable = null;
                    if (followIndex.type == "torneo") {
                        myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_tournament, null);
                    } else if (followIndex.type == "equipo") {
                        myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_team, null);
                    } else if (followIndex.type == "jugador") {
                        myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_profile, null);
                    }
                    finalFollow.imgIcon.setImageBitmap(((BitmapDrawable) myDrawable).getBitmap());
                }
            }
        }.execute(Functions.URL_WS + "/imagenes/" + tipoFoto + "/" + followIndex.id + ".jpg");

        return row;
    }

    static class FollowData {
        ImageView imgIcon;
        ImageView arrow;
        TextView txtTitle;
        ImageView banner;
    }
}