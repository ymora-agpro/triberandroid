package cl.agpro.triber;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by yerkomora on 24-09-15.
 */
public class FragmentPagerAdapterTutorial extends FragmentPagerAdapter {
    private static final int TOTAL = 3;

    public FragmentPagerAdapterTutorial(FragmentManager fm) {
        super(fm);
    }

    @Override
    public FragmentTutorial getItem(int position) {
        FragmentTutorial fragmentTutorial = new FragmentTutorial();
        fragmentTutorial.setPosition(position);
        return fragmentTutorial;
    }

    @Override
    public int getCount() {
        return TOTAL;
    }
}
