package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class ActivityCompetencia extends AppCompatActivity {
    public int competencia_id;
    Functions funciones;
    private getData getData;
    private getSave getSave;

    Boolean con_foto_competencia = null;
    ImageView foto_competencia;
    Bitmap competencia_bipmap = null;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_competencia);

        funciones = new Functions(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        Bundle bundle = getIntent().getExtras();
        competencia_id = bundle.getInt(Functions.COMPETENCIA_ID);

        Log.d(Functions.COMPETENCIA_ID, String.valueOf(competencia_id));


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_competencia);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getSave = new getSave();
                getSave.execute();

                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });

        getData = new getData();
        getData.execute(); //OBTENGO DATA DE WS
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_competencia, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                funciones.irAlMuro();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private class getData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/detalle_competencia.php?torneo=" + competencia_id + "&user=" + funciones.userId() + "&type=1");
                url = new URL(Functions.URL_WS + "/cms/ws/detalle_competencia.php?torneo=" + competencia_id + "&user=" + funciones.userId() + "&type=1");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("details");
            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            //RELLENO MI CLASE CON LA INFORMACION DEL WS

            JSONObject row = dataArray.getJSONObject(0);

            String com_id = String.valueOf(competencia_id);

            foto_competencia = (ImageView) findViewById(R.id.img_competencia);

            if (con_foto_competencia == null)
                new AsyncTaskFotoPartido().execute(com_id);
            else if (con_foto_competencia == true)
                foto_competencia.setImageBitmap(competencia_bipmap);

            TextView txt_name = (TextView) findViewById(R.id.txtNameCompetencia);
            txt_name.setText(row.getString("nombre_torneo"));
            TextView txt_follow = (TextView) findViewById(R.id.txtFollowCompetencia);
            txt_follow.setText(row.getString("followers"));

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_competencia);

            if (row.getInt("seguido") == 1) {
                fab.setImageResource(R.mipmap.ic_star_fixed);
                fab.setBackgroundTintList(ContextCompat.getColorStateList(getBaseContext(), R.color.rojo_claro));
            } else {
                fab.setImageResource(R.mipmap.ic_star_fixed_off);
                fab.setBackgroundTintList(ContextCompat.getColorStateList(getBaseContext(), R.color.gris));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private class getSave extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + competencia_id + "&follow_table=seguidores_torneos&follow_type=torneo_id");
                url = new URL(Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + competencia_id + "&follow_table=seguidores_torneos&follow_type=torneo_id");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsSave(result);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public void getResultsSave(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("click");
            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            //RELLENO MI CLASE CON LA INFORMACION DEL WS

            JSONObject row = dataArray.getJSONObject(0);

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_competencia);

            if (row.getString("action").equals("eliminado")) {
                fab.setImageResource(R.mipmap.ic_star_fixed_off);
                fab.setBackgroundTintList(ContextCompat.getColorStateList(getBaseContext(), R.color.gris));
            } else {
                fab.setImageResource(R.mipmap.ic_star_fixed);
                fab.setBackgroundTintList(ContextCompat.getColorStateList(getBaseContext(), R.color.rojo_claro));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Fragment para los detalles de la competencia
     */
    public static class FragmentDetalles extends Fragment {

        Functions funciones;
        private getData getData;
        private View rootView;

        public FragmentDetalles() {
        }

        public static FragmentDetalles newInstance(int competencia_id) {
            FragmentDetalles fragment = new FragmentDetalles();
            Bundle args = new Bundle();
            args.putInt(Functions.COMPETENCIA_ID, competencia_id);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            funciones = new Functions(getActivity());

            getData = new getData();
            getData.execute(); //OBTENGO DATA DE WS

            rootView = inflater.inflate(R.layout.fragment_tab_competencias_detalle, container, false);
            //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            //textView.setText(getString(R.string.section_format, getArguments().getInt(COMPETENCIA_ID)));
            return rootView;
        }

        private class getData extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                URL url = null;
                try {
                    Log.d("URL ", Functions.URL_WS + "/cms/ws/detalle_competencia.php?torneo=" + getArguments().getInt(Functions.COMPETENCIA_ID) + "&user=" + funciones.userId() + "&type=1");
                    url = new URL(Functions.URL_WS + "/cms/ws/detalle_competencia.php?torneo=" + getArguments().getInt(Functions.COMPETENCIA_ID) + "&user=" + funciones.userId() + "&type=1");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResults(result);
            }

            @Override
            protected void onPreExecute() {

            }

            @Override
            protected void onProgressUpdate(Void... values) {

            }
        }

        public void getResults(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("details");

                JSONObject row = dataArray.getJSONObject(0);

                TextView txt_type = (TextView) rootView.findViewById(R.id.txtTipoCompetenciaDetalle);
                txt_type.setText(row.getString("tipo"));
                TextView txt_teams = (TextView) rootView.findViewById(R.id.txtTeamsCompetenciasDetalle);
                txt_teams.setText(row.getString("cant"));
                TextView txt_gender = (TextView) rootView.findViewById(R.id.txtGenderCompetenciasDetalle);
                txt_gender.setText(row.getString("genero"));
                TextView txt_age = (TextView) rootView.findViewById(R.id.txtAgeCompetenciasDetalle);
                txt_age.setText(row.getString("edad"));
                TextView txt_date_ini = (TextView) rootView.findViewById(R.id.txtDateIniCompetenciasDetalle);
                txt_date_ini.setText(row.getString("inicio"));
                TextView txt_date_fin = (TextView) rootView.findViewById(R.id.txtDateFinCompetenciasDetalle);
                txt_date_fin.setText(row.getString("fin"));
                TextView txt_org = (TextView) rootView.findViewById(R.id.txtOrgCompetenciasDetalle);
                txt_org.setText(row.getString("nombre_org"));
                TextView txt_adm_name = (TextView) rootView.findViewById(R.id.txtAdmNameCompetenciasDetalle);
                txt_adm_name.setText(row.getString("nombre_user"));
                TextView txt_adm_email = (TextView) rootView.findViewById(R.id.txtAdmEmailCompetenciasDetalle);
                txt_adm_email.setText(row.getString("user_email"));
                TextView txt_adm_phone = (TextView) rootView.findViewById(R.id.txtAdmPhoneCompetenciasDetalle);
                txt_adm_phone.setText(row.getString("user_telefono"));
                TextView txt_descrip = (TextView) rootView.findViewById(R.id.txtDescripcionCompetenciasDetalle);
                txt_descrip.setText(row.getString("torneo_descripcion"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Fragment para los punteros de la competencia
     */
    public static class FragmentPunteros extends Fragment {

        private Functions funciones;
        private View rootView;

        public FragmentPunteros() {
        }

        public static FragmentPunteros newInstance(int equipo_id) {
            FragmentPunteros fragment = new FragmentPunteros();
            Bundle args = new Bundle();
            args.putInt(Functions.COMPETENCIA_ID, equipo_id);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            funciones = new Functions(getActivity());

            GetData getData = new GetData();
            getData.execute(getArguments().getInt(Functions.COMPETENCIA_ID)); //OBTENGO DATA DE WS

            rootView = inflater.inflate(R.layout.fragment_tab_competencias_punteros, container, false);
            return rootView;
        }

        private class GetData extends AsyncTask<Integer, Void, String> {
            @Override
            protected String doInBackground(Integer... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                String competencia_id = params[0].toString();

                URL url = null;
                try {
                    Log.d("URL ", Functions.URL_WS + "/cms/ws/detalle_competencia_equipos.php?torneo=" + competencia_id + "&type=0");
                    url = new URL(Functions.URL_WS + "/cms/ws/detalle_competencia_equipos.php?torneo=" + competencia_id + "&type=0");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResults(result);
            }

        }

        public void getResults(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("matches");

                //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
                Punteros punteros = new Punteros();

                //RELLENO MI CLASE CON LA INFORMACION DEL WS
                Puntero puntero;
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject row = dataArray.getJSONObject(i);

                    puntero = new Puntero();

                    puntero.id = row.getInt("equipo_id");
                    puntero.id_serie = row.getInt("serie_id");
                    puntero.equipo_nombre = row.getString("nombre");
                    puntero.serie_nombre = row.getString("nombre_serie");
                    puntero.partidos = row.getString("partidos");
                    puntero.diferencia = row.getString("diferencia");
                    puntero.puntos = row.getString("puntos");

                    punteros.add(puntero);
                }

                ListView ListViewPunteros = (ListView) rootView.findViewById(R.id.listPunterosCompetencias);
                ArrayAdapterPunteros arrayAdapterPunteros = new ArrayAdapterPunteros(getActivity(), punteros);
                ListViewPunteros.setAdapter(arrayAdapterPunteros);

                ListViewPunteros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Puntero puntero1 = (Puntero) parent.getItemAtPosition(position);


                        Log.d(Functions.SERIE_ID, String.valueOf(puntero1.id_serie));

                        Intent intent = new Intent(getContext(), ActivityEquipo.class);
                        intent.putExtra(Functions.SERIE_ID, puntero1.id_serie);
                        startActivity(intent);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private class ArrayAdapterPunteros extends ArrayAdapter<Puntero> {

            private ImageView foto;

            public ArrayAdapterPunteros(Context context, List<Puntero> punteros) {
                super(context, R.layout.listview_puntero, punteros);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                LayoutInflater inflater = getActivity().getLayoutInflater();
                view = inflater.inflate(R.layout.listview_puntero, parent, false);

                if ((position + 1) % 2 == 1)
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_1));
                else
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_2));

                Puntero puntero = getItem(position);

                puntero.imageView = (ImageView) view.findViewById(R.id.foto);

                if (puntero.con_foto == null)
                    new AsyncTaskFoto().execute(puntero);
                else if (puntero.con_foto == true)
                    puntero.imageView.setImageBitmap(puntero.foto);

                TextView equipo = (TextView) view.findViewById(R.id.namePuntero);
                equipo.setText(puntero.equipo_nombre);

                TextView serie = (TextView) view.findViewById(R.id.seriePuntero);
                serie.setText(puntero.serie_nombre);

                TextView partido = (TextView) view.findViewById(R.id.partidosPuntero);
                partido.setText(puntero.partidos);

                TextView diferencia = (TextView) view.findViewById(R.id.diferenciaPuntero);
                diferencia.setText(puntero.diferencia);

                TextView puntos = (TextView) view.findViewById(R.id.puntosPuntero);
                puntos.setText(puntero.puntos);

                return view;
            }

            private void setImage(Puntero puntero) {
                if (puntero.foto != null)
                    puntero.imageView.setImageBitmap(puntero.foto);
            }

            private class AsyncTaskFoto extends AsyncTask<Puntero, Void, Puntero> {

                protected Puntero doInBackground(Puntero... params) {
                    Puntero puntero = params[0];
                    puntero.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/equipos/" + puntero.id + ".jpg");

                    if (puntero.foto != null) {
                        puntero.con_foto = true;
                        puntero.foto = funciones.imagenRedonda(puntero.foto);
                    } else puntero.con_foto = false;

                    return puntero;
                }

                @Override
                protected void onPostExecute(Puntero puntero) {
                    setImage(puntero);
                }
            }
        }
    }

    /**
     * Fragment para los goleadores de la competencia
     */
    public static class FragmentGoleadores extends Fragment {
        private Functions funciones;
        private View rootView;

        public FragmentGoleadores() {
        }

        public static FragmentGoleadores newInstance(int equipo_id) {
            FragmentGoleadores fragment = new FragmentGoleadores();
            Bundle args = new Bundle();
            args.putInt(Functions.COMPETENCIA_ID, equipo_id);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            funciones = new Functions(getActivity());

            GetData getData = new GetData();
            getData.execute(getArguments().getInt(Functions.COMPETENCIA_ID)); //OBTENGO DATA DE WS

            rootView = inflater.inflate(R.layout.fragment_tab_competencias_goleadores, container, false);
            return rootView;
        }

        private class GetData extends AsyncTask<Integer, Void, String> {
            @Override
            protected String doInBackground(Integer... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                String competencia_id = params[0].toString();

                URL url = null;
                try {
                    Log.d("URL ", Functions.URL_WS + "/cms/ws/detalle_competencia_jugadores.php?torneo=" + competencia_id + "&type=0");
                    url = new URL(Functions.URL_WS + "/cms/ws/detalle_competencia_jugadores.php?torneo=" + competencia_id + "&type=0");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResults(result);
            }

        }

        public void getResults(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("players");

                //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
                Goleadores goleadores = new Goleadores();

                //RELLENO MI CLASE CON LA INFORMACION DEL WS
                Goleador goleador;
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject row = dataArray.getJSONObject(i);

                    goleador = new Goleador();

                    goleador.id = row.getInt("id");
                    goleador.nombre = row.getString("nombre");
                    goleador.equipo = row.getString("equipo_nombre");
                    goleador.goles = row.getString("goles");
                    goleador.partidos = row.getString("partidos");

                    goleadores.add(goleador);
                }

                ListView ListViewGoleadores = (ListView) rootView.findViewById(R.id.listGoleadoresCompetencias);
                ArrayAdapterGoleadores arrayAdapterJugadores = new ArrayAdapterGoleadores(getActivity(), goleadores);
                ListViewGoleadores.setAdapter(arrayAdapterJugadores);


                ListViewGoleadores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Goleador goleador1 = (Goleador) parent.getItemAtPosition(position);


                        Log.d(ActivityJugador.JUGADOR_ID, String.valueOf(goleador1.id));

                        Intent intent = new Intent(getContext(), ActivityJugador.class);
                        intent.putExtra(ActivityJugador.JUGADOR_ID, goleador1.id);
                        startActivity(intent);

                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private class ArrayAdapterGoleadores extends ArrayAdapter<Goleador> {
            private ImageView foto;

            public ArrayAdapterGoleadores(Context context, List<Goleador> goleadores) {
                super(context, R.layout.listview_goleador, goleadores);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                LayoutInflater inflater = getActivity().getLayoutInflater();
                view = inflater.inflate(R.layout.listview_goleador, parent, false);

                if ((position + 1) % 2 == 1)
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_1));
                else
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_2));

                Goleador goleador = getItem(position);

                goleador.imageView = (ImageView) view.findViewById(R.id.foto);

                if (goleador.con_foto == null)
                    new AsyncTaskFoto().execute(goleador);
                else if (goleador.con_foto == true)
                    goleador.imageView.setImageBitmap(goleador.foto);

                TextView nombre = (TextView) view.findViewById(R.id.nameGoleador);
                nombre.setText(goleador.nombre);

                TextView equipo = (TextView) view.findViewById(R.id.teamGoleador);
                equipo.setText(goleador.equipo);

                TextView partido = (TextView) view.findViewById(R.id.partidosGoleador);
                partido.setText(goleador.partidos);

                TextView gol = (TextView) view.findViewById(R.id.golesGoleador);
                gol.setText(goleador.goles);

                return view;
            }

            private void setImage(Goleador goleador) {
                if (goleador.foto != null)
                    goleador.imageView.setImageBitmap(goleador.foto);
            }

            private class AsyncTaskFoto extends AsyncTask<Goleador, Void, Goleador> {

                protected Goleador doInBackground(Goleador... params) {
                    Goleador goleador = params[0];
                    goleador.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/perfiles/" + goleador.id + ".jpg");

                    if (goleador.foto != null) {
                        goleador.con_foto = true;
                        goleador.foto = funciones.imagenRedonda(goleador.foto);
                    } else goleador.con_foto = false;

                    return goleador;
                }

                @Override
                protected void onPostExecute(Goleador goleador) {
                    setImage(goleador);
                }
            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return FragmentDetalles.newInstance(competencia_id);
                case 1:
                    return FragmentPartidos.newInstanceCompetencia(competencia_id);
                case 2:
                    return FragmentPunteros.newInstance(competencia_id);
                case 3:
                    return FragmentGoleadores.newInstance(competencia_id);
            }

            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Detalle";
                case 1:
                    return "Partidos";
                case 2:
                    return "Punteros";
                case 3:
                    return "Goleadores";
            }
            return null;
        }
    }

    private void setImage(Bitmap bitmap) {
        if (bitmap != null)
            foto_competencia.setImageBitmap(bitmap);
    }

    private class AsyncTaskFotoPartido extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... params) {
            String id = params[0];
            competencia_bipmap = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/torneos/" + id + ".jpg");


            if (competencia_bipmap != null) {
                con_foto_competencia = true;
                competencia_bipmap = funciones.imagenRedonda(competencia_bipmap);
            } else con_foto_competencia = false;

            return competencia_bipmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            setImage(bitmap);
        }
    }
}
