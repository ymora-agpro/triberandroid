package cl.agpro.triber;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.regex.Pattern;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentBienvenidaEquipos.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentBienvenidaEquipos#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentBienvenidaEquipos extends Fragment {
    Functions funciones;
    private getData getData;

    private Follows follows, follows_todos;
    private FollowAdapterDynamic adapterDynamic;

    private ImageView img;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentBienvenidaEquipos() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ActivityBienvenidaEquipos.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentBienvenidaEquipos newInstance(String param1, String param2) {
        FragmentBienvenidaEquipos fragment = new FragmentBienvenidaEquipos();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bienvenida_equipos, container, false);

        img = (ImageView) view.findViewById(R.id.img);

        Log.d("paso", "2");
        funciones = new Functions(getActivity());

        getData = new getData();
        getData.execute(); //OBTENGO DATA DE WS

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        getData.cancel(true);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void setData(String searchText) {

        if (img.getVisibility() == View.VISIBLE)
            img.setVisibility(View.GONE);

        follows.clear();

        for (Follow follow : follows_todos)
            if (Pattern.compile(Pattern.quote(searchText), Pattern.CASE_INSENSITIVE).matcher(follow.title).find())
                follows.add(follow);

        adapterDynamic.notifyDataSetChanged();
    }

    public void closeData() {
        img.setVisibility(View.VISIBLE);
    }

    private class getData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                int paisId = getActivity().getIntent().getExtras().getInt("pais");
                String sURL = Functions.URL_WS + "/cms/ws/list_categories.php?action=equipo&user=" + funciones.userId() + "&pais=" + paisId + "&busqueda=1";
                Log.d("WebService",sURL);
                url = new URL(sURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("equipo");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            follows = new Follows();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);
                int id = row.getInt("serie_id");
                int equipo_id = row.getInt("id");
                String nombre = row.getString("nombre") + "\n" + row.getString("serie");
                String follower = row.getString("follower");
                follows.add(new Follow("equipo", equipo_id, id, nombre, follower));
            }

            follows_todos = new Follows();
            follows_todos.addAll(follows);

            ListView lv = (ListView) getActivity().findViewById(R.id.listView2);
            adapterDynamic = new FollowAdapterDynamic(getActivity(), R.layout.item_follow, follows);
            lv.setAdapter(adapterDynamic);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
