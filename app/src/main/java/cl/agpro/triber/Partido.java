package cl.agpro.triber;

/**
 * Created by yerkomora on 10-03-16.
 */
public class Partido {
    public int id;

    public Equipo[] equipo;
    public String fecha;
    public Torneo torneo;
    public Cancha cancha;

    public Boolean jugado;

    public Partido() {
        equipo = new Equipo[2];
        torneo = new Torneo();
        cancha = new Cancha();

        jugado = null;
    }
}