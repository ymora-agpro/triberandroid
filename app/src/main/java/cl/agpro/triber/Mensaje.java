package cl.agpro.triber;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by franciscoriveros on 14-03-16.
 */
public class Mensaje {
    public int id;
    public String invita, descripcion, nombre, fecha;

    public Bitmap foto = null;
    public Boolean con_foto = null;
    public ImageView imageView;
}
