package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

/**
 * Created by yerkomora on 15-03-16.
 */
public class FragmentPartidoNotas extends Fragment {
    private Functions funciones;

    private GetData getData;

    private int partido_id;

    private View rootView;

    private Notas notas;
    private ArrayAdapterNotas arrayAdapterNotas;

    public FragmentPartidoNotas() {
    }

    public static FragmentPartidoNotas newInstance(int partido_id) {
        FragmentPartidoNotas fragment = new FragmentPartidoNotas();
        Bundle args = new Bundle();
        args.putInt(Functions.PARTIDO_ID, partido_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        funciones = new Functions(getActivity());

        partido_id = getArguments().getInt(Functions.PARTIDO_ID);

        getData = new GetData();
        getData.execute(partido_id);

        rootView = inflater.inflate(R.layout.fragment_partido_notas, container, false);

        return rootView;
    }

    public void updateData() {
        getData = new GetData();
        getData.execute(partido_id, 1);
    }

    private class GetData extends AsyncTask<Integer, Void, String> {
        private Boolean update = false;

        @Override
        protected String doInBackground(Integer... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            String partido_id = params[0].toString();

            if (params.length > 1 && params[1] == 1)
                update = true;

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/notas.php?action=list&partido_id=" + partido_id);
                url = new URL(Functions.URL_WS + "/cms/ws/notas.php?action=list&partido_id=" + partido_id);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            if (update)
                getResultsUpdate(result);
            else
                getResults(result);
        }

    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");

            notas = new Notas();
            Nota nota;

            if (dataArray.length() > 0) {

                JSONObject fotosArray = (JSONObject) dataArray.get(0);

                JSONArray fotosAr = fotosArray.getJSONArray("notas");

                for (int i = 0; i < fotosAr.length(); i++) {
                    JSONObject row = fotosAr.getJSONObject(i);

                    nota = new Nota();

                    nota.nota_id = row.getInt("nota_id");
                    nota.partido_id = row.getInt("partido_id");
                    nota.usuario_id = row.getInt("usuario_id");
                    nota.name_notero = row.getString("name_notero");
                    nota.titulo = row.getString("titulo");
                    nota.descripcion = row.getString("texto");
                    nota.cant = row.getInt("total_comentarios");

                    notas.add(nota);
                }

            }

            ListView ListViewNotas = (ListView) rootView.findViewById(R.id.listNotasPartidos);
            arrayAdapterNotas = new ArrayAdapterNotas(getActivity(), notas);
            ListViewNotas.setAdapter(arrayAdapterNotas);

            ListViewNotas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Nota nota1 = (Nota) parent.getItemAtPosition(position);
                    int id_nota = nota1.nota_id;
                    int id_partido = nota1.partido_id;

                    Intent intent = new Intent(getContext(), ActivityNota.class);
                    intent.putExtra(ActivityNota.NOTA_ID, id_nota);
                    intent.putExtra(ActivityNota.PARTIDO_ID, id_partido);
                    startActivity(intent);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getResultsUpdate(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");

            notas.clear();

            Nota nota;

            if (dataArray.length() > 0) {

                JSONObject fotosArray = (JSONObject) dataArray.get(0);

                JSONArray fotosAr = fotosArray.getJSONArray("notas");

                for (int i = 0; i < fotosAr.length(); i++) {
                    JSONObject row = fotosAr.getJSONObject(i);

                    nota = new Nota();

                    nota.nota_id = row.getInt("nota_id");
                    nota.partido_id = row.getInt("partido_id");
                    nota.usuario_id = row.getInt("usuario_id");
                    nota.name_notero = row.getString("name_notero");
                    nota.titulo = row.getString("titulo");
                    nota.descripcion = row.getString("texto");
                    nota.cant = row.getInt("total_comentarios");

                    notas.add(nota);
                }

            }

            arrayAdapterNotas.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class ArrayAdapterNotas extends ArrayAdapter<Nota> {

        public ArrayAdapterNotas(Context context, List<Nota> notas) {
            super(context, R.layout.listview_partido_notas, notas);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = getActivity().getLayoutInflater();
                convertView = inflater.inflate(R.layout.listview_partido_notas, parent, false);

                Nota nota = getItem(position);

                ImageView imageView = (ImageView) convertView.findViewById(R.id.img_notero_partido);

                if (nota.con_foto == null)
                    new AsyncTaskFoto(imageView).execute(nota);
                else if (nota.con_foto == true)
                    imageView.setImageBitmap(nota.foto);


                TextView nombre = (TextView) convertView.findViewById(R.id.txt_nombre_notero_partido);
                nombre.setText(nota.name_notero);

                TextView titulo = (TextView) convertView.findViewById(R.id.txt_titulo_nota);
                titulo.setText(nota.titulo);

                TextView descripcion = (TextView) convertView.findViewById(R.id.txt_descripcion_nota);
                descripcion.setText(nota.descripcion);

                TextView serie = (TextView) convertView.findViewById(R.id.txt_cant_comentarios_nota);
                serie.setText(String.valueOf(nota.cant));
            }

            return convertView;
        }

        private class AsyncTaskFoto extends AsyncTask<Nota, Void, Bitmap> {
            private ImageView imageView;

            public AsyncTaskFoto(ImageView imageView) {
                this.imageView = imageView;
            }

            protected Bitmap doInBackground(Nota... params) {
                Nota nota = params[0];
                nota.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/perfiles/" + "/" + nota.usuario_id + ".jpg");

                if (nota.foto != null) {
                    nota.con_foto = true;
                    nota.foto = funciones.imagenRedonda(nota.foto);
                } else nota.con_foto = false;

                return nota.foto;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if (bitmap != null)
                    imageView.setImageBitmap(bitmap);
            }
        }
    }
}
