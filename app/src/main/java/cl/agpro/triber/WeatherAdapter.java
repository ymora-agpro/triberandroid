package cl.agpro.triber;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by agpro on 23-02-16.
 */
public class WeatherAdapter extends ArrayAdapter<Weather> {

    Context context;
    int layoutResourceId;
    Weather data[] = null;

    public WeatherAdapter(Context context, int layoutResourceId, Weather[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        WeatherHolder holder = null;
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new WeatherHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.imagen);
            holder.txtTitle = (TextView)row.findViewById(R.id.nombre);
            row.setTag(holder);
        }
        else
        {
            holder = (WeatherHolder)row.getTag();
        }

        Button btn = (Button)row.findViewById(R.id.imagenStar);
        Log.d("BTN QLAO", String.valueOf(btn));

        Weather weather = data[position];
        holder.txtTitle.setText(weather.title);
        btn.setTag(weather.title);
        holder.imgIcon.setImageResource(weather.icon);

        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View cellSelected) {
                Log.d("CLICK CELL", "AL FIN FUNCIONA LA WEA" + cellSelected.getTag());
                Drawable btnImage = cellSelected.getBackground();
                Drawable resourceImage = ResourcesCompat.getDrawable(context.getResources(), R.drawable.estrella_activada, null);
                Bitmap bitmap = ((BitmapDrawable)btnImage).getBitmap();
                Bitmap bitmap2 = ((BitmapDrawable)resourceImage).getBitmap();
                if (bitmap==bitmap2){
                    cellSelected.setBackgroundResource(R.drawable.estrella_desactivada);
                }else{
                    cellSelected.setBackgroundResource(R.drawable.estrella_activada);
                }
            }
        });

        return row;
    }

    static class WeatherHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
    }
}