package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class ActivityEditProfile extends AppCompatActivity {

    Functions funciones;
    private getData getData;
    private getDatosJugador getDatosJugador;
    private getDatosFotosJugador getDatosFotosJugador;
    private getDatosNotasJugador getDatosNotasJugador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        funciones = new Functions(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarEditProfile);
        setSupportActionBar(toolbar);

        getDatosJugador = new getDatosJugador();
        getDatosJugador.execute();

        getDatosFotosJugador = new getDatosFotosJugador();
        getDatosFotosJugador.execute();

        getDatosNotasJugador = new getDatosNotasJugador();
        getDatosNotasJugador.execute();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_edit_profile);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                goEditProfile(view);

                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Datos

        getData = new getData();
        getData.execute();

        // Foto

        Usuario usuario = new Usuario();
        usuario.id = funciones.userId();
        usuario.imageView = (ImageView) findViewById(R.id.imagenPerfil);

        if (usuario.con_foto == null)
            new AsyncTaskFoto().execute(usuario);
        else if (usuario.con_foto == true)
            usuario.imageView.setImageBitmap(usuario.foto);
    }

    public void goEditProfile(View v) {
        Intent intent = new Intent(getBaseContext(), ActivityEditMyProfile.class);
        startActivity(intent);
    }

    private class getData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/detalle_usuario.php?usuario=" + funciones.userId() + "&user=" + funciones.userId());
                url = new URL(Functions.URL_WS + "/cms/ws/detalle_usuario.php?usuario=" + funciones.userId() + "&user=" + funciones.userId());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("details");

            JSONObject row = dataArray.getJSONObject(0);//

            TextView txt_name = (TextView) findViewById(R.id.txtNameEditProfile);
            txt_name.setText(row.getString("nombre"));
            TextView txt_follow = (TextView) findViewById(R.id.txtFollowEditProfile);
            txt_follow.setText(row.getString("followers"));


            TextView txt_rol = (TextView) findViewById(R.id.txtRol);
            txt_rol.setText(row.getString("rol"));
            TextView txt_rut = (TextView) findViewById(R.id.txtRut);
            txt_rut.setText(row.getString("rut"));
            TextView txt_address = (TextView) findViewById(R.id.txtAddress);
            txt_address.setText(row.getString("direccion"));
            TextView txt_email = (TextView) findViewById(R.id.txtEmail);
            txt_email.setText(row.getString("user_email"));
            TextView txt_phone = (TextView) findViewById(R.id.txtPhone);
            txt_phone.setText(row.getString("user_telefono"));
            TextView txt_age = (TextView) findViewById(R.id.txtAge);
            txt_age.setText(row.getString("edad"));
            TextView txt_weight = (TextView) findViewById(R.id.txtWeight);
            txt_weight.setText(row.getString("user_peso"));
            TextView txt_height = (TextView) findViewById(R.id.txtHeight);
            txt_height.setText(row.getString("user_altura"));
            TextView txt_shoes = (TextView) findViewById(R.id.txtShoes);
            txt_shoes.setText(row.getString("zapato"));
            TextView txt_glove = (TextView) findViewById(R.id.txtGlove);
            txt_glove.setText(row.getString("guante"));
            TextView txt_tshirt = (TextView) findViewById(R.id.txtTshirt);
            txt_tshirt.setText(row.getString("camiseta"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Datos
     */
    private class getDatosJugador extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/jugadores_equipos.php?user=" + funciones.userId() + "&action=jugador_estadisticas");
                url = new URL(Functions.URL_WS + "/cms/ws/jugadores_equipos.php?user=" + funciones.userId() + "&action=jugador_estadisticas");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsDatosJugador(result);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public void getResultsDatosJugador(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");
            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            //RELLENO MI CLASE CON LA INFORMACION DEL WS

            int n = dataArray.length();

            if (n > 0) {

                JSONObject datosArray = (JSONObject) dataArray.get(0);

                JSONArray forAr = datosArray.getJSONArray("partidos");
                JSONArray datosAr = datosArray.getJSONArray("general");
                JSONObject row = datosAr.getJSONObject(0);

                TextView fecha = (TextView) findViewById(R.id.text_fecha_jugador);
                fecha.setText(row.getString("fecha_union"));

                TextView partidos = (TextView) findViewById(R.id.txt_patidos_jugadores);
                partidos.setText(row.getString("total_partidos"));

                TextView victoria = (TextView) findViewById(R.id.txt_victoria_jugador);
                victoria.setText(row.getString("total_victorias"));

                TextView destacado = (TextView) findViewById(R.id.txt_destacado_jugador);
                destacado.setText(row.getString("total_destacado"));


                DetalleJugadores detalleJugadores = new DetalleJugadores();
                DetalleJugador detalleJugador;

                int height = 0;
                for (int i = 0; i < forAr.length(); i++) {
                    JSONObject f = forAr.getJSONObject(i);

                    String tipo = f.getString("tipo");
                    Log.d("ELTIPOES:", tipo);

                    detalleJugador = new DetalleJugador();

                    detalleJugador.tipo = tipo;

                    if (tipo.equals("header")) {

                        detalleJugador.team_id = f.getInt("equipo_id");
                        detalleJugador.name_team = f.getString("equipo");
                        detalleJugador.date_team = f.getString("unido");

                        detalleJugadores.add(detalleJugador);
                        height += 60;

                    } else {

                        detalleJugador.posicion = f.getString("posicion");
                        detalleJugador.jugados = f.getString("partidos");
                        detalleJugador.victorias = f.getString("victorias");
                        detalleJugador.destacado = f.getString("destacado");
                        detalleJugador.goles = f.getString("goles");
                        detalleJugador.tiros = f.getString("tiros");
                        detalleJugador.bloqueos = f.getString("bloqueos");
                        detalleJugador.minutos = f.getString("minutos");
                        detalleJugador.verdes = f.getString("verdes");
                        detalleJugador.amarillas = f.getString("amarillas");
                        detalleJugador.rojas = f.getString("rojas");

                        detalleJugadores.add(detalleJugador);

                        height += 340;
                    }

                    ListView ListViewDetalle = (ListView) findViewById(R.id.listViewDetalleJugador);
                    ArrayAdapterDetalle arrayAdapterDetalle = new ArrayAdapterDetalle(this, detalleJugadores);
                    ListViewDetalle.setAdapter(arrayAdapterDetalle);

                    ListViewDetalle.getLayoutParams().height = funciones.getPixeles(height);
                }
            } else {
                RelativeLayout rel = (RelativeLayout) findViewById(R.id.relativeEstadisticaJugador);
                rel.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }/**/
    }


    private class ArrayAdapterDetalle extends ArrayAdapter<DetalleJugador> {

        private ImageView foto;

        public ArrayAdapterDetalle(Context context, List<DetalleJugador> detalleJugadores) {
            super(context, R.layout.listview_detalles_jugador, detalleJugadores);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_detalles_jugador, parent, false);

            DetalleJugador detalleJugador = getItem(position);

            String tipo = detalleJugador.tipo;

            if (tipo.equals("header")) {
                RelativeLayout relative = (RelativeLayout) view.findViewById(R.id.relative2Detalle);
                relative.setVisibility(view.GONE);

                detalleJugador.imageView = (ImageView) view.findViewById(R.id.img_team_jugador);

                if (detalleJugador.con_foto == null)
                    new AsyncTaskFoto().execute(detalleJugador);
                else if (detalleJugador.con_foto == true)
                    detalleJugador.imageView.setImageBitmap(detalleJugador.foto);

                TextView name = (TextView) view.findViewById(R.id.txt_name_team_jugador);
                name.setText(detalleJugador.name_team);

                TextView date = (TextView) view.findViewById(R.id.txt_fecha_team_jugador);
                date.setText(detalleJugador.date_team);

            } else {
                RelativeLayout relative = (RelativeLayout) view.findViewById(R.id.relative1Detalle);
                relative.setVisibility(view.GONE);

                TextView posicion = (TextView) view.findViewById(R.id.txt_position_jugador);
                posicion.setText(detalleJugador.posicion);

                TextView jugados = (TextView) view.findViewById(R.id.txt_partidos_jugados_jugador);
                jugados.setText(detalleJugador.jugados);

                TextView victorias = (TextView) view.findViewById(R.id.txt_victoria_detalle_jugador);
                victorias.setText(detalleJugador.victorias);

                TextView destacado = (TextView) view.findViewById(R.id.txt_destacado_detalle_jugador);
                destacado.setText(detalleJugador.destacado);

                TextView goles = (TextView) view.findViewById(R.id.txt_goles_detalle_jugador);
                goles.setText(detalleJugador.goles);

                TextView tiros = (TextView) view.findViewById(R.id.txt_shot_detalle_jugador);
                tiros.setText(detalleJugador.tiros);

                TextView bloqueos = (TextView) view.findViewById(R.id.txt_block_detalle_jugador);
                bloqueos.setText(detalleJugador.bloqueos);

                TextView minutos = (TextView) view.findViewById(R.id.txt_minute_detalle_jugador);
                minutos.setText(detalleJugador.minutos);

                TextView verdes = (TextView) view.findViewById(R.id.txt_verde_detalle_jugador);
                verdes.setText(detalleJugador.verdes);

                TextView amarillas = (TextView) view.findViewById(R.id.txt_amarillo_detalle_jugador);
                amarillas.setText(detalleJugador.amarillas);

                TextView rojas = (TextView) view.findViewById(R.id.txt_rojo_detalle_jugador);
                rojas.setText(detalleJugador.rojas);

            }


            return view;
        }

        private void setImage(DetalleJugador detalleJugador) {
            if (detalleJugador.foto != null)
                detalleJugador.imageView.setImageBitmap(detalleJugador.foto);
        }

        private class AsyncTaskFoto extends AsyncTask<DetalleJugador, Void, DetalleJugador> {

            protected DetalleJugador doInBackground(DetalleJugador... params) {
                DetalleJugador detalleJugador = params[0];
                detalleJugador.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/equipos/" + detalleJugador.team_id + ".jpg");

                if (detalleJugador.foto != null) {
                    detalleJugador.con_foto = true;
                    detalleJugador.foto = funciones.imagenRedonda(detalleJugador.foto);
                } else detalleJugador.con_foto = false;

                return detalleJugador;
            }

            @Override
            protected void onPostExecute(DetalleJugador detalleJugador) {
                setImage(detalleJugador);
            }
        }
    }

    /**
     * Imagen de Perfil
     */
    private void setImage(Usuario usuario) {
        if (usuario.foto != null)
            usuario.imageView.setImageBitmap(usuario.foto);
    }

    private class AsyncTaskFoto extends AsyncTask<Usuario, Void, Usuario> {

        protected Usuario doInBackground(Usuario... params) {
            Usuario usuario = params[0];
            usuario.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/perfiles/" + usuario.id + ".jpg");

            if (usuario.foto != null) {
                usuario.con_foto = true;
                usuario.foto = funciones.imagenRedonda(usuario.foto);
            } else usuario.con_foto = false;

            return usuario;
        }

        @Override
        protected void onPostExecute(Usuario usuario) {
            setImage(usuario);
        }
    }

    /**
     * Fotos
     */
    private class getDatosFotosJugador extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/jugadores_equipos.php?user=" + funciones.userId() + "&action=fotografo");
                url = new URL(Functions.URL_WS + "/cms/ws/jugadores_equipos.php?user=" + funciones.userId() + "&action=fotografo");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsDatosFotosJugador(result);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public void getResultsDatosFotosJugador(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("photo");
            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            //RELLENO MI CLASE CON LA INFORMACION DEL WS

            JSONObject row1 = dataArray.getJSONObject(0);

            int n = row1.getInt("suc");

            if (n > 0) {

                DetalleJugadores detalleJugadores = new DetalleJugadores();
                DetalleJugador detalleJugador;

                for (int i = 0; i < dataArray.length(); i++) {

                    JSONObject row = dataArray.getJSONObject(i);

                    detalleJugador = new DetalleJugador();

                    detalleJugador.team_id = row.getInt("equipo_id");
                    detalleJugador.name_team = row.getString("equipo_nombre");
                    detalleJugador.date_team = row.getString("serie_nombre");
                    detalleJugador.posicion = row.getString("fotos");

                    detalleJugadores.add(detalleJugador);
                }

                ListView ListViewDetalleFoto = (ListView) findViewById(R.id.listViewFotosJugador);
                ArrayAdapterDetalleFoto arrayAdapterDetalleFoto = new ArrayAdapterDetalleFoto(this, detalleJugadores);
                ListViewDetalleFoto.setAdapter(arrayAdapterDetalleFoto);

                ListViewDetalleFoto.getLayoutParams().height = dataArray.length() * funciones.getPixeles(100);

            } else {
                RelativeLayout rel = (RelativeLayout) findViewById(R.id.relativeFotosJugador);
                rel.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class ArrayAdapterDetalleFoto extends ArrayAdapter<DetalleJugador> {

        private ImageView foto;

        public ArrayAdapterDetalleFoto(Context context, List<DetalleJugador> detalleJugadores) {
            super(context, R.layout.listview_fotos_jugador, detalleJugadores);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_fotos_jugador, parent, false);

            DetalleJugador detalleJugador = getItem(position);


            detalleJugador.imageView = (ImageView) view.findViewById(R.id.img_team_foto_jugador);

            if (detalleJugador.con_foto == null)
                new AsyncTaskFoto().execute(detalleJugador);
            else if (detalleJugador.con_foto == true)
                detalleJugador.imageView.setImageBitmap(detalleJugador.foto);


            TextView name = (TextView) view.findViewById(R.id.txt_name_team_foto_jugador);
            name.setText(detalleJugador.name_team);

            TextView date = (TextView) view.findViewById(R.id.txt_serie_team_foto_jugador);
            date.setText(detalleJugador.date_team);

            TextView posicion = (TextView) view.findViewById(R.id.txt_foto_jugador);
            posicion.setText(detalleJugador.posicion);


            return view;
        }

        private void setImage(DetalleJugador detalleJugador) {
            if (detalleJugador.foto != null)
                detalleJugador.imageView.setImageBitmap(detalleJugador.foto);
        }

        private class AsyncTaskFoto extends AsyncTask<DetalleJugador, Void, DetalleJugador> {

            protected DetalleJugador doInBackground(DetalleJugador... params) {
                DetalleJugador detalleJugador = params[0];
                detalleJugador.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/equipos/" + detalleJugador.team_id + ".jpg");

                if (detalleJugador.foto != null) {
                    detalleJugador.con_foto = true;
                    detalleJugador.foto = funciones.imagenRedonda(detalleJugador.foto);
                } else detalleJugador.con_foto = false;

                return detalleJugador;
            }

            @Override
            protected void onPostExecute(DetalleJugador detalleJugador) {
                setImage(detalleJugador);
            }
        }
    }

    /**
     * Notas
     */
    private class getDatosNotasJugador extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/jugadores_equipos.php?user=" + funciones.userId() + "&action=notero");
                url = new URL(Functions.URL_WS + "/cms/ws/jugadores_equipos.php?user=" + funciones.userId() + "&action=notero");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsDatosNotasJugador(result);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public void getResultsDatosNotasJugador(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("nota");
            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            //RELLENO MI CLASE CON LA INFORMACION DEL WS

            JSONObject row1 = dataArray.getJSONObject(0);

            int n = row1.getInt("suc");

            if (n > 0) {

                DetalleJugadores detalleJugadores = new DetalleJugadores();
                DetalleJugador detalleJugador;

                for (int i = 0; i < dataArray.length(); i++) {

                    detalleJugador = new DetalleJugador();

                    JSONObject row = dataArray.getJSONObject(i);

                    detalleJugador.team_id = row.getInt("equipo_id");
                    detalleJugador.name_team = row.getString("equipo_nombre");
                    detalleJugador.date_team = row.getString("serie_nombre");
                    detalleJugador.posicion = row.getString("notas");

                    detalleJugadores.add(detalleJugador);
                }

                ListView ListViewDetalleNota = (ListView) findViewById(R.id.listViewNotasJugador);
                ArrayAdapterDetalleNota arrayAdapterDetalleNota = new ArrayAdapterDetalleNota(this, detalleJugadores);
                ListViewDetalleNota.setAdapter(arrayAdapterDetalleNota);

                ListViewDetalleNota.getLayoutParams().height = dataArray.length() * funciones.getPixeles(100);

            } else {
                RelativeLayout rel = (RelativeLayout) findViewById(R.id.relativeNotasJugador);
                rel.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class ArrayAdapterDetalleNota extends ArrayAdapter<DetalleJugador> {

        private ImageView foto;

        public ArrayAdapterDetalleNota(Context context, List<DetalleJugador> detalleJugadores) {
            super(context, R.layout.listview_notas_jugador, detalleJugadores);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_notas_jugador, parent, false);

            DetalleJugador detalleJugador = getItem(position);


            detalleJugador.imageView = (ImageView) view.findViewById(R.id.img_team_nota_jugador);

            if (detalleJugador.con_foto == null)
                new AsyncTaskFoto().execute(detalleJugador);
            else if (detalleJugador.con_foto == true)
                detalleJugador.imageView.setImageBitmap(detalleJugador.foto);


            TextView name = (TextView) view.findViewById(R.id.txt_name_team_nota_jugador);
            name.setText(detalleJugador.name_team);

            TextView date = (TextView) view.findViewById(R.id.txt_serie_team_nota_jugador);
            date.setText(detalleJugador.date_team);

            TextView posicion = (TextView) view.findViewById(R.id.txt_nota_jugador);
            posicion.setText(detalleJugador.posicion);


            return view;
        }

        private void setImage(DetalleJugador detalleJugador) {
            if (detalleJugador.foto != null)
                detalleJugador.imageView.setImageBitmap(detalleJugador.foto);
        }

        private class AsyncTaskFoto extends AsyncTask<DetalleJugador, Void, DetalleJugador> {

            protected DetalleJugador doInBackground(DetalleJugador... params) {
                DetalleJugador detalleJugador = params[0];
                detalleJugador.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/equipos/" + detalleJugador.team_id + ".jpg");

                if (detalleJugador.foto != null) {
                    detalleJugador.con_foto = true;
                    detalleJugador.foto = funciones.imagenRedonda(detalleJugador.foto);
                } else detalleJugador.con_foto = false;

                return detalleJugador;
            }

            @Override
            protected void onPostExecute(DetalleJugador detalleJugador) {
                setImage(detalleJugador);
            }
        }
    }
}
