package cl.agpro.triber;

/**
 * Created by agpro on 26-02-16.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by agpro on 23-02-16.
 */
public class SeguimientosAdapterDynamic extends ArrayAdapter<Seguimiento> {
    Functions funciones;

    Context context;
    int layoutResourceId;

    Seguimientos seguimientos = null;

    public SeguimientosAdapterDynamic(Context context, int layoutResourceId, Seguimientos seguimientos) {
        super(context, layoutResourceId, seguimientos);

        funciones = new Functions(context);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.seguimientos = seguimientos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final Seguimiento seguimiento = getItem(position);

        if (seguimiento.view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            if ((position + 1) % 2 == 1)
                row.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.gris_1));
            else
                row.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.gris_2));

            ImageView imgIcon = (ImageView) row.findViewById(R.id.imagen);
            ImageView arrow = (ImageView) row.findViewById(R.id.arrowRight);
            ImageView banner = (ImageView) row.findViewById(R.id.SeguimientosBanner);

            TextView txtTitle = (TextView) row.findViewById(R.id.nombre);
            txtTitle.setText(seguimiento.title);

            TextView serieNombre = (TextView) row.findViewById(R.id.serieNombre);
            if (serieNombre != null)
                serieNombre.setText(seguimiento.serie);

            //TIPO DE FILA
            if (seguimiento.typeRow == "3") {
                banner.setVisibility(View.INVISIBLE);
                imgIcon.setVisibility(View.VISIBLE);
                txtTitle.setVisibility(View.VISIBLE);

                if (serieNombre != null)
                    serieNombre.setVisibility(View.VISIBLE);

                arrow.setVisibility(View.VISIBLE);
            } else if (seguimiento.typeRow == "2") {
                banner.setVisibility(View.VISIBLE);
                imgIcon.setVisibility(View.INVISIBLE);
                txtTitle.setVisibility(View.INVISIBLE);

                if (serieNombre != null)
                    serieNombre.setVisibility(View.INVISIBLE);

                arrow.setVisibility(View.INVISIBLE);
            }

            //URL PARA IMAGEN
            String tipoFoto = null;

            int id = seguimiento.id;

            if (seguimiento.typeRow == "2") {
                tipoFoto = "publicidades/banner";
            } else {
                if (seguimiento.type == "torneo") {
                    tipoFoto = "torneos";
                } else if (seguimiento.type == "equipo") {
                    tipoFoto = "equipos";
                    id = seguimiento.equipo_id;
                } else if (seguimiento.type == "jugador") {
                    tipoFoto = "perfiles";
                }
            }

            Drawable myDrawable = null;
            if (seguimiento.type == "torneo") {
                myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_tournament, null);
            } else if (seguimiento.type == "equipo") {
                myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_team, null);
            } else if (seguimiento.type == "jugador") {
                myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_profile, null);
            }

            imgIcon.setImageBitmap(((BitmapDrawable) myDrawable).getBitmap());


            //if (seguimiento.icon == null)
            new AsyncTaskImage(row, seguimiento).execute(Functions.URL_WS + "/imagenes/" + tipoFoto + "/" + id + ".jpg");

            return row;
        } else
            return seguimiento.view;
    }

    public class AsyncTaskImage extends AsyncTask<String, Void, Bitmap> {
        private View row;
        private Seguimiento seguimiento;

        public AsyncTaskImage(View row, Seguimiento seguimiento) {
            this.row = row;
            this.seguimiento = seguimiento;
        }

        protected Bitmap doInBackground(String... params) {
            Log.d("AsyncTaskImage", params[0]);
            return funciones.downloadBitmap(params[0]);
        }

        @Override
        protected void onPostExecute(Bitmap mBitmap) {
            ImageView imgIcon = (ImageView) row.findViewById(R.id.imagen);
            ImageView banner = (ImageView) row.findViewById(R.id.SeguimientosBanner);

            if (mBitmap != null) {
                if (seguimiento.typeRow == "2") {
                    banner.setImageBitmap(mBitmap);
                } else {
                    imgIcon.setImageBitmap(funciones.imagenRedonda(mBitmap));

                    seguimiento.icon = funciones.imagenRedonda(mBitmap);
                }
            } else {
                Drawable myDrawable = null;
                if (seguimiento.type == "torneo") {
                    myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_tournament, null);
                } else if (seguimiento.type == "equipo") {
                    myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_team, null);
                } else if (seguimiento.type == "jugador") {
                    myDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ico_profile, null);
                }
                imgIcon.setImageBitmap(((BitmapDrawable) myDrawable).getBitmap());

                seguimiento.icon = ((BitmapDrawable) myDrawable).getBitmap();
            }

            seguimiento.view = row;
        }
    }
}