package cl.agpro.triber;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by franciscoriveros on 22-03-16.
 */
public class DetalleJugador {
    public int team_id;
    public String tipo, name_team, date_team;

    public String posicion, jugados, victorias, destacado, goles, tiros, bloqueos, minutos, verdes, amarillas, rojas;


    public Bitmap foto = null;
    public Boolean con_foto = null;
    public ImageView imageView;

}
