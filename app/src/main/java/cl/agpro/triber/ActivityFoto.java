package cl.agpro.triber;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class ActivityFoto extends AppCompatActivity {

    Functions funciones;

    public static final String FOTO_ID = "FOTO_ID", GALERY_ID = "GALERY_ID", PARTIDO_ID = "PARTIDO_ID";
    private GetData getData;
    private SaveData saveData;
    int foto_id, partido_id, galery_id;

    Boolean con_foto_partido = null;
    ImageView foto_partido;
    int foto_id_partido;
    Bitmap foto_bipmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto);

        funciones = new Functions(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        foto_id = bundle.getInt(FOTO_ID);
        partido_id = bundle.getInt(PARTIDO_ID);
        galery_id = bundle.getInt(GALERY_ID);
        Log.d("FOTO_ID", String.valueOf(foto_id));
        Log.d("GALERY_ID", String.valueOf(partido_id));
        Log.d("PARTIDO_ID", String.valueOf(galery_id));

        RelativeLayout rel = (RelativeLayout) findViewById(R.id.rel_add_coment);
        rel.setVisibility(View.GONE);

        getData = new GetData();
        getData.execute(partido_id); //OBTENGO DATA DE WS

        Button btn_add = (Button) findViewById(R.id.btn_add_comment_foto);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addComment();
            }
        });

        Button btn_cancel = (Button) findViewById(R.id.btn_cancel_comment_foto);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_comment();
            }
        });

        Button btn_save = (Button) findViewById(R.id.btn_save_comment_foto);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save_comment();
            }
        });

    }

    public void addComment() {
        RelativeLayout rel = (RelativeLayout) findViewById(R.id.rel_add_coment);
        rel.setVisibility(View.VISIBLE);
    }

    public void cancel_comment() {
        EditText text = (EditText) findViewById(R.id.txt_add_comentario_foto);
        text.setText("");

        RelativeLayout rel = (RelativeLayout) findViewById(R.id.rel_add_coment);
        rel.setVisibility(View.GONE);
    }

    public void save_comment() {
        EditText text = (EditText) findViewById(R.id.txt_add_comentario_foto);
        String texto = text.getText().toString();
        if (texto.equals("")) {
            DialogResultError();
        } else {
            texto = texto.replace(" ", "*");

            saveData = new SaveData();
            saveData.execute(texto); //OBTENGO DATA DE WS
        }
    }

    private void DialogResultError() {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage("La comentario no puede estar vacío.")
                .setTitle("Error");

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private class GetData extends AsyncTask<Integer, Void, String> {
        @Override
        protected String doInBackground(Integer... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            String partido_id = params[0].toString();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/gallery.php?action=partidos&partido_id=" + partido_id);
                url = new URL(Functions.URL_WS + "/cms/ws/gallery.php?action=partidos&partido_id=" + partido_id);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }

    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");

            Notas notas = new Notas();
            Nota nota;

            if (dataArray.length() > 0) {

                JSONObject fotosArray = (JSONObject) dataArray.get(0);

                JSONArray fotosAr = fotosArray.getJSONArray("fotos");

                for (int j = 0; j < fotosAr.length(); j++) {
                    JSONObject row = fotosAr.getJSONObject(j);

                    foto_partido = (ImageView) findViewById(R.id.img_foto);

                    foto_id_partido = row.getInt("foto_id");

                    if (foto_id == foto_id_partido) {

                        if (con_foto_partido == null)
                            new AsyncTaskFotoPartido().execute(String.valueOf(foto_id));
                        else if (con_foto_partido == true)
                            foto_partido.setImageBitmap(foto_bipmap);

                        TextView nombre = (TextView) findViewById(R.id.txt_nombre_foto_partido);
                        nombre.setText(row.getString("fotografo_nombre"));

                        JSONObject comentariosArray = (JSONObject) fotosAr.get(j);
                        JSONArray CommentAr = comentariosArray.getJSONArray("comentarios");

                        if (CommentAr.length() == 0) {

                            ListView list = (ListView) findViewById(R.id.listComentFotos);
                            list.setVisibility(View.GONE);
                        }

                        for (int i = 0; i < CommentAr.length(); i++) {
                            JSONObject rowComment = CommentAr.getJSONObject(i);
                            nota = new Nota();

                            nota.usuario_id = rowComment.getInt("user_com_id");
                            nota.name_notero = rowComment.getString("user_com_name");
                            nota.descripcion = rowComment.getString("user_com_txt");
                            nota.titulo = rowComment.getString("agregado");

                            notas.add(nota);
                        }

                        ListView ListViewNotas = (ListView) findViewById(R.id.listComentFotos);
                        ArrayAdapterNotas arrayAdapterNotas = new ArrayAdapterNotas(this, notas);
                        ListViewNotas.setAdapter(arrayAdapterNotas);

                        ListViewNotas.getLayoutParams().height = funciones.getPixeles(110 * CommentAr.length());
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class ArrayAdapterNotas extends ArrayAdapter<Nota> {

        public ArrayAdapterNotas(Context context, List<Nota> notas) {
            super(context, R.layout.listview_partidos_comentario_notas, notas);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.listview_partidos_comentario_notas, parent, false);

                Nota nota = getItem(position);

                nota.imageView = (ImageView) convertView.findViewById(R.id.img_cometario_nota_partido);

                if (nota.con_foto == null)
                    new AsyncTaskFoto().execute(nota);
                else if (nota.con_foto == true)
                    nota.imageView.setImageBitmap(nota.foto);

                TextView fecha = (TextView) convertView.findViewById(R.id.txt_fecha_comentario_nota_partido);
                fecha.setText(nota.titulo);

                TextView nombre = (TextView) convertView.findViewById(R.id.txt_nombre_comentario_nota_partido);
                nombre.setText(nota.name_notero);

                TextView descripcion = (TextView) convertView.findViewById(R.id.txt_descripcion_comentario_nota);
                descripcion.setText(nota.descripcion);
            }

            return convertView;
        }

        private void setImage(Nota nota) {
            if (nota.foto != null)
                nota.imageView.setImageBitmap(nota.foto);
        }

        private class AsyncTaskFoto extends AsyncTask<Nota, Void, Nota> {

            protected Nota doInBackground(Nota... params) {
                Nota nota = params[0];
                nota.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/perfiles/" + nota.usuario_id + ".jpg");

                if (nota.foto != null) {
                    nota.con_foto = true;
                    nota.foto = funciones.imagenRedonda(nota.foto);
                } else nota.con_foto = false;

                return nota;
            }

            @Override
            protected void onPostExecute(Nota nota) {
                setImage(nota);
            }
        }
    }

    private void setImage(Bitmap bitmap) {
        if (bitmap != null)
            foto_partido.setImageBitmap(bitmap);
    }

    private class AsyncTaskFotoPartido extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... params) {
            String id = params[0];
            foto_bipmap = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/galerias/" + galery_id + "/" + id + ".jpg");


            if (foto_bipmap != null) {
                con_foto_partido = true;
                //foto = funciones.imagenRedonda(foto);
            } else con_foto_partido = false;

            return foto_bipmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            setImage(bitmap);
        }
    }


    private class SaveData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            String text = params[0];

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/gallery.php?action=comentario&foto=" + foto_id + "&id=" + funciones.userId() + "&texto=" + text);
                url = new URL(Functions.URL_WS + "/cms/ws/gallery.php?action=comentario&foto=" + foto_id + "&id=" + funciones.userId() + "&texto=" + text);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultSave(result);
        }

    }

    public void getResultSave(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");
            JSONObject rowComment = dataArray.getJSONObject(0);
            int val = rowComment.getInt("ok");

            if (val == 1) {
                EditText text = (EditText) findViewById(R.id.txt_add_comentario_foto);
                text.setText("");

                RelativeLayout rel = (RelativeLayout) findViewById(R.id.rel_add_coment);
                rel.setVisibility(View.GONE);

                getData = new GetData();
                getData.execute(partido_id); //OBTENGO DATA DE WS
            } else {
                EditText text = (EditText) findViewById(R.id.txt_add_comentario_foto);
                text.setText("");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
