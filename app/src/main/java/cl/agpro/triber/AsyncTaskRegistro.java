package cl.agpro.triber;

import android.os.AsyncTask;

/**
 * Created by yerkomora on 23-09-15.
 */
public class AsyncTaskRegistro extends AsyncTask<Void, Void, Boolean> {

    private final ActivityRegistro activityRegistro;
    private final Usuario usuario;

    public AsyncTaskRegistro(ActivityRegistro activityRegistro, Usuario usuario) {
        this.activityRegistro = activityRegistro;
        this.usuario = usuario;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        MySQL mySQL = new MySQL();
        mySQL.conectar();

        Boolean guardado = false;

        if (!mySQL.existeUsuarioNick(usuario.nick) && !mySQL.existeUsuarioEmail(usuario.email))
            guardado = mySQL.setRegistro(usuario);

        mySQL.desconectar();
        return guardado;
    }

    @Override
    protected void onPostExecute(Boolean guardado) {
        super.onPostExecute(guardado);

        if (guardado)
            activityRegistro.irAlHome();
    }
}
