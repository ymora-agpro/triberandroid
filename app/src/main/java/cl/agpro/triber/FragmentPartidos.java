package cl.agpro.triber;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by yerkomora on 22-03-16.
 */
public class FragmentPartidos extends Fragment {
    private static final int SERIE = 0, COMPETENCIA = 1;
    private Functions funciones;
    private GetData getData;
    private View rootView;
    private int serie_id = 0, competencia_id = 0;

    private SwipeRefreshLayout swipeContainer;
    private ArrayAdapterPartidos arrayAdapterPartidos;
    private Partidos partidos;

    private int pageFuture, pagePast;
    private String futureActive, stopScroll;

    private ProgressBar progressBar;


    public FragmentPartidos() {
    }

    public static FragmentPartidos newInstance(int serie_id) {
        FragmentPartidos fragment = new FragmentPartidos();
        Bundle args = new Bundle();
        args.putInt(Functions.SERIE_ID, serie_id);
        fragment.setArguments(args);
        return fragment;
    }

    public static FragmentPartidos newInstanceCompetencia(int competencia_id) {
        FragmentPartidos fragment = new FragmentPartidos();
        Bundle args = new Bundle();
        args.putInt(Functions.COMPETENCIA_ID, competencia_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        funciones = new Functions(getActivity());

        Bundle bundle = getArguments();

        serie_id = bundle.getInt(Functions.SERIE_ID);
        competencia_id = bundle.getInt(Functions.COMPETENCIA_ID);

        GetDataFuturo getDataFuturo = new GetDataFuturo();

        if (serie_id > 0)
            getDataFuturo.execute(SERIE, serie_id);
        else
            getDataFuturo.execute(COMPETENCIA, competencia_id);

        rootView = inflater.inflate(R.layout.fragment_partidos, container, false);

        return rootView;
    }

    /**
     * Saber si existen partidos futuros
     */
    private class GetDataFuturo extends AsyncTask<Integer, Void, String> {
        @Override
        protected String doInBackground(Integer... params) {
            StringBuilder response = new StringBuilder();

            String id = params[1].toString();

            URL url = null;
            try {
                String sURL = Functions.URL_WS + "/cms/ws/isfuture.php?";

                if (params[0] == SERIE)
                    sURL += "equipo_id=" + id + "&type=equipos";
                else
                    sURL += "torneo=" + id + "&type=partido";

                sURL += "&time=future&page=0";

                Log.d("WebService", sURL);

                url = new URL(sURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsFuturo(result);
        }

    }

    public void getResultsFuturo(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");

            JSONObject row = dataArray.getJSONObject(0);

            if (row.getInt("ok") == 1) {
                pageFuture = 1;
                pagePast = 0;
                futureActive = "disabled";
            } else {
                pageFuture = 0;
                pagePast = 1;
                futureActive = "active";
                stopScroll = "stop";
            }

            getData = new GetData("");

            if (serie_id > 0)
                getData.execute(SERIE, serie_id);
            else
                getData.execute(COMPETENCIA, competencia_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Fragment para los partidos del equipo
     */
    private class GetData extends AsyncTask<Integer, Void, String> {
        private String time;

        public GetData(String time) {
            this.time = time;
        }

        @Override
        protected String doInBackground(Integer... params) {
            StringBuilder response = new StringBuilder();

            String id = params[1].toString();

            String sURL;
            int page;

            sURL = Functions.URL_WS + "/cms/ws/";

            if (params[0] == SERIE)
                sURL += "equipos_partidos.php?equipo_id=" + id;
            else
                sURL += "partidos_competencias.php?torneo=" + id;

            if (time.equals(""))

                if (futureActive.equals("active"))
                    sURL += "&time=past&page=0";
                else
                    sURL += "&time=future&page=0";

            else {
                if (time.equals("past"))
                    page = pagePast;
                else
                    page = pageFuture;

                sURL += "&time=" + time + "&page=" + page + "&loadFuture=" + futureActive;
            }

            Log.d("WebService", sURL);

            URL url = null;
            try {
                url = new URL(sURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (!time.equals("past")) {
                progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
                progressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (!time.equals("past"))
                progressBar.setVisibility(View.INVISIBLE);

            if (time.equals(""))
                getResults(result);
            else
                getResultsAgregados(result, time.equals("past"));
        }

    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray;

            if (serie_id > 0)
                dataArray = obj.getJSONArray("matches");
            else
                dataArray = obj.getJSONArray("match");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            partidos = new Partidos();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            Partido partido;

            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);

                if (row.getInt("suc") == 1) {
                    partido = setPartido(row);

                    if (futureActive.equals("active"))
                        partidos.add(0, partido);
                    else
                        partidos.add(partido);
                }
            }

            final ListView listViewPartidos = (ListView) rootView.findViewById(R.id.partidos);

            arrayAdapterPartidos = new ArrayAdapterPartidos(getActivity(), partidos);
            listViewPartidos.setAdapter(arrayAdapterPartidos);

            listViewPartidos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    int partido_id = (int) view.getTag();

                    Intent intent = new Intent(getContext(), ActivityPartido.class);
                    intent.putExtra(Functions.PARTIDO_ID, partido_id);
                    startActivity(intent);
                }
            });

            // Attach the listener to the AdapterView onCreate
            listViewPartidos.setOnScrollListener(new InfiniteScrollListener(1) {
                @Override
                public void loadMore(int page, int totalItemsCount) {
                    getData = new GetData("future");

                    if (serie_id > 0)
                        getData.execute(SERIE, serie_id);
                    else
                        getData.execute(COMPETENCIA, competencia_id);
                }
            });


            // Lookup the swipe container view
            swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
            // Setup refresh listener which triggers new data loading
            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // Your code to refresh the list here.
                    // Make sure you call swipeContainer.setRefreshing(false)
                    // once the network request has completed successfully.
                    fetchTimelineAsync();
                }
            });
            // Configure the refreshing colors
                /*swipeContainer.setColorSchemeResources(android.R.color.transparent,
                        android.R.color.transparent,
                        android.R.color.transparent,
                        android.R.color.transparent);*/

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Partido setPartido(JSONObject row) {
        Partido partido = new Partido();

        try {
            partido.id = row.getInt("partido_id");

            if (serie_id > 0)
                partido.fecha = row.getString("partido_fecha");
            else
                partido.fecha = row.getString("fecha_partido");

            partido.equipo[0] = new Equipo();
            partido.equipo[0].id = row.getInt("local_id");
            partido.equipo[0].nombre = row.getString("local_nombre");
            partido.equipo[0].serie.nombre = row.getString("local_serie_nombre");

            if (serie_id > 0)
                partido.equipo[0].goles = row.getInt("local_goles");
            else
                partido.equipo[0].goles = row.getInt("goles_local");

            partido.equipo[1] = new Equipo();
            partido.equipo[1].id = row.getInt("visita_id");
            partido.equipo[1].nombre = row.getString("visita_nombre");
            partido.equipo[1].serie.nombre = row.getString("visita_serie_nombre");

            if (serie_id > 0)
                partido.equipo[0].goles = row.getInt("visita_goles");
            else
                partido.equipo[0].goles = row.getInt("goles_visita");

            partido.jugado = row.getInt("jugado") == 1;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return partido;
    }

    public void getResultsAgregados(String response, Boolean principio) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray;

            if (serie_id > 0)
                dataArray = obj.getJSONArray("matches");
            else
                dataArray = obj.getJSONArray("match");

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            Partido partido;
            Boolean resultado = false;
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);

                if (row.getInt("suc") == 1) {
                    resultado = true;
                    partido = setPartido(row);

                    if (principio)
                        partidos.add(0, partido);
                    else
                        partidos.add(partido);
                }
            }

            if (resultado) {
                if (principio)
                    pagePast++;
                else
                    pageFuture++;

                arrayAdapterPartidos.notifyDataSetChanged();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (principio)
            swipeContainer.setRefreshing(false);
    }

    public void fetchTimelineAsync() {
        getData = new GetData("past");

        if (serie_id > 0)
            getData.execute(SERIE, serie_id);
        else
            getData.execute(COMPETENCIA, competencia_id);
    }
}

