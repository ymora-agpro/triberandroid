package cl.agpro.triber;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by franciscoriveros on 17-02-16.
 */
public class BienvenidaCompetenciasAdapter extends ArrayAdapter<BienvenidaCompetencias> {
    Context myContext;
    int myLayoutResourceID;
    BienvenidaCompetencias mydata[] = null;

    public BienvenidaCompetenciasAdapter(Context context, int layoutResourceID, BienvenidaCompetencias[] data) {
        super(context, layoutResourceID, data);

        this.myContext = context;
        this.myLayoutResourceID = layoutResourceID;
        this.mydata = data;
    }



    public View getView(int position, View convertView, ViewGroup parent){
        View row =  convertView;

        BienvenidaCompetenciasHolder holder = null;

        if(row == null) {
            LayoutInflater inflater = ((Activity) myContext).getLayoutInflater();
            row =  inflater.inflate(myLayoutResourceID, parent, false);

            holder = new BienvenidaCompetenciasHolder();
            holder.imagen = (ImageView) row.findViewById(R.id.img);
            holder.texto = (TextView) row.findViewById(R.id.textCom);
            row.setTag(holder);

        } else {
            holder = (BienvenidaCompetenciasHolder) row.getTag();
        }

        BienvenidaCompetencias bienvenidaCompetencias = mydata[position];
        holder.texto.setText(bienvenidaCompetencias.title);
        holder.imagen.setImageResource(bienvenidaCompetencias.icon);

        return row;
    }


    static class BienvenidaCompetenciasHolder{
        ImageView imagen;
        TextView texto;
    }
}