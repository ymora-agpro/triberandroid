package cl.agpro.triber;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.regex.Pattern;

public class FragmentBienvenidaCompetencias extends Fragment {
    Functions funciones;
    View view;
    private GetData getData;

    private Follows follows, follows_todos;
    private FollowAdapterDynamic adapterDynamic;

    private ImageView img;

    public FragmentBienvenidaCompetencias() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bienvenida_competencias, container, false);

        img = (ImageView) view.findViewById(R.id.img);

        funciones = new Functions(getActivity());
        Log.d("paso", "1");

        getData = new GetData();
        getData.execute();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        getData.cancel(true);
    }

    public void setData(String searchText) {

        if (img.getVisibility() == View.VISIBLE)
            img.setVisibility(View.GONE);

        follows.clear();

        for (Follow follow : follows_todos)
            if (Pattern.compile(Pattern.quote(searchText), Pattern.CASE_INSENSITIVE).matcher(follow.title).find())
                follows.add(follow);

        adapterDynamic.notifyDataSetChanged();
    }

    public void closeData() {
        img.setVisibility(View.VISIBLE);
    }

    private class GetData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                int paisId = getActivity().getIntent().getExtras().getInt("pais");
                String sURL = Functions.URL_WS + "/cms/ws/list_categories.php?action=torneo&user=" + funciones.userId() + "&pais=" + paisId + "&busqueda=1";
                Log.d("WebService", sURL);
                url = new URL(sURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("torneos");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            follows = new Follows();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);
                int id = row.getInt("id");
                String nombre = row.getString("nombre");
                String follower = row.getString("follower");
                follows.add(new Follow("torneo", id, nombre, follower));
            }

            follows_todos = new Follows();
            follows_todos.addAll(follows);

            ListView lv = (ListView) getActivity().findViewById(R.id.listView);
            adapterDynamic = new FollowAdapterDynamic(getActivity(), R.layout.item_follow, follows);
            lv.setAdapter(adapterDynamic);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}