package cl.agpro.triber;

import android.os.AsyncTask;

/**
 * Created by yerkomora on 18-01-16.
 */
public class AsyncTaskPaises extends AsyncTask<Void, Void, Boolean> {
    private static final String TAG = "AsyncTaskPaises";
    Paises paises;
    ActivityPaises activityPaises;

    public AsyncTaskPaises(ActivityPaises activityPaises) {
        this.activityPaises = activityPaises;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        MySQL mySQL = new MySQL();
        mySQL.conectar();

        paises = mySQL.getPaises();
        mySQL.desconectar();

        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if (aBoolean)
            activityPaises.cargarPaises(paises);
    }
}
