package cl.agpro.triber;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTutorial extends Fragment {
    private int position;
    public String TAG = "FragmentTutorial";
    public String blogTitles[];
    private ImageView ivFragmentTutorialFondo;


    public FragmentTutorial() {
        // Required empty public constructor
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tutorial, container, false);

        //FrameLayout flFragmentTutorialFondo = (FrameLayout) view.findViewById(R.id.flFragmentTutorialFondo);
        //int fondo = getResources().getIdentifier("tutorial_fondo_" + String.valueOf(position), "drawable", getContext().getPackageName());
        //flFragmentTutorialFondo.setBackgroundResource(fondo);

        ivFragmentTutorialFondo = (ImageView) view.findViewById(R.id.ivFragmentTutorialFondo);
        int fondo = getResources().getIdentifier("tutorial_fondo_" + String.valueOf(position), "drawable", getContext().getPackageName());
        ivFragmentTutorialFondo.setBackgroundResource(fondo);

        TextView tvFragmentTutorialTitulo = (TextView) view.findViewById(R.id.tvFragmentTutorialTitulo);
        int titulo = getResources().getIdentifier("fragment_tutorial_" + String.valueOf(position) + "_titulo", "string", getContext().getPackageName());
        tvFragmentTutorialTitulo.setText(getString(titulo));

        TextView tvFragmentTutorialTexto = (TextView) view.findViewById(R.id.tvFragmentTutorialTexto);
        int texto = getResources().getIdentifier("fragment_tutorial_" + String.valueOf(position) + "_texto", "string", getContext().getPackageName());
        tvFragmentTutorialTexto.setText(getString(texto));

        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        animarFondo();
    }

    public void animarFondo() {
        TranslateAnimation translateAnimation = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f
                , Animation.RELATIVE_TO_PARENT, 0.3f
                , Animation.RELATIVE_TO_PARENT, 0.0f
                , Animation.RELATIVE_TO_PARENT, 0.0f);

        translateAnimation.setDuration(5000);
        translateAnimation.setRepeatCount(Animation.INFINITE);
        translateAnimation.setRepeatMode(Animation.REVERSE);
        translateAnimation.setFillAfter(true);

        ivFragmentTutorialFondo.startAnimation(translateAnimation);
    }
}