package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.facebook.login.LoginManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ActivityMain extends ActivityFacebook {
    //URL FOR WEBSERVICE
    public String URL_WS = "http://www.triber.ag-pro.cl/desarrollo/cms/ws";
    public String TAG = "ActivityMain";
    SharedPreferences prefs;
    private static final int TIEMPO = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new checkLogin().execute();
            }
        }, TIEMPO);
    }

    private class checkLogin extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            StringBuilder response  = new StringBuilder();
            URL url = null;
            try {
                prefs = getSharedPreferences("usuario", Context.MODE_PRIVATE);
                url = new URL(URL_WS+"/have_fav.php?user_id="+prefs.getInt("userId", 0)+"&action=get");
                Log.d(TAG,URL_WS+"/have_fav.php?user_id="+prefs.getInt("userId", 0)+"&action=get");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection)url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK)
                {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null)
                    {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            ParseJson(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }
    public String ParseJson(String response){
        JSONObject obj = null;
        try {
            if (prefs.getInt("userId", 0) > 0) {
                obj = new JSONObject(response.toString());
                String biendenidaCompleta = obj.getString("val");
                if(biendenidaCompleta.equals("1")){
                    goNewHome();
                }else{
                    irBienvenida();
                }
            } else {
                irALTutorial();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }
    public void irALTutorial() {
        Intent intent = new Intent(getBaseContext(), ActivityTutorial.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();
    }
    public void irBienvenida() {
        Intent intent = new Intent(getBaseContext(), ActivityPaises.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }
    public void goNewHome() {
        Intent intent = new Intent(getBaseContext(), ActivityHomeFinal.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();
    }
}
/*
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class ActivityMain extends Activity {
    private static final String TAG = ActivityMain.class.getSimpleName();
    public static final String PACKAGE = "cl.agpro.triber";

    Button login_linkedin_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login_linkedin_btn = (Button) findViewById(R.id.login_button);
        login_linkedin_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login_linkedin();
            }
        });
    }

    // Authenticate with linkedin and intialize Session.

    public void login_linkedin(){
        LISessionManager.getInstance(getApplicationContext()).init(this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                // Toast.makeText(getApplicationContext(), "success" + LISessionManager.getInstance(getApplicationContext()).getSession().getAccessToken().toString(), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onAuthError(LIAuthError error) {

                Toast.makeText(getApplicationContext(), "failed " + error.toString(),
                        Toast.LENGTH_LONG).show();
            }
        }, true);
    }

    // After complete authentication start new HomePage Activity

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this,
                requestCode, resultCode, data);
        //Intent intent = new Intent(ActivityMain.this,UserProfile.class);
        //startActivity(intent);
        Log.d("asdasd", String.valueOf(requestCode));
    }

    // This method is used to make permissions to retrieve data from linkedin

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }
}*/

/*public class ActivityMain extends Activity {
    private static final String TAG = ActivityMain.class.getSimpleName();
    public static final String PACKAGE = "cl.agpro.triber";
    // Your Package Name
    Button hask_key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hask_key = (Button) findViewById(R.id.show_hash);

        //Compute application package and hash

        hask_key.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ctm", generateHashkey());
            }
        });
    }

    // This Method is used to generate "Android Package Name" hash key

    public String generateHashkey(){
        String Hash="";
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    PACKAGE,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

                ((TextView) findViewById(R.id.package_name)).setText(info.packageName);
                Hash = Base64.encodeToString(md.digest(), Base64.NO_WRAP);
                Log.d("hash qlao",Base64.encodeToString(md.digest(),Base64.NO_WRAP));
                        ((TextView) findViewById(R.id.hash_key)).setText(Base64.encodeToString(md.digest(),
                                Base64.NO_WRAP));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, e.getMessage(), e);
        } catch (NoSuchAlgorithmException e) {
            Log.d(TAG, e.getMessage(), e);
        }
        return Hash;
    }

}*/
