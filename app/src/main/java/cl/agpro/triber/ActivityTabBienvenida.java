package cl.agpro.triber;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ActivityTabBienvenida extends AppCompatActivity implements SearchView.OnQueryTextListener {
    private static final String TAG = "ActivityTabBienvenida";
    Functions funciones;
    private TabLayout tabLayout;

    SectionPagerAdapter sectionPagerAdapter;

    private FragmentBienvenidaCompetencias fragmentBienvenidaCompetencias = null;
    private FragmentBienvenidaEquipos fragmentBienvenidaEquipos = null;
    private FragmentBienvenidaJugadores fragmentBienvenidaJugadores = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        funciones = new Functions(this);
        setContentView(R.layout.activity_tab_bienvenida);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        sectionPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(sectionPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bienvenida, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        //searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_aceptar) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.alert_confirmacion)
                    .setMessage(R.string.alert_seguro_favoritos)
                    .setNegativeButton(android.R.string.cancel, null) // dismisses by default
                    .setPositiveButton(R.string.alert_aceptar, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String UrlSet = Functions.URL_WS + "/cms/ws/have_fav.php?user_id=" + funciones.userId() + "&action=set";
                            Log.d("SET FAV", UrlSet);
                            new SetBienvenidaCompleta().execute(UrlSet);
                        }
                    })
                    .create()
                    .show();
            return true;
        }
        if (id == android.R.id.home) {
            funciones.irSeleccionDePaises();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class SectionPagerAdapter extends FragmentPagerAdapter {
        public SectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    if (fragmentBienvenidaCompetencias == null)
                        fragmentBienvenidaCompetencias = new FragmentBienvenidaCompetencias();
                    return fragmentBienvenidaCompetencias;
                case 1:
                    if (fragmentBienvenidaEquipos == null)
                        fragmentBienvenidaEquipos = new FragmentBienvenidaEquipos();
                    return fragmentBienvenidaEquipos;
                default:
                    if (fragmentBienvenidaJugadores == null)
                        fragmentBienvenidaJugadores = new FragmentBienvenidaJugadores();
                    return fragmentBienvenidaJugadores;
            }
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
        }

        public void setData(String searchText) {
            switch (tabLayout.getSelectedTabPosition()) {
                case 0:
                    fragmentBienvenidaCompetencias.setData(searchText);
                    break;
                case 1:
                    fragmentBienvenidaEquipos.setData(searchText);
                    break;
                default:
                    fragmentBienvenidaJugadores.setData(searchText);
            }
        }


        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Competencias";
                case 1:
                    return "Equipos";
                default:
                    return "Jugadores";
            }
        }
    }

    private class SetBienvenidaCompleta extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String UrlFollow = params[0];
            StringBuilder response = new StringBuilder();
            URL url = null;
            try {
                url = new URL(UrlFollow);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("post on follow", result);
            funciones.irAlMuro();
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    @Override
    public boolean onQueryTextChange(String searchText) {
        sectionPagerAdapter.setData(searchText);

        if (searchText.isEmpty())
            switch (tabLayout.getSelectedTabPosition()) {
                case 0:
                    fragmentBienvenidaCompetencias.closeData();
                    break;
                case 1:
                    fragmentBienvenidaEquipos.closeData();
                    break;
                default:
                    fragmentBienvenidaJugadores.closeData();
            }

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String searchText) {
        return false;
    }
}

