package cl.agpro.triber;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;

public class ActivityHome extends ActivityFacebook {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.salir))
                .setMessage(getString(R.string.desea_salir))
                .setPositiveButton(getString(R.string.si), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityHome.super.onBackPressed();
                    }
                })
                .setNegativeButton(getString(R.string.no), null)
                .show();
    }
}