package cl.agpro.triber;

/**
 * Created by agpro on 23-02-16.
 */
public class Weather {
    public int icon;
    public String title;
    public Weather(){
        super();
    }

    public Weather(int icon, String title) {
        super();
        this.icon = icon;
        this.title = title;
    }
}