package cl.agpro.triber;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Locale;

public class ActivityArriendo extends AppCompatActivity {


    private GetData getData;
    private GetData2 getData2;
    private ArriendoTipos tipos, tipos_todos;
    private ArrayAdapterArriendo adapterDynamic;

    double latitude, longitude;

    GPSTracker gps;

    Switch switchArriendo;

    Spinner spinRegion, spinCity, spinType, spinCant;
    String[] datosType = {"Canchas", "Arbitros"};
    String[] datosCant = {"1", "2", "3"};
    Button btn_date, btn_time;
    int year_x, month_x, day_x, hour_x, minute_x;
    TextView txt_date, txt_time;
    static final int DIALOG_ID = 0;
    static final int DIALOG_TIME = 0;

    String strinReg, strinCit, strintype, strinCant, strinFecha, stringHora;
    int checked = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arriendo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_arriendo);
        setSupportActionBar(toolbar);

        final Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH);
        day_x = cal.get(Calendar.DAY_OF_MONTH);

        showDiaLogOnButtonClick();
        showTimePickerDialog();

        switchArriendo = (Switch) findViewById(R.id.switchArriendo);

        //setOnCheckedChangeListener(new OnCheckedChangeListener()

        switchArriendo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    checked = 1;
                    Log.d("EL CHECK ES: ", String.valueOf(checked));

                    TextView txt = (TextView) findViewById(R.id.lblTitle2Arriendo);
                    txt.setVisibility(View.GONE);

                    Spinner reg = (Spinner) findViewById(R.id.spinnerRegionArriendo);
                    reg.setVisibility(View.GONE);

                    Spinner cit = (Spinner) findViewById(R.id.spinnerCityArriendo);
                    cit.setVisibility(View.GONE);

                    gps = new GPSTracker(ActivityArriendo.this);

                    // check if GPS enabled
                    if(gps.canGetLocation()){

                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();

                        // \n is for new line
                        Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    } else {
                        // can't get location
                        // GPS or Network is not enabled
                        // Ask user to enable GPS/network in settings
                        gps.showSettingsAlert();
                    }
                }else{
                    checked = 0;
                    Log.d("EL CHECK ES: ", String.valueOf(checked));

                    TextView txt = (TextView) findViewById(R.id.lblTitle2Arriendo);
                    txt.setVisibility(View.VISIBLE);

                    Spinner reg = (Spinner) findViewById(R.id.spinnerRegionArriendo);
                    reg.setVisibility(View.VISIBLE);

                    Spinner cit = (Spinner) findViewById(R.id.spinnerCityArriendo);
                    cit.setVisibility(View.VISIBLE);
                }
            }
        });


        spinType = (Spinner) findViewById(R.id.spinnerTipoArriendo);
        ArrayAdapter<String> adapterType = new ArrayAdapter<String>(this, R.layout.spinner_arriendo, datosType);
        spinType.setAdapter(adapterType);

        spinType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strintype = spinType.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        getData = new GetData();
        getData.execute(); //OBTENGO DATA DE WS

        spinCant = (Spinner) findViewById(R.id.spinnerCantArriendo);
        ArrayAdapter<String> adapterCant = new ArrayAdapter<String>(this, R.layout.spinner_arriendo, datosCant);
        spinCant.setAdapter(adapterCant);

        spinCant.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                strinCant = spinCant.getSelectedItem().toString();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.arriendo_buscar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.arriendoBuscar) {
            goSendData();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showDiaLogOnButtonClick() {
        btn_date = (Button) findViewById(R.id.btn_date);
        btn_date.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(1);
                    }
                }
        );
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 1) {
            return new DatePickerDialog(this, dpickerListner, year_x, month_x, day_x);
        } else if (id == 2) {
            return new TimePickerDialog(ActivityArriendo.this, kTimePickerListner, hour_x, minute_x, false);
        } else {
            return null;
        }
    }

    private DatePickerDialog.OnDateSetListener dpickerListner
            = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            year_x = year;
            month_x = monthOfYear + 1;
            day_x = dayOfMonth;

            String y = String.format("%d", year_x);
            String m = String.format("%02d", month_x);
            String d = String.format("%02d", day_x);

            //Toast.makeText(ActivityArriendo.this, year_x + "/" + month_x + "/" + day_x, Toast.LENGTH_SHORT).show();

            txt_date = (TextView) findViewById(R.id.lblDateArriendo);
            txt_date.setText(y + "/" + m + "/" + d);
            strinFecha = y + "/" + m + "/" + d;
        }
    };


    public void showTimePickerDialog() {
        btn_time = (Button) findViewById(R.id.btn_time);
        btn_time.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(2);
                    }
                }
        );
    }

    protected TimePickerDialog.OnTimeSetListener kTimePickerListner
            = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            hour_x = hourOfDay;
            minute_x = minute;

            String h = String.format("%02d", hour_x);
            String min = String.format("%02d", minute_x);

            txt_time = (TextView) findViewById(R.id.lblTimeArriendo);
            txt_time.setText(h + ":" + min);

            stringHora = h + ":" + min;

            //Toast.makeText(ActivityArriendo.this, hour_x + ":" + minute_x, Toast.LENGTH_LONG).show();
        }
    };

    private class GetData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {

                String sURL = Functions.URL_WS + "/cms/ws/get_region.php";
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);

            spinRegion = (Spinner) findViewById(R.id.spinnerRegionArriendo);

            if (spinRegion != null) {
                spinRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        ArriendoTipo arriendoTipo = (ArriendoTipo) parent.getItemAtPosition(position);
                        //Toast.makeText(ActivityArriendo.this, arriendoTipo.toString(), Toast.LENGTH_LONG).show();

                        strinReg = arriendoTipo.toString();

                        getData2 = new GetData2();
                        getData2.execute(); //OBTENGO DATA DE WS
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("regiones");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            tipos = new ArriendoTipos();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);
                int id = 0;
                String nombre = row.getString("nombre");
                tipos.add(new ArriendoTipo(nombre, id));
            }

            tipos_todos = new ArriendoTipos();
            tipos_todos.addAll(tipos);

            spinRegion = (Spinner) findViewById(R.id.spinnerRegionArriendo);
            adapterDynamic = new ArrayAdapterArriendo(this, tipos);
            spinRegion.setAdapter(adapterDynamic);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class GetData2 extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                 String stringReg = strinReg.replace(" ", "_");
                String sURL = Functions.URL_WS + "/cms/ws/get_comuna.php?region_id="+stringReg;
                Log.d("QUE HAY AQUI: ", sURL);
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults2(result);


            spinCity = (Spinner) findViewById(R.id.spinnerCityArriendo);

            if (spinCity != null) {
                spinCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        ArriendoTipo arriendoTipo = (ArriendoTipo) parent.getItemAtPosition(position);
                        //Toast.makeText(ActivityArriendo.this, arriendoTipo.toString(), Toast.LENGTH_LONG).show();

                        strinCit = arriendoTipo.toString();


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResults2(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("comunas");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            tipos = new ArriendoTipos();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);
                int id = 0;
                String nombre = row.getString("nombre");
                tipos.add(new ArriendoTipo(nombre, id));
            }

            //tipos_todos = new ArriendoTipos();
            //tipos_todos.addAll(tipos);

            spinCity = (Spinner) findViewById(R.id.spinnerCityArriendo);
            adapterDynamic = new ArrayAdapterArriendo(this, tipos);
            spinCity.setAdapter(adapterDynamic);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void goSendData() {

        String StringRegion, StringCiudad, StringFecha, StringHora, StringTipo, StringCantidad;
        int IntLocacion;

        IntLocacion = checked;
        String variable;
        String new_city = strinCit.replace(" ", "*");

        //Log.d("DATOS BUSQUEDA", "La region es: " + StringRegion + ", la ciudad es: " + StringCiudad + ", la fecha es: " + StringFecha + ", la hora es: " + StringHora + ", el tipo es: " + StringTipo + " y la cantidad de horas es: " + StringCantidad );

        if ( IntLocacion == 1 ) {
            variable = "fecha=" + strinFecha + "&hora=" + stringHora + "&cantidadHr=" + strinCant + "&action=myLocation&latitud=" + latitude + "&longitud=" + longitude;
        } else {
            variable = "fecha=" + strinFecha + "&hora=" + stringHora + "&cantidadHr=" + strinCant + "&action=comuna&comuna=" + new_city;
        }

        Intent intent = new Intent(getBaseContext(), ActivityListArriendo.class);
        intent.putExtra("variable", variable);
        startActivity(intent);
    }
}
