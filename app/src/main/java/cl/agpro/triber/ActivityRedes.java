package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Locale;

public class ActivityRedes extends ActivityFacebook {
    private static final String TAG = "ActivityRedes";

    public static ActivityRedes activityRedes;

    public static final String ACTIVIDAD = "ACTIVIDAD";

    public enum Actividad {
        REGISTRO, INGRESO
    }

    Actividad actividad;

    // Facebook Variables
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRedes = this;
        setContentView(R.layout.activity_redes);
        Intent intent = getIntent();
        actividad = (Actividad) intent.getSerializableExtra(ACTIVIDAD);

        TextView tvRedesTitulo = (TextView) findViewById(R.id.tvRedesTitulo);
        switch (actividad) {
            case REGISTRO:
                tvRedesTitulo.setText(getString(R.string.registrate));
                ActivityFacebook.typeLogin("REGISTRO");
                break;
            case INGRESO:
                tvRedesTitulo.setText(getString(R.string.ingresa));
                ActivityFacebook.typeLogin("LOGIN");
                break;
        }

        Button bLoginsEmail = (Button) findViewById(R.id.bRedesEmail);

        bLoginsEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (actividad) {
                    case REGISTRO:
                        irAlRegistro();
                        break;
                    case INGRESO:
                        irAlIngreso();
                        break;
                }
            }
        });

        Button bRedesLinkedin = (Button) findViewById(R.id.bRedesLinkedin);

        bRedesLinkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (actividad) {
                    case REGISTRO:
                        irAlinkedinRegistro();
                        break;
                    case INGRESO:
                        irAlinkedinIngreso();
                        break;
                }
            }
        });


        /*Button bRedesFacebook = (Button) findViewById(R.id.bRedesFacebook);

        bRedesFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (actividad) {
                    case REGISTRO:
                        irAlRegistroFacebook();
                        break;
                    case INGRESO:
                        irAlIngreso();
                        break;
                }
            }
        });*/
    }

    public class loginFb extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String email  = params[0];
            String deviceId = params[1];
            String Lang = params[2];
            StringBuilder response  = new StringBuilder();
            URL url = null;
            try {
                url = new URL(URL_WS+"/login_ios.php?username=&correo="+email+"&action=validateRed&disp="+deviceId+"&lang="+Lang+"&token=");
                Log.d("VALIDARE RED ",URL_WS+"/login_ios.php?username=&correo="+email+"&action=validateRed&disp="+deviceId+"&lang="+Lang+"&token=");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection)url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK)
                {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null)
                    {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            loginFaceResp(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public String loginFaceResp(String response){
        JSONObject obj = null;
        try {
            obj = new JSONObject(response.toString());
            JSONArray dataWs = obj.getJSONArray("login");
            JSONObject c = dataWs.getJSONObject(0);
            int idUser  = c.getInt("id");
            int val     = c.getInt("val");
            Log.d(TAG, "usuario = " + idUser + " es seguidor = " + val);
            if (idUser>0){
                Log.d(TAG,"user id mayor a 0");
                SharedPreferences prefs = getSharedPreferences("usuario",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("userId", idUser);
                editor.commit();
                if (val>0){
                    Log.d(TAG,"ir a home");
                    goNewHome();
                }else{
                    Log.d(TAG,"ir a bienvenida");
                    irBienvenida();
                }
            }else{
                usuarioNoExiste();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }

    public class registrarValidarFacebook extends AsyncTask<String, Void, String> {
        String UsernameF = "";
        String EmailF = "";
        String NameF = "";
        @Override
        protected String doInBackground(String... params) {

            String username         = params[0];
            String email            = params[1];
            String name             = params[2];

            UsernameF = username;
            EmailF = email;
            NameF = name;

            StringBuilder response  = new StringBuilder();

            URL url = null;
            try {
                url = new URL(URL_WS+"/login_ios.php?username="+username+"&correo="+email+"&action=validate");
                Log.d("URL VALIDATE",URL_WS+"/login_ios.php?username="+username+"&correo="+email+"&action=validate");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection)url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK)
                {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null)
                    {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            responseValidate(result,UsernameF,EmailF,NameF);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }
    private class registrar extends AsyncTask<String, Void, String> {
        String deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        String Lang = Locale.getDefault().getDisplayLanguage();
        @Override
        protected String doInBackground(String... params) {
            String email         = params[0];
            String name         = params[2].replace(" ", "%20");
            Log.d("nombre ctm ",name);
            StringBuilder response  = new StringBuilder();

            URL url = null;
            try {
                String url_formatted = URL_WS+"/login_ios.php?correo="+email+"&name="+name+"&last_name=&disp="+deviceId+"&lang="+Lang+"&action=new&social=facebook&socialImage=&useridrrss=";
                //url_formatted.replace(" ", "%20");
                url = new URL(url_formatted);
                Log.d("registrar",url_formatted);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection)url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK)
                {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null)
                    {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            responseCreateNew(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void responseCreateNew(String response){
        JSONObject obj = null;
        try {
            obj = new JSONObject(response.toString());
            Log.d("resultado de ingreso", String.valueOf(obj));
            int userId = obj.getInt("success");
            if(userId>0){
                Log.d(TAG,"user id mayor a 0 es = "+userId);
                SharedPreferences prefs = getSharedPreferences("usuario",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("userId", userId);
                editor.commit();

                irBienvenida();
            }else{
                Toast toast = Toast.makeText(getApplicationContext(), R.string.activity_registro_email_existe, Toast.LENGTH_SHORT);
                toast.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void responseValidate(String response, String username, String email, String name){
        JSONObject obj = null;
        try {
            obj = new JSONObject(response.toString());
            Log.d("respuesta qla registro", String.valueOf(obj));
            int success = obj.getInt("success");
            if (success==2){
                Log.d("xD","sad 2");
                Toast toast = Toast.makeText(getApplicationContext(), R.string.activity_registro_nick_existe_final, Toast.LENGTH_SHORT);
                toast.show();
            }else if(success==3){
                Log.d("xD","sad 3");
                Toast toast = Toast.makeText(getApplicationContext(), R.string.activity_registro_email_existe_final, Toast.LENGTH_SHORT);
                toast.show();
            }else{
                Log.d("xD", "sad 1");
                new registrar().execute(username,email,name);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void goNewHome() {
        Intent intent = new Intent(getBaseContext(), ActivityHomeFinal.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();
    }

    public void usuarioNoExiste() {
        LoginManager.getInstance().logOut();
        Toast toast =
                Toast.makeText(getApplicationContext(),
                        R.string.activity_ingreso_usuario_no_existe, Toast.LENGTH_SHORT);

        toast.show();
    }
    public void irBienvenida() {
        Intent intent = new Intent(getBaseContext(), ActivityTabBienvenida.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();
    }
    // Manual
    private void irAlRegistro() {
        Intent intent = new Intent(getBaseContext(), ActivityRegistro.class);
        startActivity(intent);
    }

    private void irAlIngreso() {
        Intent intent = new Intent(getBaseContext(), ActivityIngreso.class);
        startActivity(intent);
    }

    // Linkedin
    private void irAlinkedinRegistro() {
        Toast.makeText(getApplicationContext(), getString(R.string.en_desarrollo), Toast.LENGTH_SHORT).show();
    }

    private void irAlinkedinIngreso() {
        Toast.makeText(getApplicationContext(), getString(R.string.en_desarrollo), Toast.LENGTH_SHORT).show();
    }

    protected void irAlHome() {
        super.irAlHome();

        // Cerrar Tutorial
        ActivityTutorial.activityTutorial.finish();
    }
}
