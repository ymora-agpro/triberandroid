package cl.agpro.triber;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by agpro on 23-02-16.
 */
public class Follow {
    public String type;
    public int id, equipo_id;
    public Bitmap icon;
    public String title;
    public String follower;

    public Follow(String type, int id, String title, String follower) {
        super();
        this.type = type;
        this.id = id;
        this.title = title;
        this.follower = follower;
    }

    public Follow(String type, int equipo_id, int id, String title, String follower) {
        super();
        this.type = type;
        this.id = id;
        this.equipo_id = equipo_id;
        this.title = title;
        this.follower = follower;
    }
}