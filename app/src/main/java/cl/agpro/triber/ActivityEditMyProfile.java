package cl.agpro.triber;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ActivityEditMyProfile extends AppCompatActivity {

    Functions funciones;
    private getData getData;
    String shoes, glove, tshirt;
    int int_shoes = 0, int_glove = 0, gender, int_tshirt = 0, gender_send;
    String name_send, last_name_send, rut_send, date_send, address_send, weight_send, height_send, shoes_send, glove_send, tshirt_send, phone_send, pass_send, repeat_pass_send;

    private GetDataShoes getDataShoes;
    private GetDataGlove getDataGlove;
    private GetDataTshirt getDataTshirt;
    private GetDataSave getDataSave;

    private ArriendoTipos tipos;
    private ArrayAdapterArriendo adapterDynamic;

    Spinner spinGenero, spinShoes, spinGlove, spinTshirt;
    String[] datosGenero = {"Masculino", "Femenino"};

    /**
     * Foto
     */
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
    private static final int CAPTURE_IMAGE_FROM_GALLERY = 300;

    private static final int MEDIA_TYPE_IMAGE = 1;
    private static final int MEDIA_TYPE_VIDEO = 2;

    private Uri fileUri;

    private ImageView ivFotoPerfil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_my_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarEditMyProfile);
        setSupportActionBar(toolbar);

        funciones = new Functions(this);

        spinGenero = (Spinner) findViewById(R.id.spinnerGeneroPerfil);
        ArrayAdapter<String> adapterType = new ArrayAdapter<String>(this, R.layout.spinner_arriendo, datosGenero);
        spinGenero.setAdapter(adapterType);

        spinGenero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ImageButton btnCamera = (ImageButton) findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickpic();
            }
        });

        getData = new getData();
        getData.execute(); //OBTENGO DATA DE WS

        Usuario usuario = new Usuario();
        usuario.id = funciones.userId();
        usuario.imageView = (ImageView) findViewById(R.id.imgUs);

        ivFotoPerfil = (ImageView) findViewById(R.id.imgUs);

        if (usuario.con_foto == null)
            new AsyncTaskFoto().execute(usuario);
        else if (usuario.con_foto == true)
            usuario.imageView.setImageBitmap(usuario.foto);

        getDataShoes = new GetDataShoes();
        getDataShoes.execute(); //OBTENGO DATA DE WS

        getDataGlove = new GetDataGlove();
        getDataGlove.execute(); //OBTENGO DATA DE WS

        getDataTshirt = new GetDataTshirt();
        getDataTshirt.execute(); //OBTENGO DATA DE WS

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_my_profile_guardar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.editarPerfilSave) {
            goArriendo();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Foto
     */
    private void clickpic() {
        android.app.AlertDialog.Builder getImageFrom = new android.app.AlertDialog.Builder(this);
        getImageFrom.setTitle("Escoger Imagen desde: ")
                .setItems(R.array.array_items_foto, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a file to save the image
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
                            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE); //// start the image capture Intent
                        }
                        if (which == 1) {
                            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                            startActivityForResult(intent, CAPTURE_IMAGE_FROM_GALLERY);
                        }
                    }
                });
        android.app.AlertDialog dialog = getImageFrom.create();
        dialog.show();
    }

    private static Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private Bitmap BitmapCenterCropCuadrado(Bitmap srcBmp) {
        Bitmap dstBmp;

        if (srcBmp.getWidth() >= srcBmp.getHeight()) {
            dstBmp = Bitmap.createBitmap(
                    srcBmp,
                    srcBmp.getWidth() / 2 - srcBmp.getHeight() / 2,
                    0,
                    srcBmp.getHeight(),
                    srcBmp.getHeight()
            );

        } else {
            dstBmp = Bitmap.createBitmap(
                    srcBmp,
                    0,
                    srcBmp.getHeight() / 2 - srcBmp.getWidth() / 2,
                    srcBmp.getWidth(),
                    srcBmp.getWidth()
            );
        }

        return dstBmp;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                try {

                    Bitmap photo = MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);
                    photo = BitmapCenterCropCuadrado(photo);
                    photo = Bitmap.createScaledBitmap(photo, 200, 200, false);

                    ivFotoPerfil.setImageBitmap(funciones.imagenRedonda(photo));

                    new AsyncTaskWebServiceImagePerfil(photo, Functions.URL_WS + "/cms/ws/androi_subir_imagen_perfil.php", funciones.userId()).execute();
                    Toast.makeText(getBaseContext(), "Foto Actualizada", Toast.LENGTH_SHORT).show();
                } catch (IOException ex) {

                }

            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }

        if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Video captured and saved to fileUri specified in the Intent
                Toast.makeText(this, "Video saved to:\n" +
                        data.getData(), Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the video capture
            } else {
                // Video capture failed, advise user
            }
        }

        if (requestCode == CAPTURE_IMAGE_FROM_GALLERY) {
            if (resultCode == RESULT_OK) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();

                Bitmap imgseleccionado = BitmapFactory.decodeFile(imgDecodableString);
                imgseleccionado = BitmapCenterCropCuadrado(imgseleccionado);
                imgseleccionado = Bitmap.createScaledBitmap(imgseleccionado, 200, 200, false);

                ivFotoPerfil.setImageBitmap(imgseleccionado);
                ivFotoPerfil.setImageBitmap(funciones.imagenRedonda(imgseleccionado));

                new AsyncTaskWebServiceImagePerfil(imgseleccionado, Functions.URL_WS + "/cms/ws/androi_subir_imagen_perfil.php", funciones.userId()).execute();

                Toast.makeText(getBaseContext(), "Foto Actualizada", Toast.LENGTH_SHORT).show();
                //ImageView imgView = (ImageView) findViewById(R.id.imgView);
                // Set the Image in ImageView after decoding the String
                //imgView.setImageBitmap(BitmapFactory
                //        .decodeFile(imgDecodableString));

            }
        }
    }

    /**
     * Imagen de Perfil
     */
    private void setImage(Usuario usuario) {
        if (usuario.foto != null)
            usuario.imageView.setImageBitmap(usuario.foto);
    }

    private class AsyncTaskFoto extends AsyncTask<Usuario, Void, Usuario> {

        protected Usuario doInBackground(Usuario... params) {
            Usuario usuario = params[0];
            usuario.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/perfiles/" + usuario.id + ".jpg");

            if (usuario.foto != null) {
                usuario.con_foto = true;
                usuario.foto = funciones.imagenRedonda(usuario.foto);
            } else usuario.con_foto = false;

            return usuario;
        }

        @Override
        protected void onPostExecute(Usuario usuario) {
            setImage(usuario);
        }
    }


    public void goArriendo() {


        EditText txt_pass = (EditText) findViewById(R.id.txtPassPerfil);
        pass_send = txt_pass.getText().toString();


        if (pass_send.equals("")) {
            EditText txt_name = (EditText) findViewById(R.id.txtNamePerfil);
            name_send = txt_name.getText().toString();
            EditText txt_last_name = (EditText) findViewById(R.id.txtLastNamePerfil);
            last_name_send = txt_last_name.getText().toString();
            EditText txt_rut = (EditText) findViewById(R.id.txtRutPerfil);
            rut_send = txt_rut.getText().toString();
            EditText txt_date = (EditText) findViewById(R.id.txtDatePerfil);
            date_send = txt_date.getText().toString();
            EditText txt_address = (EditText) findViewById(R.id.txtAddressPerfil);
            address_send = txt_address.getText().toString();
            EditText txt_weight = (EditText) findViewById(R.id.txtWeightPerfil);
            weight_send = txt_weight.getText().toString();
            EditText txt_height = (EditText) findViewById(R.id.txtHeightPerfil);
            height_send = txt_height.getText().toString();
            EditText txt_phone = (EditText) findViewById(R.id.txtPhonePerfil);
            phone_send = txt_phone.getText().toString();

            if (spinShoes != null)
                shoes_send = spinShoes.getSelectedItem().toString();

            if (spinShoes != null)
                glove_send = spinGlove.getSelectedItem().toString();

            if (spinShoes != null)
                tshirt_send = spinTshirt.getSelectedItem().toString();

            if (spinGenero.getSelectedItem().toString().equals("Masculino")) {
                gender_send = 1;
            } else {
                gender_send = 2;
            }

            getDataSave = new GetDataSave();
            getDataSave.execute(); //OBTENGO DATA DE WS

        } else {
            EditText txt_repeat_pass = (EditText) findViewById(R.id.txtRepeatPassPerfil);
            repeat_pass_send = txt_repeat_pass.getText().toString();

            if (pass_send.equals(repeat_pass_send)) {
                EditText txt_name = (EditText) findViewById(R.id.txtNamePerfil);
                name_send = txt_name.getText().toString();
                EditText txt_last_name = (EditText) findViewById(R.id.txtLastNamePerfil);
                last_name_send = txt_last_name.getText().toString();
                EditText txt_rut = (EditText) findViewById(R.id.txtRutPerfil);
                rut_send = txt_rut.getText().toString();
                EditText txt_date = (EditText) findViewById(R.id.txtDatePerfil);
                date_send = txt_date.getText().toString();
                EditText txt_address = (EditText) findViewById(R.id.txtAddressPerfil);
                address_send = txt_address.getText().toString();
                EditText txt_weight = (EditText) findViewById(R.id.txtWeightPerfil);
                weight_send = txt_weight.getText().toString();
                EditText txt_height = (EditText) findViewById(R.id.txtHeightPerfil);
                height_send = txt_height.getText().toString();
                EditText txt_phone = (EditText) findViewById(R.id.txtPhonePerfil);
                phone_send = txt_phone.getText().toString();
                shoes_send = spinShoes.getSelectedItem().toString();
                glove_send = spinGlove.getSelectedItem().toString();
                tshirt_send = spinTshirt.getSelectedItem().toString();

                if (spinGenero.getSelectedItem().toString().equals("Masculino")) {
                    gender_send = 1;
                } else {
                    gender_send = 2;
                }

                getDataSave = new GetDataSave();
                getDataSave.execute(); //OBTENGO DATA DE WS

            } else {
                DialogErrorPass();
            }
        }
    }

    private void DialogErrorPass() {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(getString(R.string.error_pass))
                .setTitle("Error");

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private void DialogErrorResult() {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(getString(R.string.error_result))
                .setTitle("Error");

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private class GetDataSave extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {

                String adrress = address_send.replace(" ", "*");
                String name = name_send.replace(" ", "*");
                String lastname = last_name_send.replace(" ", "*");
                String rut = rut_send.replace(" ", "*");
                String weight = weight_send.replace(" ", "*");
                String height = height_send.replace(" ", "*");
                String shoes = shoes_send.replace(" ", "*");
                String glove = glove_send.replace(" ", "*");
                String tshirt = tshirt_send.replace(" ", "*");
                String phone = phone_send.replace(" ", "*");
                String pass = pass_send.replace(" ", "*");
                String date = date_send.replace(" ", "*");

                Log.d("URL ", Functions.URL_WS + "/cms/ws/edit_profile.php?action=edit&user_id=" + funciones.userId() + "&nom=" + name + "&pat=" + lastname + "&rut=" + rut + "&pes=" + weight + "&alt=" + height + "&zap=" + shoes + "&gua=" + glove + "&cam=" + tshirt + "&tel=" + phone + "&pass=" + pass + "&date=" + date + "&genero=" + gender_send + "&dir=" + adrress);
                url = new URL(Functions.URL_WS + "/cms/ws/edit_profile.php?action=edit&user_id=" + funciones.userId() + "&nom=" + name + "&pat=" + lastname + "&rut=" + rut + "&pes=" + weight + "&alt=" + height + "&zap=" + shoes + "&gua=" + glove + "&cam=" + tshirt + "&tel=" + phone + "&pass=" + pass + "&date=" + date + "&genero=" + gender_send + "&dir=" + adrress);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsSave(result);
        }
    }

    public void getResultsSave(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");

            JSONObject row = dataArray.getJSONObject(0);//

            String success = row.getString("ok");

            if (success.equals("1")) {
                finish();
            } else {
                DialogErrorResult();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class getData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/edit_profile.php?action=information&user_id=" + funciones.userId());
                url = new URL(Functions.URL_WS + "/cms/ws/edit_profile.php?action=information&user_id=" + funciones.userId());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");

            JSONObject row = dataArray.getJSONObject(0);//

            EditText txt_name = (EditText) findViewById(R.id.txtNamePerfil);
            txt_name.setText(row.getString("nombres"));
            EditText txt_last_name = (EditText) findViewById(R.id.txtLastNamePerfil);
            txt_last_name.setText(row.getString("apellidos"));
            EditText txt_rut = (EditText) findViewById(R.id.txtRutPerfil);
            txt_rut.setText(row.getString("rut"));

            if (!row.getString("birthdate").equals("0")) {
                String[] data = row.getString("birthdate").split("-");
                EditText txt_date = (EditText) findViewById(R.id.txtDatePerfil);
                txt_date.setText(data[2] + "-" + data[1] + "-" + data[0]);
            }

            EditText txt_address = (EditText) findViewById(R.id.txtAddressPerfil);
            txt_address.setText(row.getString("direccion"));
            EditText txt_weight = (EditText) findViewById(R.id.txtWeightPerfil);
            txt_weight.setText(row.getString("peso"));
            EditText txt_height = (EditText) findViewById(R.id.txtHeightPerfil);
            txt_height.setText(row.getString("altura"));
            EditText txt_email = (EditText) findViewById(R.id.txtEmailPerfil);
            txt_email.setText(row.getString("email"));
            EditText txt_phone = (EditText) findViewById(R.id.txtPhonePerfil);
            txt_phone.setText(row.getString("telefono"));

            shoes = row.getString("zapato");
            glove = row.getString("guante");
            tshirt = row.getString("camiseta");
            gender = row.getInt("genero");

            if (gender == 2) {
                spinGenero.setSelection(1);
            } else {
                spinGenero.setSelection(0);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class GetDataShoes extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {

                String sURL = Functions.URL_WS + "/cms/ws/get_list_dress.php?action=zapatos";
                Log.d("WebService", sURL);
                url = new URL(sURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsShoes(result);
        }
    }

    public void getResultsShoes(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("zapatos");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            tipos = new ArriendoTipos();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);
                int id = 0;
                String nombre = row.getString("talla_zapato_id");
                tipos.add(new ArriendoTipo(nombre, id));

                if (shoes.equals(row.getString("talla_zapato_id"))) {
                    int_shoes = i;
                }
            }


            spinShoes = (Spinner) findViewById(R.id.spinnerShoesPerfil);
            adapterDynamic = new ArrayAdapterArriendo(this, tipos);
            spinShoes.setAdapter(adapterDynamic);

            spinShoes.setSelection(int_shoes);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class GetDataGlove extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {

                String sURL = Functions.URL_WS + "/cms/ws/get_list_dress.php?action=guantes";
                Log.d("WebService", sURL);
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsGlove(result);
        }
    }

    public void getResultsGlove(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("guantes");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            tipos = new ArriendoTipos();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);
                int id = 0;
                String nombre = row.getString("descripcion");
                tipos.add(new ArriendoTipo(nombre, id));

                if (glove.equals(row.getString("descripcion"))) {
                    int_glove = i;
                }
            }

            spinGlove = (Spinner) findViewById(R.id.spinnerGlovePerfil);
            adapterDynamic = new ArrayAdapterArriendo(this, tipos);
            spinGlove.setAdapter(adapterDynamic);

            spinGlove.setSelection(int_glove);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class GetDataTshirt extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {

                String sURL = Functions.URL_WS + "/cms/ws/get_list_dress.php?action=camisetas";
                Log.d("WebService", sURL);
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsTshirt(result);
        }
    }

    public void getResultsTshirt(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("camisetas");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            tipos = new ArriendoTipos();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);
                int id = 0;
                String nombre = row.getString("descripcion");
                tipos.add(new ArriendoTipo(nombre, id));

                if (tshirt.equals(row.getString("descripcion"))) {
                    int_tshirt = i;
                }
            }

            spinTshirt = (Spinner) findViewById(R.id.spinnerTshirtPerfil);
            adapterDynamic = new ArrayAdapterArriendo(this, tipos);
            spinTshirt.setAdapter(adapterDynamic);

            spinTshirt.setSelection(int_tshirt);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
