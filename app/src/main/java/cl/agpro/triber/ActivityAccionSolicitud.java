package cl.agpro.triber;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class ActivityAccionSolicitud extends AppCompatActivity {

    private int partido_id;
    Functions funciones;

    String equipo_nombre, tipo = "", minutes, pk1, pk2;
    int tiempo = 0;

    private ArriendoTipos tipos;
    private ArrayAdapterArriendo adapterDynamic;

    Spinner spinTipo, spinEquipo, spinTiempo, spinTiro1, spinTiro2, spinBloque1, spinBloque2;
    String[] datosTipo = {"Seleccione Detalle", "Tiro", "Bloqueador"}, datosTiempo = {"Seleccion Tiempo", "Primer tiempo", "Segundo tiempo"};

    TextView title1, title2, title3, title4;
    Spinner spin1, spin2, spin3, spin4;

    private GetDataEquipos getDataEquipos;
    private GetDataEquipo1 getDataEquipo1;
    private GetDataEquipo2 getDataEquipo2;


    private GetDataSend getDataSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accion_solicitud);
        funciones = new Functions(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        partido_id = bundle.getInt(Functions.PARTIDO_ID);

        Log.d("PARTIDO_ID", String.valueOf(partido_id));

        title1 = (TextView) findViewById(R.id.lbl_title_5_accion);
        title2 = (TextView) findViewById(R.id.lbl_title_6_accion);
        title3 = (TextView) findViewById(R.id.lbl_title_7_accion);
        title4 = (TextView) findViewById(R.id.lbl_title_8_accion);
        spin1 = (Spinner) findViewById(R.id.spinnerTirador1Accion);
        spin2 = (Spinner) findViewById(R.id.spinnerBloqueador1Accion);
        spin3 = (Spinner) findViewById(R.id.spinnerTirador2Accion);
        spin4 = (Spinner) findViewById(R.id.spinnerBloqueador2Accion);

        title1.setVisibility(View.GONE);
        title2.setVisibility(View.GONE);
        title3.setVisibility(View.GONE);
        title4.setVisibility(View.GONE);
        spin1.setVisibility(View.GONE);
        spin2.setVisibility(View.GONE);
        spin3.setVisibility(View.GONE);
        spin4.setVisibility(View.GONE);


        spinTipo = (Spinner) findViewById(R.id.spinnertipoAccion);
        ArrayAdapter<String> adapterType = new ArrayAdapter<String>(this, R.layout.spinner_arriendo, datosTipo);
        spinTipo.setAdapter(adapterType);

        spinTipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        title1.setVisibility(View.GONE);
                        title2.setVisibility(View.GONE);
                        title3.setVisibility(View.GONE);
                        title4.setVisibility(View.GONE);
                        spin1.setVisibility(View.GONE);
                        spin2.setVisibility(View.GONE);
                        spin3.setVisibility(View.GONE);
                        spin4.setVisibility(View.GONE);
                        tipo = "";

                        vaciaSpin();

                        break;
                    case 1:
                        title3.setVisibility(View.GONE);
                        title4.setVisibility(View.GONE);
                        spin3.setVisibility(View.GONE);
                        spin4.setVisibility(View.GONE);
                        title1.setVisibility(View.VISIBLE);
                        title2.setVisibility(View.VISIBLE);
                        spin1.setVisibility(View.VISIBLE);
                        spin2.setVisibility(View.VISIBLE);

                        tipo = "tiro";

                        getDataEquipos = new GetDataEquipos();
                        getDataEquipos.execute(); //OBTENGO DATA DE WS
                        break;
                    case 2:
                        title1.setVisibility(View.GONE);
                        title2.setVisibility(View.GONE);
                        spin1.setVisibility(View.GONE);
                        spin2.setVisibility(View.GONE);
                        title3.setVisibility(View.VISIBLE);
                        title4.setVisibility(View.VISIBLE);
                        spin3.setVisibility(View.VISIBLE);
                        spin4.setVisibility(View.VISIBLE);

                        tipo = "bloqueo";

                        getDataEquipos = new GetDataEquipos();
                        getDataEquipos.execute(); //OBTENGO DATA DE WS
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinTiempo = (Spinner) findViewById(R.id.spinnerTiempoAccion);
        ArrayAdapter<String> adapterType1 = new ArrayAdapter<String>(this, R.layout.spinner_arriendo, datosTiempo);
        spinTiempo.setAdapter(adapterType1);

        spinTiempo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        tiempo = 0;
                        break;
                    case 1:
                        tiempo = 1;
                        break;
                    case 2:
                        tiempo = 2;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_accion_solicitud, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.send_solicitud) {
            sendSolicitud();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class GetDataEquipos extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/list_team_partido.php?partido_id=" + partido_id);
                String sURL = Functions.URL_WS + "/cms/ws/list_team_partido.php?partido_id=" + partido_id;
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsEquipos(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResultsEquipos(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("partido");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            tipos = new ArriendoTipos();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {

                if (i == 0) {
                    String nomnbre = "Seleccione Equipo";
                    int id = 0;
                    tipos.add(new ArriendoTipo(nomnbre, id));
                }

                JSONObject row = dataArray.getJSONObject(i);
                int id = 0;
                String nombre = row.getString("equipo_nombre");
                tipos.add(new ArriendoTipo(nombre, id));
            }

            spinEquipo = (Spinner) findViewById(R.id.spinnerEquipoAccion);
            adapterDynamic = new ArrayAdapterArriendo(this, tipos);
            spinEquipo.setAdapter(adapterDynamic);


            spinEquipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    equipo_nombre = spinEquipo.getSelectedItem().toString();
                    Log.d("NOMBRE DEL EQUIPO", equipo_nombre);

                    getDataEquipo1 = new GetDataEquipo1();
                    getDataEquipo1.execute(); //OBTENGO DATA DE WS

                    getDataEquipo2 = new GetDataEquipo2();
                    getDataEquipo2.execute(); //OBTENGO DATA DE WS
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class GetDataEquipo1 extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {

                String new_equipo_nombre = equipo_nombre.replace(" ", "*");

                Log.d("URL ", Functions.URL_WS + "/cms/ws/get_player_request.php?action=pk1&partido_id=" + partido_id + "&equipo=" + new_equipo_nombre);
                String sURL = Functions.URL_WS + "/cms/ws/get_player_request.php?action=pk1&partido_id=" + partido_id + "&equipo=" + new_equipo_nombre;
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsEquipo1(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResultsEquipo1(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("jugadores");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            tipos = new ArriendoTipos();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {

                if (i == 0) {
                    String nomnbre = "Seleccione";
                    int id = 0;
                    tipos.add(new ArriendoTipo(nomnbre, id));
                }

                JSONObject row = dataArray.getJSONObject(i);
                int id = 0;
                String nombre = row.getString("jugador");
                tipos.add(new ArriendoTipo(nombre, id));
            }


            if (tipo.equals("tiro")) {
                spinTiro1 = (Spinner) findViewById(R.id.spinnerTirador1Accion);
                adapterDynamic = new ArrayAdapterArriendo(this, tipos);
                spinTiro1.setAdapter(adapterDynamic);


                spinTiro1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        pk1 = spinTiro1.getSelectedItem().toString();
                        Log.d("NOMBRE DEL JUGADOR 1", pk1);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            } else if (tipo.equals("bloqueo")) {
                spinBloque2 = (Spinner) findViewById(R.id.spinnerBloqueador2Accion);
                adapterDynamic = new ArrayAdapterArriendo(this, tipos);
                spinBloque2.setAdapter(adapterDynamic);

                spinBloque2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        pk1 = spinBloque2.getSelectedItem().toString();
                        Log.d("NOMBRE DEL JUGADOR 1", pk1);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class GetDataEquipo2 extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {

                String new_equipo_nombre = equipo_nombre.replace(" ", "*");

                Log.d("URL ", Functions.URL_WS + "/cms/ws/get_player_request.php?action=pk2&partido_id=" + partido_id + "&equipo=" + new_equipo_nombre);
                String sURL = Functions.URL_WS + "/cms/ws/get_player_request.php?action=pk2&partido_id=" + partido_id + "&equipo=" + new_equipo_nombre;
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsEquipo2(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResultsEquipo2(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("jugadores");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            tipos = new ArriendoTipos();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {

                if (i == 0) {
                    String nombre_equipo;
                    if (tipo.equals("tiro")) {
                        nombre_equipo = "Nadie";
                    } else {
                        nombre_equipo = "No sé";
                    }

                    int id = 0;
                    tipos.add(new ArriendoTipo(nombre_equipo, id));
                }

                JSONObject row = dataArray.getJSONObject(i);
                int id = 0;
                String nombre = row.getString("jugador");
                tipos.add(new ArriendoTipo(nombre, id));
            }


            if (tipo.equals("bloqueo")) {
                spinTiro2 = (Spinner) findViewById(R.id.spinnerTirador2Accion);
                adapterDynamic = new ArrayAdapterArriendo(this, tipos);
                spinTiro2.setAdapter(adapterDynamic);

                spinTiro2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        pk2 = spinTiro2.getSelectedItem().toString();
                        Log.d("NOMBRE DEL JUGADOR 2", pk2);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            } else if (tipo.equals("tiro")) {
                spinBloque1 = (Spinner) findViewById(R.id.spinnerBloqueador1Accion);
                adapterDynamic = new ArrayAdapterArriendo(this, tipos);
                spinBloque1.setAdapter(adapterDynamic);

                spinBloque1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        pk2 = spinBloque1.getSelectedItem().toString();
                        Log.d("NOMBRE DEL JUGADOR 2", pk2);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void vaciaSpin() {
        tipos = new ArriendoTipos();
        String nombre_equipo = "Seleccione Equipo";
        int id = 0;
        tipos.add(new ArriendoTipo(nombre_equipo, id));
        spinEquipo = (Spinner) findViewById(R.id.spinnerEquipoAccion);
        adapterDynamic = new ArrayAdapterArriendo(this, tipos);
        spinEquipo.setAdapter(adapterDynamic);
    }

    public void sendSolicitud() {
        EditText txt_minutos = (EditText) findViewById(R.id.txt_minutoAccion);
        minutes = txt_minutos.getText().toString();

        if (tipo.equals("")) {
            Dialog("detalle");
        } else {
            if (equipo_nombre.equals("Seleccione Equipo")) {
                Dialog("equipo");
            } else {
                if (tiempo == 0) {
                    Dialog("tiempo");
                } else {
                    if (minutes.equals("")) {
                        Dialog("minuto");
                    } else {
                        switch (tipo) {
                            case "tiro":
                                if (spin1.getSelectedItemPosition() == 0) {
                                    Dialog("tirador");
                                    return;
                                }
                            case "bloqueo":
                                if (spin3.getSelectedItemPosition() == 0) {
                                    Dialog("bloqueador");
                                    return;
                                }
                        }

                        getDataSend = new GetDataSend();
                        getDataSend.execute(); //OBTENGO DATA DE WS
                    }
                }
            }
        }
    }

    private class GetDataSend extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {

                String new_equipo_nombre = equipo_nombre.replace(" ", "*");
                String new_pk1 = pk1.replace(" ", "*");
                String new_pk2 = pk2.replace(" ", "*");

                Log.d("URL ", Functions.URL_WS + "/cms/ws/get_player_request.php?action=save&partido_id=" + partido_id + "&tiempo=" + tiempo + "&minutos=" + minutes + "&equipo=" + new_equipo_nombre + "&pk1=" + new_pk1 + "&pk2=" + new_pk2 + "&user_id=" + funciones.userId());
                String sURL = Functions.URL_WS + "/cms/ws/get_player_request.php?action=save&partido_id=" + partido_id + "&tiempo=" + tiempo + "&minutos=" + minutes + "&equipo=" + new_equipo_nombre + "&pk1=" + new_pk1 + "&pk2=" + new_pk2 + "&user_id=" + funciones.userId();
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsSend(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResultsSend(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("solicitud");

            JSONObject row = dataArray.getJSONObject(0);

            int soli = row.getInt("soli");

            if (soli == 1) {
                Dialog("ok");
                finish();
            } else {
                Dialog("red");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void Dialog(String str) {

        String mensaje = "";

        switch (str) {
            case "detalle":
                mensaje = getString(R.string.error_detalle);
                break;
            case "equipo":
                mensaje = getString(R.string.error_equipo);
                break;
            case "tiempo":
                mensaje = getString(R.string.error_tiempo);
                break;
            case "minuto":
                mensaje = getString(R.string.error_minuto);
                break;
            case "red":
                mensaje = getString(R.string.error_red);
                break;
            case "ok":
                mensaje = getString(R.string.solicitud_ok);
                break;
            case "bloqueador":
                mensaje = getString(R.string.error_bloqueador);
                break;
            case "tirador":
                mensaje = getString(R.string.error_tirador);
                break;
        }

        Toast toast = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
        toast.show();
    }

}
