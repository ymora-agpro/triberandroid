package cl.agpro.triber;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class ActivityEquipo extends AppCompatActivity {
    public int serie_id;

    private Functions funciones;
    private getData getData;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        fab = (FloatingActionButton) findViewById(R.id.fab);

        Bundle bundle = getIntent().getExtras();
        serie_id = bundle.getInt(Functions.SERIE_ID);

        Log.d(Functions.SERIE_ID, String.valueOf(serie_id));

        funciones = new Functions(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        getData = new getData();
        getData.execute(serie_id);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_equipo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                funciones.irAlMuro();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void favoritoAgregar() {
        fab.setImageResource(R.mipmap.ic_star_fixed);
        fab.setBackgroundTintList(ContextCompat.getColorStateList(getBaseContext(), R.color.rojo_claro));
    }

    private void favoritoQuitar() {
        fab.setImageResource(R.mipmap.ic_star_fixed_off);
        fab.setBackgroundTintList(ContextCompat.getColorStateList(getBaseContext(), R.color.gris));
    }

    /**
     * Detalles del Equipo
     */
    private class getData extends AsyncTask<Integer, Void, String> {
        @Override
        protected String doInBackground(Integer... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            String serie_id = params[0].toString();

            URL url = null;
            try {
                String sURL = Functions.URL_WS + "/cms/ws/detalle_equipo.php?equipo_id=" + serie_id + "&user=" + funciones.userId();
                Log.d("URL ", sURL);
                url = new URL(sURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }
    }

    private void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("details");

            JSONObject row = dataArray.getJSONObject(0);

            TextView TextViewEquipo = (TextView) findViewById(R.id.equipo);
            TextViewEquipo.setText(row.getString("nombre_equipo"));

            TextView serie = (TextView) findViewById(R.id.serie);
            serie.setText(row.getString("nombre_serie"));

            TextView seguidores = (TextView) findViewById(R.id.seguidores);
            seguidores.setText(row.getString("followers"));

            if (row.getInt("seguido") == 0)
                favoritoQuitar();

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SetFavorito setFavorito = new SetFavorito();
                    setFavorito.execute(serie_id);
                }
            });

            Equipo equipo = new Equipo();

            equipo.id = row.getInt("equipo_id");
            equipo.imageView = (ImageView) findViewById(R.id.imagen);

            if (equipo.con_foto == null)
                new AsyncTaskFoto().execute(equipo);
            else if (equipo.con_foto == true)
                equipo.imageView.setImageBitmap(equipo.foto);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Imagen del Equipo
     */
    private void setImage(Equipo equipo) {
        if (equipo.foto != null)
            equipo.imageView.setImageBitmap(equipo.foto);
    }

    private class AsyncTaskFoto extends AsyncTask<Equipo, Void, Equipo> {

        protected Equipo doInBackground(Equipo... params) {
            Equipo equipo = params[0];
            equipo.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/equipos/" + equipo.id + ".jpg");

            if (equipo.foto != null) {
                equipo.con_foto = true;
                equipo.foto = funciones.imagenRedonda(equipo.foto);
            } else equipo.con_foto = false;

            return equipo;
        }

        @Override
        protected void onPostExecute(Equipo equipo) {
            setImage(equipo);
        }
    }

    /**
     * Favorito
     */
    private class SetFavorito extends AsyncTask<Integer, Void, String> {
        @Override
        protected String doInBackground(Integer... params) {

            Integer serie_id = params[0];

            String UrlFollow = Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + serie_id.toString() + "&follow_table=seguidores_equipos_series&follow_type=equipo_serie_id";

            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                url = new URL(UrlFollow);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("SetFavorito", result);

            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(result.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("click");

                JSONObject row = dataArray.getJSONObject(0);

                if (row.getString("action").equals("eliminado"))
                    favoritoQuitar();
                else
                    favoritoAgregar();


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    /**
     * Fragment para los detalles del partido
     */
    public static class FragmentDetalles extends Fragment {
        private Functions funciones;
        private getData getData;
        private View rootView;

        public FragmentDetalles() {
        }

        public static FragmentDetalles newInstance(int serie_id) {
            FragmentDetalles fragment = new FragmentDetalles();
            Bundle args = new Bundle();
            args.putInt(Functions.SERIE_ID, serie_id);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            funciones = new Functions(getActivity());

            getData = new getData();
            getData.execute(getArguments().getInt(Functions.SERIE_ID)); //OBTENGO DATA DE WS

            rootView = inflater.inflate(R.layout.fragment_equipo_detalles, container, false);

            return rootView;
        }

        private class getData extends AsyncTask<Integer, Void, String> {
            @Override
            protected String doInBackground(Integer... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                String equipo_id = params[0].toString();

                URL url = null;
                try {
                    String sURL = Functions.URL_WS + "/cms/ws/detalle_equipo.php?equipo_id=" + equipo_id + "&user=" + funciones.userId();
                    Log.d("URL ", sURL);
                    url = new URL(sURL);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResults(result);
            }
        }

        private void getResults(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("details");

                JSONObject row = dataArray.getJSONObject(0);

                TextView pais = (TextView) rootView.findViewById(R.id.pais);
                pais.setText(row.getString("pais"));

                TextView administrador = (TextView) rootView.findViewById(R.id.administrador);
                administrador.setText(row.getString("nombre_user"));

                TextView correo = (TextView) rootView.findViewById(R.id.correo);
                correo.setText(row.getString("user_email"));

                TextView telefono = (TextView) rootView.findViewById(R.id.telefono);
                telefono.setText(row.getString("user_telefono"));

                TextView jugadores = (TextView) rootView.findViewById(R.id.jugadores);
                jugadores.setText(row.getString("cant"));

                TextView unido = (TextView) rootView.findViewById(R.id.unido);
                unido.setText(row.getString("miembro_desde"));

                TextView descripcion = (TextView) rootView.findViewById(R.id.descripcion);
                descripcion.setText(row.getString("descripcion"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Fragment para los jugadores del equipo (Plantel)
     */
    public static class FragmentJugadores extends Fragment {
        private Functions funciones;
        private View rootView;

        public FragmentJugadores() {
        }

        public static FragmentJugadores newInstance(int equipo_id) {
            FragmentJugadores fragment = new FragmentJugadores();
            Bundle args = new Bundle();
            args.putInt(Functions.SERIE_ID, equipo_id);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            funciones = new Functions(getActivity());

            GetData getData = new GetData();
            getData.execute(getArguments().getInt(Functions.SERIE_ID)); //OBTENGO DATA DE WS

            rootView = inflater.inflate(R.layout.fragment_equipo_jugadores, container, false);

            return rootView;
        }

        private class GetData extends AsyncTask<Integer, Void, String> {
            @Override
            protected String doInBackground(Integer... params) {
                String Lang = Locale.getDefault().getDisplayLanguage();
                StringBuilder response = new StringBuilder();

                String serie_id = params[0].toString();

                URL url = null;
                try {
                    url = new URL(Functions.URL_WS + "/cms/ws/detella_jugadores_equipos.php?equipo=" + serie_id + "&user=" + funciones.userId());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection httpconn = null;
                try {
                    httpconn = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null) {
                            response.append(strLine);
                        }
                        response.append(strLine);
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                getResults(result);
            }

        }

        public void getResults(String response) {
            JSONObject obj = null;
            try {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("players");

                //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
                Jugadores jugadores = new Jugadores();

                //RELLENO MI CLASE CON LA INFORMACION DEL WS
                Jugador jugador;
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject row = dataArray.getJSONObject(i);

                    jugador = new Jugador();

                    jugador.id = row.getInt("jugador");
                    jugador.posicion = row.getString("posicion");
                    jugador.nombre = row.getString("nombre");

                    jugadores.add(jugador);
                }

                ListView ListViewJugadores = (ListView) rootView.findViewById(R.id.jugadores);
                ArrayAdapterJugadores arrayAdapterJugadores = new ArrayAdapterJugadores(getActivity(), jugadores);
                ListViewJugadores.setAdapter(arrayAdapterJugadores);

                ListViewJugadores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Jugador jugador = (Jugador) parent.getItemAtPosition(position);

                        Intent intent = new Intent(getContext(), ActivityJugador.class);
                        intent.putExtra(ActivityJugador.JUGADOR_ID, jugador.id);
                        startActivity(intent);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private class ArrayAdapterJugadores extends ArrayAdapter<Jugador> {

            public ArrayAdapterJugadores(Context context, List<Jugador> jugadores) {
                super(context, R.layout.listview_jugador, jugadores);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                LayoutInflater inflater = getActivity().getLayoutInflater();
                view = inflater.inflate(R.layout.listview_jugador, parent, false);

                if ((position + 1) % 2 == 1)
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_1));
                else
                    view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gris_2));

                Jugador jugador = getItem(position);

                jugador.imageView = (ImageView) view.findViewById(R.id.foto);

                if (jugador.con_foto == null)
                    new AsyncTaskFoto().execute(jugador);
                else if (jugador.con_foto == true)
                    jugador.imageView.setImageBitmap(jugador.foto);

                TextView nombre = (TextView) view.findViewById(R.id.nombre);
                nombre.setText(jugador.nombre);

                TextView posicion = (TextView) view.findViewById(R.id.posicion);
                posicion.setText(jugador.posicion);

                return view;
            }

            private void setImage(Jugador jugador) {
                if (jugador.foto != null)
                    jugador.imageView.setImageBitmap(jugador.foto);
            }

            private class AsyncTaskFoto extends AsyncTask<Jugador, Void, Jugador> {

                protected Jugador doInBackground(Jugador... params) {
                    Jugador jugador = params[0];
                    jugador.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/perfiles/" + jugador.id + ".jpg");

                    if (jugador.foto != null) {
                        jugador.con_foto = true;
                        jugador.foto = funciones.imagenRedonda(jugador.foto);
                    } else jugador.con_foto = false;

                    return jugador;
                }

                @Override
                protected void onPostExecute(Jugador jugador) {
                    setImage(jugador);
                }
            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return FragmentDetalles.newInstance(serie_id);
                case 1:
                    return FragmentPartidos.newInstance(serie_id);
                case 2:
                    return FragmentJugadores.newInstance(serie_id);
            }

            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Detalles";
                case 1:
                    return "Partidos";
                case 2:
                    return "Plantel";
            }
            return null;
        }
    }
}
