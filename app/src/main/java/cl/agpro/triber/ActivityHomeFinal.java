package cl.agpro.triber;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.login.LoginManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ActivityHomeFinal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {
    SectionPagerAdapter sectionPagerAdapter;
    private TabLayout tabLayout;

    private FragmentTabSeguimientosCompetencias fragmentTabSeguimientosCompetencias = null;
    private FragmentTabSeguimientosEquipos fragmentTabSeguimientosEquipos = null;
    private FragmentTabSeguimientosJugadores fragmentTabSeguimientosJugadores = null;

    private TextView tvUsuarioNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_home_final);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView nav_view = (NavigationView) findViewById(R.id.nav_view);

        View nav_header = LayoutInflater.from(this).inflate(R.layout.nav_header_activity_home_final, null);
        tvUsuarioNombre = (TextView) nav_header.findViewById(R.id.usuarioNombre);
        nav_view.addHeaderView(nav_header);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        sectionPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(sectionPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        //sectionPagerAdapter.updateData();

        Functions functions = new Functions(this);
        new GetUsuarioNombre().execute(functions.userId());
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.salir))
                .setMessage(getString(R.string.desea_salir))
                .setPositiveButton(getString(R.string.si), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityHomeFinal.super.onBackPressed();
                    }
                })
                .setNegativeButton(getString(R.string.no), null)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_home_final, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        //searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.menu_perfil) {
            goEditProfile();
        } else if (id == R.id.menu_favo) {
            Log.d("menu", "favoritos");
        } else if (id == R.id.menu_msg) {
            goMsg();
        } else if (id == R.id.menu_arre) {
            // goArriendo();
        } else if (id == R.id.menu_segui) {
            goSeguimiento();
        } else if (id == R.id.menu_logout) {
            logoutApp();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class SectionPagerAdapter extends FragmentPagerAdapter {

        public SectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    if (fragmentTabSeguimientosCompetencias == null)
                        fragmentTabSeguimientosCompetencias = new FragmentTabSeguimientosCompetencias();
                    return fragmentTabSeguimientosCompetencias;
                case 1:
                    if (fragmentTabSeguimientosEquipos == null)
                        fragmentTabSeguimientosEquipos = new FragmentTabSeguimientosEquipos();
                    return fragmentTabSeguimientosEquipos;
                default:
                    if (fragmentTabSeguimientosJugadores == null)
                        fragmentTabSeguimientosJugadores = new FragmentTabSeguimientosJugadores();
                    return fragmentTabSeguimientosJugadores;
            }
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
        }

        public void setData(String searchText) {
            switch (tabLayout.getSelectedTabPosition()) {
                case 0:
                    fragmentTabSeguimientosCompetencias.setData(searchText);
                    break;
                case 1:
                    fragmentTabSeguimientosEquipos.setData(searchText);
                    break;
                default:
                    fragmentTabSeguimientosJugadores.setData(searchText);
            }
        }

        public void updateData() {
            switch (tabLayout.getSelectedTabPosition()) {
                case 0:
                    //if (fragmentTabSeguimientosCompetencias != null)
                    //  fragmentTabSeguimientosCompetencias.updateData();
                    break;
                case 1:
                    if (fragmentTabSeguimientosEquipos != null)
                        fragmentTabSeguimientosEquipos.updateData();
                    break;
                default:
                    //if (fragmentTabSeguimientosJugadores != null)
                    //  fragmentTabSeguimientosJugadores.updateData();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Competencias";
                case 1:
                    return "Equipos";
                default:
                    return "Jugadores";
            }
        }
    }


    /**
     * Nombre del Usuario
     */
    private class GetUsuarioNombre extends AsyncTask<Integer, Void, String> {
        @Override
        protected String doInBackground(Integer... params) {
            StringBuilder response = new StringBuilder();

            String usuario_id = params[0].toString();

            URL url = null;
            try {
                String sURL = Functions.URL_WS + "/cms/ws/get_name.php?user_id=" + usuario_id;
                Log.d("URL ", sURL);
                url = new URL(sURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }
    }

    private void getResults(String response) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(response.toString());

            JSONArray dataArray = obj.getJSONArray("name");
            JSONObject row = dataArray.getJSONObject(0);

            tvUsuarioNombre.setText(row.getString("nombre"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void logoutApp() {
        //CERRAR SESIÓN FACEBOOK
        LoginManager.getInstance().logOut();

        //BORRAR DADTOS DE LOGIN
        SharedPreferences prefs = getSharedPreferences("usuario", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();

        //CERRAR TODAS LAS ACTIVITIES Y ENVIAR A MAIN
        Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void goEditProfile() {
        Intent intent = new Intent(getBaseContext(), ActivityEditProfile.class);

        startActivity(intent);
    }

    public void goSeguimiento() {
        Intent intent = new Intent(getBaseContext(), ActivitySeguimientos.class);

        startActivity(intent);
    }

    public void goArriendo() {
        Intent intent = new Intent(getBaseContext(), ActivityArriendo.class);

        startActivity(intent);
    }

    public void goMsg() {
        Intent intent = new Intent(getBaseContext(), ActivityMensajes.class);

        startActivity(intent);
    }

    public void goNewHome() {
        Intent intent = new Intent(getBaseContext(), ActivityHomeFinal.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();
    }

    @Override
    public boolean onQueryTextChange(String searchText) {


        if (searchText.isEmpty())
            switch (tabLayout.getSelectedTabPosition()) {
                case 0:
                    fragmentTabSeguimientosCompetencias.closeData();
                    break;
                case 1:
                    fragmentTabSeguimientosEquipos.closeData();
                    break;
                default:
                    fragmentTabSeguimientosJugadores.closeData();
            }
        else
            sectionPagerAdapter.setData(searchText);

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String searchText) {
        return false;
    }
}
