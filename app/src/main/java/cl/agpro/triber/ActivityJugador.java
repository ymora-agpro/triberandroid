package cl.agpro.triber;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class ActivityJugador extends AppCompatActivity {

    public static final String JUGADOR_ID = "JUGADOR_ID";
    int jugador_id;
    Functions funciones;
    private getData getData;
    private  getSave getSave;
    private getDatosJugador getDatosJugador;
    private getDatosFotosJugador getDatosFotosJugador;
    private getDatosNotasJugador getDatosNotasJugador;

    Boolean con_foto_jugador = null;
    ImageView foto_jugador;
    Bitmap jugador_bipmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jugador);

        funciones = new Functions(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_jugador);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSave = new getSave();
                getSave.execute();
            }
        });


        Bundle bundle = getIntent().getExtras();
        jugador_id = bundle.getInt(JUGADOR_ID);

        Log.d(JUGADOR_ID, String.valueOf(jugador_id));

        getData = new getData();
        getData.execute(); //OBTENGO DATA DE WS

        getDatosJugador = new getDatosJugador();
        getDatosJugador.execute(); //OBTENGO DATA DE WS

        getDatosFotosJugador= new getDatosFotosJugador();
        getDatosFotosJugador.execute();

        getDatosNotasJugador = new getDatosNotasJugador();
        getDatosNotasJugador.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_jugador, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                funciones.irAlMuro();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class getData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/detalle_usuario.php?usuario=" + jugador_id + "&user=" + funciones.userId());
                url = new URL(Functions.URL_WS + "/cms/ws/detalle_usuario.php?usuario=" + jugador_id + "&user=" + funciones.userId());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("details");

            JSONObject row = dataArray.getJSONObject(0);//

            TextView txt_name = (TextView) findViewById(R.id.txtNameJugador);
            txt_name.setText(row.getString("nombre"));
            TextView txt_follow = (TextView) findViewById(R.id.txtFollowJugador);
            txt_follow.setText(row.getString("followers"));

            String jug_id = row.getString("usuario_id");

            foto_jugador = (ImageView) findViewById(R.id.img_jugador);

            if (con_foto_jugador == null)
                new AsyncTaskFotoPartido().execute(jug_id);
            else if (con_foto_jugador == true)
                foto_jugador.setImageBitmap(jugador_bipmap);

            TextView txt_rol = (TextView) findViewById(R.id.txtRolJugador);
            txt_rol.setText(row.getString("rol"));
            TextView txt_address = (TextView) findViewById(R.id.txtAddressJugador);
            txt_address.setText(row.getString("direccion"));

            TextView txt_age = (TextView) findViewById(R.id.txtAgeJugador);
            txt_age.setText(row.getString("edad"));

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_jugador);

            if (row.getInt("seguido") == 1) {
                fab.setImageResource(R.mipmap.ic_star_fixed);
                fab.setBackgroundTintList(ContextCompat.getColorStateList(getBaseContext(), R.color.rojo_claro));
            } else {
                fab.setImageResource(R.mipmap.ic_star_fixed_off);
                fab.setBackgroundTintList(ContextCompat.getColorStateList(getBaseContext(), R.color.gris));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class getSave extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + jugador_id + "&follow_table=seguidores_usuarios&follow_type=user_seguidor_id");
                url = new URL(Functions.URL_WS + "/cms/ws/list_categories.php?action=click&user=" + funciones.userId() + "&follow_id=" + jugador_id + "&follow_table=seguidores_usuarios&follow_type=user_seguidor_id");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) { getResultsSave(result); }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    public void getResultsSave(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("click");
            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            //RELLENO MI CLASE CON LA INFORMACION DEL WS

            JSONObject row = dataArray.getJSONObject(0);

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_jugador);

            if (row.getString("action").equals("eliminado")) {
                fab.setImageResource(R.mipmap.ic_star_fixed_off);
                fab.setBackgroundTintList(ContextCompat.getColorStateList(getBaseContext(), R.color.gris));
            } else {
                fab.setImageResource(R.mipmap.ic_star_fixed);
                fab.setBackgroundTintList(ContextCompat.getColorStateList(getBaseContext(), R.color.rojo_claro));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void setImage(Bitmap bitmap) {
        if (bitmap != null)
            foto_jugador.setImageBitmap(bitmap);
    }

    private class AsyncTaskFotoPartido extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... params) {
            String id = params[0];
            jugador_bipmap = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/perfiles/" + id + ".jpg");


            if (jugador_bipmap != null) {
                con_foto_jugador = true;
                jugador_bipmap = funciones.imagenRedonda(jugador_bipmap);
            } else con_foto_jugador  = false;

            return jugador_bipmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            setImage(bitmap);
        }
    }


    private class getDatosJugador extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/jugadores_equipos.php?user=" + jugador_id + "&action=jugador_estadisticas");
                url = new URL(Functions.URL_WS + "/cms/ws/jugadores_equipos.php?user=" + jugador_id + "&action=jugador_estadisticas");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) { getResultsDatosJugador(result); }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    public void getResultsDatosJugador(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("results");
            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            //RELLENO MI CLASE CON LA INFORMACION DEL WS

            int n = dataArray.length();

            if ( n > 0 ) {

                JSONObject datosArray = (JSONObject) dataArray.get(0);

                JSONArray forAr = datosArray.getJSONArray("partidos");
                JSONArray datosAr = datosArray.getJSONArray("general");
                JSONObject row = datosAr.getJSONObject(0);

                TextView fecha = (TextView)findViewById(R.id.text_fecha_jugador);
                fecha.setText(row.getString("fecha_union"));

                TextView partidos = (TextView)findViewById(R.id.txt_patidos_jugadores);
                partidos.setText(row.getString("total_partidos"));

                TextView victoria = (TextView)findViewById(R.id.txt_victoria_jugador);
                victoria.setText(row.getString("total_victorias"));

                TextView destacado = (TextView)findViewById(R.id.txt_destacado_jugador);
                destacado.setText(row.getString("total_destacado"));


                DetalleJugadores detalleJugadores = new DetalleJugadores();
                DetalleJugador detalleJugador;

                int height = 0;
                for ( int i = 0; i < forAr.length(); i++ ) {
                    JSONObject f = forAr.getJSONObject(i);

                    String tipo = f.getString("tipo");
                    Log.d("ELTIPOES:", tipo);

                    detalleJugador = new DetalleJugador();

                    detalleJugador.tipo = tipo;

                    if (tipo.equals("header")) {

                        detalleJugador.team_id = f.getInt("equipo_id");
                        detalleJugador.name_team = f.getString("equipo");
                        detalleJugador.date_team = f.getString("unido");

                        detalleJugadores.add(detalleJugador);
                        height += 60;

                    } else {

                        detalleJugador.posicion = f.getString("posicion");
                        detalleJugador.jugados = f.getString("partidos");
                        detalleJugador.victorias = f.getString("victorias");
                        detalleJugador.destacado = f.getString("destacado");
                        detalleJugador.goles = f.getString("goles");
                        detalleJugador.tiros = f.getString("tiros");
                        detalleJugador.bloqueos = f.getString("bloqueos");
                        detalleJugador.minutos = f.getString("minutos");
                        detalleJugador.verdes = f.getString("verdes");
                        detalleJugador.amarillas = f.getString("amarillas");
                        detalleJugador.rojas = f.getString("rojas");

                        detalleJugadores.add(detalleJugador);

                        height += 340;
                    }

                    ListView ListViewDetalle = (ListView) findViewById(R.id.listViewDetalleJugador);
                    ArrayAdapterDetalle arrayAdapterDetalle = new ArrayAdapterDetalle(this, detalleJugadores);
                    ListViewDetalle.setAdapter(arrayAdapterDetalle);

                    ListViewDetalle.getLayoutParams().height = funciones.getPixeles(height);
                }
            } else {
                RelativeLayout rel = (RelativeLayout) findViewById(R.id.relativeEstadisticaJugador);
                rel.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class ArrayAdapterDetalle extends ArrayAdapter<DetalleJugador> {

        private ImageView foto;

        public ArrayAdapterDetalle(Context context, List<DetalleJugador> detalleJugadores) {
            super(context, R.layout.listview_detalles_jugador, detalleJugadores);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_detalles_jugador, parent, false);

            DetalleJugador detalleJugador = getItem(position);

            String tipo = detalleJugador.tipo;

            if( tipo.equals("header") ) {
                RelativeLayout relative = (RelativeLayout)view.findViewById(R.id.relative2Detalle);
                relative.setVisibility(view.GONE);

                detalleJugador.imageView = (ImageView) view.findViewById(R.id.img_team_jugador);

                if (detalleJugador.con_foto == null)
                    new AsyncTaskFoto().execute(detalleJugador);
                else if (detalleJugador.con_foto == true)
                    detalleJugador.imageView.setImageBitmap(detalleJugador.foto);

                TextView name = (TextView) view.findViewById(R.id.txt_name_team_jugador);
                name.setText(detalleJugador.name_team);

                TextView date = (TextView) view.findViewById(R.id.txt_fecha_team_jugador);
                date.setText(detalleJugador.date_team);

            } else {
                RelativeLayout relative = (RelativeLayout)view.findViewById(R.id.relative1Detalle);
                relative.setVisibility(view.GONE);

                TextView posicion = (TextView) view.findViewById(R.id.txt_position_jugador);
                posicion.setText(detalleJugador.posicion);

                TextView jugados = (TextView) view.findViewById(R.id.txt_partidos_jugados_jugador);
                jugados.setText(detalleJugador.jugados);

                TextView victorias = (TextView) view.findViewById(R.id.txt_victoria_detalle_jugador);
                victorias.setText(detalleJugador.victorias);

                TextView destacado = (TextView) view.findViewById(R.id.txt_destacado_detalle_jugador);
                destacado.setText(detalleJugador.destacado);

                TextView goles = (TextView) view.findViewById(R.id.txt_goles_detalle_jugador);
                goles.setText(detalleJugador.goles);

                TextView tiros = (TextView) view.findViewById(R.id.txt_shot_detalle_jugador);
                tiros.setText(detalleJugador.tiros);

                TextView bloqueos = (TextView) view.findViewById(R.id.txt_block_detalle_jugador);
                bloqueos.setText(detalleJugador.bloqueos);

                TextView minutos = (TextView) view.findViewById(R.id.txt_minute_detalle_jugador);
                minutos.setText(detalleJugador.minutos);

                TextView verdes = (TextView) view.findViewById(R.id.txt_verde_detalle_jugador);
                verdes.setText(detalleJugador.verdes);

                TextView amarillas = (TextView) view.findViewById(R.id.txt_amarillo_detalle_jugador);
                amarillas.setText(detalleJugador.amarillas);

                TextView rojas = (TextView) view.findViewById(R.id.txt_rojo_detalle_jugador);
                rojas.setText(detalleJugador.rojas);

            }


            return view;
        }

        private void setImage(DetalleJugador detalleJugador) {
            if (detalleJugador.foto != null)
                detalleJugador.imageView.setImageBitmap(detalleJugador.foto);
        }

        private class AsyncTaskFoto extends AsyncTask<DetalleJugador, Void, DetalleJugador> {

            protected DetalleJugador doInBackground(DetalleJugador... params) {
                DetalleJugador detalleJugador = params[0];
                detalleJugador.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/equipos/" + detalleJugador.team_id + ".jpg");

                if (detalleJugador.foto != null) {
                    detalleJugador.con_foto = true;
                    detalleJugador.foto = funciones.imagenRedonda(detalleJugador.foto);
                } else detalleJugador.con_foto = false;

                return detalleJugador;
            }

            @Override
            protected void onPostExecute(DetalleJugador detalleJugador) {
                setImage(detalleJugador);
            }
        }
    }

    /**
     * Fotos
     */
    private class getDatosFotosJugador extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/jugadores_equipos.php?user=" + jugador_id + "&action=fotografo");
                url = new URL(Functions.URL_WS + "/cms/ws/jugadores_equipos.php?user=" + jugador_id + "&action=fotografo");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) { getResultsDatosFotosJugador(result); }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    public void getResultsDatosFotosJugador(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("photo");
            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            //RELLENO MI CLASE CON LA INFORMACION DEL WS

            JSONObject row1 = dataArray.getJSONObject(0);

            int n = row1.getInt("suc");

            if ( n > 0 ) {

                DetalleJugadores detalleJugadores = new DetalleJugadores();
                DetalleJugador detalleJugador;

                for ( int i = 0; i < dataArray.length(); i++ ) {

                    JSONObject row = dataArray.getJSONObject(i);

                    detalleJugador = new DetalleJugador();

                    detalleJugador.team_id = row.getInt("equipo_id");
                    detalleJugador.name_team = row.getString("equipo_nombre");
                    detalleJugador.date_team = row.getString("serie_nombre");
                    detalleJugador.posicion = row.getString("fotos");

                    detalleJugadores.add(detalleJugador);
                }

                ListView ListViewDetalleFoto = (ListView) findViewById(R.id.listViewFotosJugador);
                ArrayAdapterDetalleFoto arrayAdapterDetalleFoto = new ArrayAdapterDetalleFoto(this, detalleJugadores);
                ListViewDetalleFoto.setAdapter(arrayAdapterDetalleFoto);

                ListViewDetalleFoto.getLayoutParams().height = dataArray.length() * funciones.getPixeles(100);

            } else {
                RelativeLayout rel = (RelativeLayout) findViewById(R.id.relativeFotosJugador);
                rel.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class ArrayAdapterDetalleFoto extends ArrayAdapter<DetalleJugador> {

        private ImageView foto;

        public ArrayAdapterDetalleFoto(Context context, List<DetalleJugador> detalleJugadores) {
            super(context, R.layout.listview_fotos_jugador, detalleJugadores);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_fotos_jugador, parent, false);

            DetalleJugador detalleJugador = getItem(position);


            detalleJugador.imageView = (ImageView) view.findViewById(R.id.img_team_foto_jugador);

            if (detalleJugador.con_foto == null)
                new AsyncTaskFoto().execute(detalleJugador);
            else if (detalleJugador.con_foto == true)
                detalleJugador.imageView.setImageBitmap(detalleJugador.foto);


            TextView name = (TextView) view.findViewById(R.id.txt_name_team_foto_jugador);
            name.setText(detalleJugador.name_team);

            TextView date = (TextView) view.findViewById(R.id.txt_serie_team_foto_jugador);
            date.setText(detalleJugador.date_team);

            TextView posicion = (TextView) view.findViewById(R.id.txt_foto_jugador);
            posicion.setText(detalleJugador.posicion);


            return view;
        }

        private void setImage(DetalleJugador detalleJugador) {
            if (detalleJugador.foto != null)
                detalleJugador.imageView.setImageBitmap(detalleJugador.foto);
        }

        private class AsyncTaskFoto extends AsyncTask<DetalleJugador, Void, DetalleJugador> {

            protected DetalleJugador doInBackground(DetalleJugador... params) {
                DetalleJugador detalleJugador = params[0];
                detalleJugador.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/equipos/" + detalleJugador.team_id + ".jpg");

                if (detalleJugador.foto != null) {
                    detalleJugador.con_foto = true;
                    detalleJugador.foto = funciones.imagenRedonda(detalleJugador.foto);
                } else detalleJugador.con_foto = false;

                return detalleJugador;
            }

            @Override
            protected void onPostExecute(DetalleJugador detalleJugador) {
                setImage(detalleJugador);
            }
        }
    }

    /**
     * Notas
     */
    private class getDatosNotasJugador extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", Functions.URL_WS + "/cms/ws/jugadores_equipos.php?user=" + jugador_id + "&action=notero");
                url = new URL(Functions.URL_WS + "/cms/ws/jugadores_equipos.php?user=" + jugador_id + "&action=notero");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) { getResultsDatosNotasJugador(result); }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    public void getResultsDatosNotasJugador(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("nota");
            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            //RELLENO MI CLASE CON LA INFORMACION DEL WS

            JSONObject row1 = dataArray.getJSONObject(0);

            int n = row1.getInt("suc");

            if ( n > 0 ) {

                DetalleJugadores detalleJugadores = new DetalleJugadores();
                DetalleJugador detalleJugador;

                for ( int i = 0; i < dataArray.length(); i++ ) {

                    detalleJugador = new DetalleJugador();

                    JSONObject row = dataArray.getJSONObject(i);

                    detalleJugador.team_id = row.getInt("equipo_id");
                    detalleJugador.name_team = row.getString("equipo_nombre");
                    detalleJugador.date_team = row.getString("serie_nombre");
                    detalleJugador.posicion = row.getString("notas");

                    detalleJugadores.add(detalleJugador);
                }

                ListView ListViewDetalleNota = (ListView) findViewById(R.id.listViewNotasJugador);
                ArrayAdapterDetalleNota arrayAdapterDetalleNota = new ArrayAdapterDetalleNota(this, detalleJugadores);
                ListViewDetalleNota.setAdapter(arrayAdapterDetalleNota);

                ListViewDetalleNota.getLayoutParams().height = dataArray.length() * funciones.getPixeles(100);

            } else {
                RelativeLayout rel = (RelativeLayout) findViewById(R.id.relativeNotasJugador);
                rel.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class ArrayAdapterDetalleNota extends ArrayAdapter<DetalleJugador> {

        private ImageView foto;

        public ArrayAdapterDetalleNota(Context context, List<DetalleJugador> detalleJugadores) {
            super(context, R.layout.listview_notas_jugador, detalleJugadores);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_notas_jugador, parent, false);

            DetalleJugador detalleJugador = getItem(position);


            detalleJugador.imageView = (ImageView) view.findViewById(R.id.img_team_nota_jugador);

            if (detalleJugador.con_foto == null)
                new AsyncTaskFoto().execute(detalleJugador);
            else if (detalleJugador.con_foto == true)
                detalleJugador.imageView.setImageBitmap(detalleJugador.foto);


            TextView name = (TextView) view.findViewById(R.id.txt_name_team_nota_jugador);
            name.setText(detalleJugador.name_team);

            TextView date = (TextView) view.findViewById(R.id.txt_serie_team_nota_jugador);
            date.setText(detalleJugador.date_team);

            TextView posicion = (TextView) view.findViewById(R.id.txt_nota_jugador);
            posicion.setText(detalleJugador.posicion);


            return view;
        }

        private void setImage(DetalleJugador detalleJugador) {
            if (detalleJugador.foto != null)
                detalleJugador.imageView.setImageBitmap(detalleJugador.foto);
        }

        private class AsyncTaskFoto extends AsyncTask<DetalleJugador, Void, DetalleJugador> {

            protected DetalleJugador doInBackground(DetalleJugador... params) {
                DetalleJugador detalleJugador = params[0];
                detalleJugador.foto = funciones.downloadBitmap(Functions.URL_WS + "/imagenes/equipos/" + detalleJugador.team_id + ".jpg");

                if (detalleJugador.foto != null) {
                    detalleJugador.con_foto = true;
                    detalleJugador.foto = funciones.imagenRedonda(detalleJugador.foto);
                } else detalleJugador.con_foto = false;

                return detalleJugador;
            }

            @Override
            protected void onPostExecute(DetalleJugador detalleJugador) {
                setImage(detalleJugador);
            }
        }
    }

}
