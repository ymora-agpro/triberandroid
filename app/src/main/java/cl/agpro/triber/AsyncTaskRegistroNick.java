package cl.agpro.triber;

import android.os.AsyncTask;

/**
 * Created by yerkomora on 23-09-15.
 */
public class AsyncTaskRegistroNick extends AsyncTask<Void, Void, Boolean> {

    private ActivityRegistro activityRegistro;
    private final String nick;

    public AsyncTaskRegistroNick(ActivityRegistro activityRegistro, String nick) {
        this.activityRegistro = activityRegistro;
        this.nick = nick;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        MySQL mySQL = new MySQL();
        mySQL.conectar();

        Boolean existe = mySQL.existeUsuarioNick(nick);

        mySQL.desconectar();
        return existe;
    }

    @Override
    protected void onPostExecute(Boolean existe) {
        super.onPostExecute(existe);

        if (existe)
            activityRegistro.nickExiste();
        else
            activityRegistro.nickValido();

    }
}
