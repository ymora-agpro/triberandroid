package cl.agpro.triber;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by yerkomora on 25-09-15.
 */
public class AsyncTaskIngreso extends AsyncTask<Void, Void, Boolean> {

    private static final String TAG = "AsyncTaskIngreso";
    private final ActivityIngreso activityIngreso;
    private final String nickOemail, clave;

    public AsyncTaskIngreso(ActivityIngreso activityIngreso, String nickOemail, String clave) {
        this.activityIngreso = activityIngreso;
        this.nickOemail = nickOemail;
        this.clave = clave;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        MySQL mySQL = new MySQL();
        mySQL.conectar();

        Boolean encontrado = mySQL.existeUsuario(nickOemail, clave);

        mySQL.desconectar();
        return encontrado;
    }

    @Override
    protected void onPostExecute(Boolean encontrado) {
        super.onPostExecute(encontrado);

        if (encontrado) {
            activityIngreso.irAlHome();
            Log.d(TAG, encontrado.toString());
        }
        else {
            activityIngreso.usuarioNoExiste();
        }
    }
}