package cl.agpro.triber;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by yerkomora on 23-09-15.
 */
public class Usuario {
    public int id;
    public String nick, email, clave;
    public boolean jugador = false, notero = false, fotografo = false;

    public Bitmap foto = null;
    public Boolean con_foto = null;
    public ImageView imageView;
}
